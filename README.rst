MVRSION
=======

Code for running MVRSION on 16s sequencing runs

Usage
-----
To duplicate the MVRSION validation download the fastq_files.tar from this link

https://data.mendeley.com/datasets/c3yzpm2428/1

Next, run the validation samples on an HPC environment using 8 threads per sample::
  
   mvrsion --ref_dir mvrsion/database -k mvrsion/gv9/data/validation_knowns.txt -p mvrsion/gv9/data/config16.yml -o mvrsion_output --data fastq_files -w 8


Installation
------------
Install miniconda as described here: https://conda.io/miniconda.html

Install git-lfs so the git clone will download the database files from bitbucket::

  sudo apt install git-lfs
  
Clone the git repo and setup the conda environment for running MVRSION::

 git clone https://bitbucket.org/WUGTAC/mvrsion
 cd mvrsion
 conda env create -n mvrsion -f conda/environment.yml
 source activate mvrsion

Install the mvrsion python package and verify the program installed correctly::
  
 pip install -r requirements.txt
 mvrsion --help

Add your novoalign licence file next to the novoalign binary installed in the conda environment

Requirements
^^^^^^^^^^^^
* python 2.7
* `Miniconda <https://conda.io/miniconda.html>`_
* Licence to use `Novoalign <http://www.novocraft.com/products/novoalign/>`_

Compatibility
-------------
Tested on Ubuntu 16+ but should work on any flavor of Linux. Supports sample-level parallelization on HPC environments using slurm, SGE, and Torque/PBS


Authors
-------

`MVRSION` was written by `Andrew Schriefer <aeschriefer@wustl.edu>`_.
