import setuptools
import os
from glob import glob


def find_console_scripts(script_dir):
    '''
    script_dir is relative to the topmost directory of the distribution.
    Assumes that every subdirectory is a module
    Assumes that .py file in script_dir has a main() function
    '''
    console_scripts = []
    module_name = lambda x: os.path.splitext(x)[0].replace(os.sep, '.')
    for py in glob(os.path.join(script_dir, '*.py')):
        if '__init__' in py:
            continue
        module = module_name(py)
        script = module.split('.')[-1] + ' = ' + module + ':main'
        console_scripts.append(script)
    return console_scripts


console_scripts = find_console_scripts('gv9/scripts')
console_scripts.append('mvrsion = gv9.__main__:main')
console_scripts.append('ssu = gv9.ssu:cli')
console_scripts.append('setup_qiime = gv9.qiime.qiime_setup_plus:main')
console_scripts.append('db_build = gv9.db_build.__main__:main')

setuptools.setup(
    name="mvrsion",
    version="1.0.0",
    url="fake-github.com",
    author="Andrew Schriefer",
    author_email="ae.schriefer123@gmail.com",
    description=
    "Code for running MVRSION and qiime analysis on 16s Illumina sequencing",
    long_description=open('README.rst').read(),
    packages=setuptools.find_packages(),
    package_dir={'gv9': 'gv9'},
    package_data={'gv9': ['data/*']},
    entry_points={'console_scripts': console_scripts},
    install_requires=[
        'path.py', 'sh', 'PyYAML', 'numpy', 'pandas', 'regex', 'biopython',
        'HTSeq', 'pysam', 'ruffus', 'gtacUtil', 'scikit-learn', 'fuzzywuzzy',
        'python-Levenshtein', 'matplotlib', 'seaborn'
    ],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ], )
