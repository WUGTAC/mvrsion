#!/bin/bash -x

#SBATCH --export=ALL
#SBATCH --mem=10000

#meant to be run in the tax_otu directory

IN_OTU=$1
KEY=$2

OTU='final_otu_table.biom'

#print out a summary of the table
biom summarize-table -i $IN_OTU -o summary.txt

#sort the analysis otu table by sample name. This changes order for graphs from summary_taxa_through_plots.py
sort_otu_table.py -i $IN_OTU -o $OTU -m $KEY

summarize_taxa_through_plots.py -i $OTU \
    -o taxa_summary -m $KEY -p qiime_parameters.txt

make_otu_heatmap.py -i $OTU \
    --suppress_row_clustering --suppress_column_clustering -o OTU_Heatmap

alpha_rarefaction.py -i $OTU \
    -m $KEY \
    -o arare -p qiime_parameters.txt \
    -a -O 8

beta_diversity_through_plots.py -i $OTU -m $KEY \
    -o bdiv -a -O 4 -p qiime_parameters.txt

