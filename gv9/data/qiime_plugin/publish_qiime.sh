#!/bin/bash
shopt -s extglob

if [ -z "$1" ]; then
    echo "Error nothing passed! Pass the basename of publish directory, ie /srv/seq/solexa/Download/$1"
    exit 1
fi

publishDir=/scratch/gtac/GTAC_RUN_DATA/$1
mkdir -p $publishDir

if [ ! -d "$publishDir/reads" ]; then
    mkdir $publishDir/reads
    cp -L ../Reads/!(*trimmed).fq.gz $publishDir/reads
fi

if [ ! -f "$publishDir/combined_seqs.fna" ]; then
    cp ../combined_seqs.fna $publishDir/
fi

cp ../full_sample_key.txt $publishDir/

rm -r $publishDir/alpha_diversity
rm -r $publishDir/alpha_diversity_plots
rm -r $publishDir/beta_diversity

cp rarefied_otu_table.* $publishDir/
cp otu_table_mc2_w_tax_no_pynast_failures.biom $publishDir/
cp otu_table.tsv $publishDir/
cp summary.txt $publishDir/otu_reads_summary.txt

cp rarefied_otu_table.biom $publishDir/
cp rarefied_otu_table.tsv $publishDir/

cp -r taxa_summary/ $publishDir

cp -r arare/alpha_rarefaction_plots $publishDir
mv $publishDir/alpha_rarefaction_plots $publishDir/alpha_diversity_plots

mkdir $publishDir/alpha_diversity
cp -r arare/alpha_div_collated/* $publishDir/alpha_diversity

cp -r bdiv $publishDir
mv $publishDir/bdiv $publishDir/beta_diversity

chmod -R o+r,g+w $publishDir/*
