#!/bin/bash -x

#SBATCH --export=ALL
#SBATCH --mem=10000

#meant to be run in the tax_otu directory

IN_OTU=$1
KEY=$2
DEPTH=$3 #number of reads to do rarefaction on

if [ -z "$DEPTH" ]; then
    DEPTH="1000"
fi

OTU='rarefied_otu_table.biom'

tmp_otu=$(mktemp -p $PWD tmp.biom.XXXXXXXXXX)

trap "rm -f $tmp_otu" EXIT

#print out a summary of the table
biom summarize-table -i $IN_OTU -o summary.txt

#This will remove any samples that have few than DEPTH reads in them
single_rarefaction.py -i $IN_OTU -o $tmp_otu -d $DEPTH

#sort the analysis otu table by sample name. This changes order for graphs from summary_taxa_through_plots.py
sort_otu_table.py -i $tmp_otu -o $OTU -m $KEY

#Make excel-importable files for input and rarefied otu tables
biom convert -i $OTU -o rarefied_otu_table.tsv --table-type "OTU table" --to-tsv --header-key=taxonomy
biom convert -i $IN_OTU -o otu_table.tsv --table-type "OTU table" --to-tsv --header-key=taxonomy

summarize_taxa_through_plots.py -i $OTU \
    -o taxa_summary -m $KEY -p ../qiime_parameters.txt

make_otu_heatmap.py -i $OTU \
    --suppress_row_clustering --suppress_column_clustering -o OTU_Heatmap

alpha_rarefaction.py -i $OTU \
    -t rep_set.tre -m $KEY \
    -o arare -p ../qiime_parameters.txt \
    -a -O 8

beta_diversity_through_plots.py -i $OTU -m $KEY -t rep_set.tre \
    -o bdiv -a -O 4 -p ../qiime_parameters.txt

make_2d_plots.py -m $KEY -i bdiv/unweighted_unifrac_pc.txt -o unweighted_unifrac_2d/

make_2d_plots.py -m $KEY -i bdiv/weighted_unifrac_pc.txt -o weighted_unifrac_2d/
