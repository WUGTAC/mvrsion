import cPickle as pickle
import sh, re, pdb
import logging
from itertools import izip, repeat, ifilter
from collections import OrderedDict, Counter, namedtuple
from HTSeq import GFF_Reader, FastaReader
from gv9.util.ssu_util import RdpFactory, SsuDataBase, RdpStringNotValidError
from Bio import Entrez
from path import path


LVL_ABBRV = {
    'superkingdom' : 'k',
    'phylum' : 'p',
    'class' : 'c',
    'order' : 'o',
    'family' : 'f',
    'genus' : 'g',
    'species' : 's',
    'strain' : 't'
    }

SsuRecord = namedtuple('SsuRecord', 'taxid,fasta,feature')

def _convert_entrez_rec_to_rdp(rec_list, rdp_factory=None, abbv=LVL_ABBRV):
    '''
    Takes as input the result of Entrez.read(Entrez.efetch(db='taxonomy'))
    Outputs dictionary
    '''
    rdp_dict = {}
    
    for rec in rec_list:
        if 'LineageEx' not in rec:
            continue

        rdp = OrderedDict(
            (abbv.get(lvl['Rank']), lvl['ScientificName'])
             for lvl in rec['LineageEx'] if lvl['Rank'] in abbv
            )

        #sometimes there is no species level in the LineageEx
        #we assume there will always be genus
        #If there is a species then we take the strain from the ScientificName
        #Otherwise we take species from ScientificName and set strain to empty
        if 's' in rdp and 'g' in rdp:
            try:
                #ncbi usually reports the species name as "Genus species"
                rdp['s'] = re.sub(r'%s' % rdp['g'], '', rdp['s'], flags=re.I).strip()

                #ncbi only holds strain info in the full Scientific name of the taxon, not in the lineage
                rdp['t'] = re.sub(r'%s|%s' % (rdp['s'],rdp['g']), '', rec['ScientificName'], flags=re.I).strip()
            except:
                logging.warning(rec['TaxId'] + '\n' + str(rdp) + '\n')
                continue
            
        else:
            #do not add taxon that have no species info
            continue
        
        #remove any taxon that is missing a higher level entry (ie family)
        try:
            rdp = rdp_factory.create(rdp)
        except RdpStringNotValidError:
            continue
        else:
            rdp_dict[rec['TaxId']] = rdp

    return rdp_dict

    
def create_entrez_taxid_to_rdp(taxid_list):
    '''
    Input is list of ncbi taxonomy ids
    Output is list of full rdp taxonomy strings
    '''
    Entrez.email = 'fake@gmail.com'
    epost_results = \
      Entrez.read(Entrez.epost('taxonomy', id=','.join(taxid_list)))

    batch_size = 200
    N = len(taxid_list)
    
    taxid_to_rdp = {}
    rdpFactory = RdpFactory()
    
    for start in xrange(0, N, batch_size):
        fetch_handle = \
          Entrez.efetch(
            db='taxonomy',
            rettype='xml',
            retmode='text',
            retstart=start,
            retmax=batch_size,
            webenv=epost_results['WebEnv'],
            query_key=epost_results['QueryKey'],
            )
        
        data = Entrez.read(fetch_handle)
 
        taxid_to_rdp.update(_convert_entrez_rec_to_rdp(data, rdpFactory, LVL_ABBRV))
        
    return taxid_to_rdp


def is_16s_length(iv):
    #average is about 1500
    return 500 < abs(iv.end - iv.start) < 3000


def parse_gff_for_16s(gff):
    '''
    Returns tuple(ncbi taxonomy id, list of HTSeq.GenomicFeatures of 16s exons)
    '''
    gff_parser = GFF_Reader(open(gff, 'r'), end_included=True)

    try:
        exons = [feat for feat in gff_parser
                 if feat.type == 'rRNA' and feat.attr.get('product') == '16S ribosomal RNA']
    except:
        return None,[]

    #we assume in above that each 16s gene has only one exon
    ## if len(set(e.attr['Parent'] for e in exons)) != len(exons):
    ##     pdb.set_trace()
        #raise RuntimeError( 'Group of exons with same 16S RNA parent feature')
    
    taxid = re.search(r'id=(\d+)', gff_parser.metadata['species']).group(1)
    
    return taxid,exons


def retrieve_16s_sequence(fna_fh, exons, extra=0):
    '''
    fna is path to a fasta file.
    exons is a list of HTSeq.GenomicFeatures
    '''
    intervals = [e.iv for e in exons]

    def _handle_strand(seq, iv, extra):
        s = seq[max(0,iv.start-extra):iv.end+extra]
        s2 = s.get_reverse_complement() if iv.strand == '-' else s
        s2.descr = seq.descr
        return s2
        
    for seq in FastaReader(fna_fh):
        for iv in intervals:
             if iv.chrom in seq.name and is_16s_length(iv):
                 yield _handle_strand(seq, iv, extra)



def format_16s_copy_name(taxid, copy_num):
    return '{0}:{1}'.format(taxid, copy_num)

    
def format_seq_name(seq, taxid, counter=Counter()):
    #htseq adds [part] to seq name if u slice it, remove that here
    seq.descr = '{0} {1}'.format(re.sub(r'\[part\]|revcomp_of_', '', seq.name), seq.descr)
    seq.name = format_16s_copy_name(taxid, counter[taxid])
    counter[taxid] += 1
    return seq
    

def write_rdp_taxonomy(tax_to_rdp, tax_counter, out_fp):
    '''
    Writes separate line for every 16s copy for every taxon
    taxons are named like 511145:2
    '''
    with open(out_fp, 'w') as fh_out:
        for each in tax_to_rdp.iteritems():
            
            N = tax_counter[each[0]]
            
            for i,(taxid, rdp) in enumerate(repeat(each, N)):
                
                name = format_16s_copy_name(taxid, i)
                fh_out.write('%s\t%s\n' % (name, rdp))
    

def write_fasta(ssu_record_list, tax_to_rdp, out_fp):
    with open(out_fp, 'w') as fh_out:
        for each in ssu_record_list:
            if each.taxid in tax_to_rdp:
                each.rec.write_to_fasta_file(fh_out)
                
    
def main():
    '''
    To make the complete database run with python -O
    Otherwise a tiny database is made for purposes of debugging this script
    Before running this script you need to download tarballs from ncbi ftp site.
    For example:
    
    ftp://ftp.ncbi.nlm.nih.gov/genomes/ASSEMBLY_BACTERIA/all.fna.tar.gz
    ftp://ftp.ncbi.nlm.nih.gov/genomes/ASSEMBLY_BACTERIA/all.gff.tar.gz

    Untar these things into directories fna/ and gff/ respectively
    These files are tarbombs so make sure to use tar -C fna|gff
    
    There may also be tars inside the tar so run:
    
    find ./gff -name *.tgz -print -execdir tar -xzf {} \;

    All this crap will take a very long time so qsub the bastards
    
    This script is meant to be run in directory containing both fna/ and gff/
    '''
    logging.basicConfig(filename='log.txt', level=logging.WARNING, filemode='w')
    
    EXTRA_BP = 100 #number of extra bp to add down and up from 16s gene

    BREAK_POINT = 5 #used for debugging. Stop after finding this many taxons
    
    fasta_fp = path('ref_16s.fna')
    taxonomy_fp = path('rdp_taxonomy.txt') #create this to support qiime
    ssu_db_fp = path('ssu_db.pickle')
    
    ssu_record_list = []
    lappend = ssu_record_list.append

    #walk through all gff collecting info along the way
    for gff in path('gff').walk('*.gff'):
        #will emit None,[] if gff cannot be parsed successfully
        taxid,features = parse_gff_for_16s(gff)

        if len(features) > 0 and taxid is not None:
            fna = path(re.sub('gff', 'fna', gff))
            lappend(SsuRecord(taxid=taxid, fasta=fna, feature=features))

        if __debug__:
            if len(set(r.taxid for r in ssu_record_list)) >= BREAK_POINT:
                break

    #filter out taxa without species info
    #filter out taxa missing higher level entries as well
    #includes all such taxa even if no 16s seqs are found for them
    tax_to_rdp = \
      create_entrez_taxid_to_rdp(
          set(r.taxid for r in ssu_record_list)
          )

    taxid_counter = Counter() #counts number of 16s copies we have for each taxon

    #write the fasta
    fasta_out_fh = open(fasta_fp, 'w')
    
    for rec in ifilter(lambda x: x.taxid in tax_to_rdp, ssu_record_list):
        with open(rec.fasta, 'r') as fna_fh:
            for seq in retrieve_16s_sequence(fna_fh, rec.feature, extra=EXTRA_BP):
                format_seq_name(seq, rec.taxid, counter=taxid_counter)
                seq.write_to_fasta_file(fasta_out_fh)

    fasta_out_fh.close()
    
    #2 column tab separated file, used in qiime and to create SsuDataBase objects
    write_rdp_taxonomy(tax_to_rdp, taxid_counter, taxonomy_fp)

    ssu_db = SsuDataBase(fasta_fp, taxonomy_fp)

    with open(ssu_db_fp, 'wb') as fh:
        pickle.dump(ssu_db, fh, -1)

    sh.novoindex(fasta_fp.namebase + '.ndx', fasta_fp)

    
if __name__ == '__main__':
    main()
        
