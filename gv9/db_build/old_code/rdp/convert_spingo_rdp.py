import pandas as pd
import cPickle as pickle
import pdb
from path import path
from Bio import SeqIO
import gv9.make_region_db
from gv9.util.ssu_util import _RdpString, RdpFactory, SsuDataBase

in_dir = path('/home/aeschriefer/spingo/database')
out_dir = path('/scratch/gtac/gtac_data/reference_sequences/microbes/rdp')

class MiniRdpString(_RdpString):
    rdp_label_hierarchy = ('g', 's')


def modify_seqid(rec):
    rec.id = rec.id + ':1'
    return rec

with open(in_dir.joinpath('RDP_11.2.species.fa'),'r') as fh_in, \
    open(out_dir.joinpath('ref_16s.fna'), 'w') as fh_out:
    SeqIO.write(
        (modify_seqid(rec) for rec in SeqIO.parse(fh_in, 'fasta')),
        fh_out,
        'fasta',)


df = pd.read_table(in_dir.joinpath('taxonomy.map'), sep='\t', header=None)

factory = RdpFactory(MiniRdpString)

new_df = pd.DataFrame()

new_df['seq'] = df.iloc[:,0].apply(lambda x: str(x) + ':1')
new_df['tax'] = df.iloc[:,1].apply(factory.create_from_unlabeled, token='_')

new_df.to_csv(out_dir.joinpath('rdp_taxonomy.txt'), sep='\t', index=False, header=False)

#we use the default RdpFactory to get the full (mostly-blank) taxonomy for the database
ssu_db = SsuDataBase(out_dir.joinpath('rdp_taxonomy.txt'))

with open(out_dir.joinpath('ssu_db.pickle'), 'wb') as fh:
    pickle.dump(ssu_db, fh, -1)
    
sh.novoindex(fasta_fp.namebase + '.ndx', fasta_fp, k=15, t=1)

gv9.make_region_db.main()


