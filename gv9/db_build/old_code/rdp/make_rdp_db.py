import re
import sh
import cPickle as pickle
import argparse
import logging
import hashlib

from path import path
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

import gv9.make_region_db
from gv9.util.ssu_util import RdpFactory, SsuDataBase, SeqID, _RdpString
from gtac.util import get_script_dir


class MiniRdp(_RdpString):
    rdp_label_hierarchy = ('s', 't')


class TaxIdMaker(object):
    def __init__(self):
        self.id = 1000
        self.dict = {}

    def make(self, rdp):
        try:
            return self.dict[rdp]
        except KeyError:
            self.dict[rdp] = self.id
            self.id += 1
            return self.dict[rdp]


class TaxIdMakerSpecies(object):
     def make(self, rdp):
        m = hashlib.md5()
        m.update(rdp.get_level('s'))
        return m.hexdigest()


def create_rdp_string(factory, stop_words, english_words, header):
    '''
    Accepts the description of a fasta header ie full text in the > line
    english words is a set generated from /usr/share/dict/words
    '''
    if header[0] == '>':
        raise RuntimeError()

    info, _ = header.split('\t', 1)
    _, species_info = info.split(' ', 1)
    rdp = factory.create_from_unlabeled(species_info)

    unclassified = bool(re.search(r'^bacterium|\sbacterium', rdp['s'], flags=re.I))
    stopped = any(re.search(word, rdp, flags=re.I) for word in stop_words)
    if not (stopped or unclassified):
        split_species = rdp['s'].split()
        num_words = 0
        for each in split_species:
            if re.match(r'^[a-zA-Z]+$', each):
                num_words += 1
            else:
                break

        if num_words >= 2 and split_species[0].lower() not in english_words:
            rdp.tokens['s'] = ' '.join(split_species[:2])
            rdp.tokens['t'] = ''  # remove the strain info
            return rdp.factory.create(rdp.tokens)
    # If execution reaches here the seq hasnt been added, lets log it
    logging.info(species_info)
    

def parse_fasta_and_make_seqid_to_rdp(fh_in, fh_out):
    stop_words = ['culture', 'unclassified', 'unidentified', 'symbiont', 'Candidatus']
    rdp_to_taxID = dict()
    seqID_to_rdp = dict()
    factory = RdpFactory(MiniRdp)
    taxMaker = TaxIdMakerSpecies()

    with open(get_script_dir().joinpath('..', 'genera.txt'), 'r') as fh:
        genera_set = set(line.strip().lower() for line in fh)

    with open('/usr/share/dict/words', 'r') as fh:
        english_words = set(line.strip().lower() for line in fh) - genera_set

    for record in SeqIO.parse(fh_in, 'fasta'):
        rdp = create_rdp_string(factory, stop_words, english_words, record.description)
        if rdp is None:
            continue

        if rdp not in rdp_to_taxID:
            rdp_to_taxID[rdp] = taxMaker.make(rdp)

        seqID = SeqID(rdp_to_taxID[rdp], record.id)
        seqID_to_rdp[seqID] = rdp

        new_record = SeqRecord(
            seq=record.seq.upper(),
            id=str(seqID),
            description=re.sub(record.id, '', record.description))
        fh_out.write(new_record.format('fasta'))

    return seqID_to_rdp


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('raw_fasta', type=path)
    parser.add_argument('--region', action='store_true')
    parser.add_argument('-n', '--amp_len', type=int, default=100)

    args = parser.parse_args()

    logging.basicConfig(filename='removed_seqs.txt', filemode='w', level=logging.INFO)
    
    fasta_fp = path('ref_16s.fna')
    taxonomy_fp = path('rdp_taxonomy.txt')
    ssu_db_fp = path('ssu_db.pickle')

    with open(args.raw_fasta, 'r') as fh_in, \
         open(fasta_fp, 'w') as fh_out:

        # writes output fasta as a side-effect
        seqID_to_rdp = parse_fasta_and_make_seqid_to_rdp(fh_in, fh_out)

    with open(taxonomy_fp, 'w') as fh:
        fh.writelines(sorted('\t'.join([str(seq), rdp]) + '\n'
                             for seq, rdp in seqID_to_rdp.iteritems()))

    ssu_db = SsuDataBase(taxonomy_fp)

    with open(ssu_db_fp, 'wb') as fh:
        pickle.dump(ssu_db, fh, -1)

    sh.novoindex(fasta_fp.namebase + '.ndx', fasta_fp)

    if args.region:
        gv9.make_region_db.main(args.amp_len)
