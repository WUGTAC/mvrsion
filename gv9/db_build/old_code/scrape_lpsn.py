import mechanicalsoup as ms
import time


def find_genus_tag(tag):
    is_right_tag = tag.name == 'span' and tag['class'] == ['genusspecies']
    is_nested = tag.parent.name == 'a'
    is_lone = tag.next_sibling is None and tag.previous_sibling is None
    return is_right_tag and is_nested and is_lone


browser = ms.Browser()

tokens = list()
for grp in ['-ac', '-dl', '-mr', '-sz']:
    page = browser.get('http://www.bacterio.net/%s.html' % grp)
    tokens.extend(each.get_text(strip=True)
                  for each in page.soup.find_all(find_genus_tag))
    time.sleep(2)

print '\n'.join(tokens)
