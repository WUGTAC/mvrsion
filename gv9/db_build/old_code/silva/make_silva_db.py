import re
import pdb
import pandas as pd
import cPickle as pickle
import sh
import argparse

from functools import partial
from itertools import izip
from path import path
from Bio import SeqIO

from gv9.util.ssu_util import RdpFactory, SsuDataBase, SeqID
import gv9.make_region_db

'''
This script should be run inside the desired output directory
first argument is path to taxmap file
second argument is path to unzipped fasta file
This script does not perform region extraction or make ssu_db.pickle
'''

def process_raw_read(seqid_to_taxid, rec):
    '''
    silva fasta files are RNA
    '''
    rec.id = str(seqid_to_taxid[rec.id])
    rec.seq = rec.seq.back_transcribe()
    return rec
    
    
def make_seqid_to_rdp(taxmap_fp):
    df = pd.read_table(
        taxmap_fp,
        sep='\t',
        index_col=0,
        names=['pacc', 'start', 'stop', 'lineage', 'name', 'taxid']
        )

    factory = RdpFactory()
    
    def _make_rdp(factory, _str):
        return factory.create_from_unlabeled(_str, token=';')

    
    def _make_full_rdp(rdp, name, factory=factory):
        #pdb.set_trace()
        rdp = rdp.tokens

        clean_name = re.sub(r'[\[\]]+', '', name)
        name_tokens = clean_name.split()
        rdp['s'] = name_tokens[1] if name_tokens[0] == rdp['g'] else ' '.join(name_tokens[:2])

        #here you filter out species named like Blah sp.  
        if not re.match(r'^[ A-Za-z]+$', rdp['s']):
            return None
            
        rdp['t'] = ' '.join(name_tokens[2:])
        
        return factory.create(rdp)
    

    def accept_bacteria(stop_words, rdp):
        '''
        Returns True if we should accept rdp string into our database
        Must return an honest to god Boolean for pandas indexing
        stop_words is a list of words that cause us to discard the rdp if found.
        
        Is called on the lineage first then the full rdp string
        '''
        x = rdp['k'] == 'Bacteria' and bool(rdp['f'])
        y = not bool(re.search(r'\sbacterium', rdp['s']))
        z = not bool(re.search(r'bacterium', rdp['t']))
        stopped = not any(re.search(word, rdp, flags=re.I) for word in stop_words)
        return x and y and z and stopped
    
        
    df['rdp'] = df['lineage'].apply(partial(_make_rdp, factory))

    stop_words = ['uncultured', 'environment', 'unclassified', 'symbiont']

    bacteria_df = \
      df[df['rdp'].map(partial(accept_bacteria, stop_words))]
    
    bac_dict = bacteria_df.to_dict('list')

    full_rdp = {}
    slv_seqid_to_SeqID = {}

    for seqid,rdp,name,start,stop,taxid in izip(
            bacteria_df.index, bac_dict['rdp'], bac_dict['name'], bac_dict['start'], bac_dict['stop'], bac_dict['taxid']):
        
        new = _make_full_rdp(rdp, name)
        slv_seqid = '.'.join([seqid, str(start), str(stop)])
        seqID = SeqID(taxid, seq=slv_seqid)
        
        if new and accept_bacteria(stop_words, new):
            full_rdp[seqID] = new
            slv_seqid_to_SeqID[slv_seqid] = seqID

    #pdb.set_trace()
    return full_rdp, slv_seqid_to_SeqID


def main():
    '''
    This script will only work with the taxmap_fp being the embl taxonomy file
    named for silva_119: taxmap_embl_ssu_ref_nr99_119.txt
    I used the fasta that has been filtered by clusterint at 99%
    This fasta should be called something like SILVA_119.1_SSURef_Nr99_tax_silva.fasta

    A slv_seqid is the first word in the fasta header ie: AB000278.1.1410
    
    A SeqID is what is used in gv9. This is something like 56192:AB000278.1.1410
     where 56192 is the embl id for a specific microbe taxonomy
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('taxmap_fp', type=path)
    parser.add_argument('raw_fasta', type=path)
    parser.add_argument('--region', action='store_true')

    args = parser.parse_args()
    
    fasta_fp = path('ref_16s.fna')
    taxonomy_fp = path('rdp_taxonomy.txt')
    ssu_db_fp = path('ssu_db.pickle')

    #seqID_to_rdp is a { SeqID : _RdpString }
    seqID_to_rdp,slv_seqid_to_SeqID = make_seqid_to_rdp(args.taxmap_fp)

    with open(taxonomy_fp, 'w') as fh:
        fh.writelines(
            sorted('\t'.join([str(seq), rdp]) + '\n'
                   for seq,rdp in seqID_to_rdp.iteritems()
                )
            )
        
    with open(args.raw_fasta, 'r') as fh_in, open(fasta_fp, 'w') as fh_out:

        out_seqs = (process_raw_read(slv_seqid_to_SeqID, seq)
                    for seq in SeqIO.parse(fh_in, 'fasta')
                    if seq.id in slv_seqid_to_SeqID)

        SeqIO.write(out_seqs, fh_out, 'fasta')

    ssu_db = SsuDataBase(taxonomy_fp)

    with open(ssu_db_fp, 'wb') as fh:
        pickle.dump(ssu_db, fh, -1)

    sh.novoindex(fasta_fp.namebase + '.ndx', fasta_fp, k=15, t=1)

    if args.region:
        gv9.make_region_db.main()


if __name__ == '__main__':
    main()
