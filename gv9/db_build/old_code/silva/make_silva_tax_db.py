import re
import cPickle as pickle
import sh
import argparse
import collections as cl
import itertools as it
import hashlib
import pdb

from path import path
from Bio import SeqIO

from gv9.util.ssu_util import RdpFactory, _RdpString, SsuDataBase, SeqID
from gtac.util import get_script_dir
import gv9.make_region_db

'''
This script takes as input only the silva fasta file. It does not use the taxmap files.
This is based off of the curated silva taxonomy
'''

SeqInfo = cl.namedtuple('SeqInfo', 'seqID,rdp')

class MiniRdp(_RdpString):
    rdp_label_hierarchy = ('s', 't')


class TaxIdMaker(object):
    def __init__(self):
        self.id = 1000
        self.dict = {}

    def make(self, rdp):
        try:
            return self.dict[rdp]
        except KeyError:
            self.dict[rdp] = self.id
            self.id += 1
            return self.dict[rdp]


class TaxIdMakerSpecies(object):
     def make(self, rdp):
        m = hashlib.md5()
        m.update(rdp.get_level('s'))
        return m.hexdigest()


def accept_and_format(stop_words, english_words, rdp):
    '''
    Returns True if we should accept rdp string into our database
    Must return an honest to god Boolean for pandas indexing
    stop_words is a list of words that cause us to discard the rdp if found.
    '''
    x = rdp['k'] == 'Bacteria' and bool(rdp['g'])
    y = not bool(re.search(r'^bacterium|\sbacterium\s', rdp['s'], flags=re.I))
    # z = not bool(re.search(r'sp\.', rdp['s']))
    stopped = not any(re.search(word, rdp, flags=re.I) for word in stop_words)
    if x and y and stopped:
        split_species = rdp['s'].split()
        num_words = 0
        for each in split_species:
            if re.match(r'^[a-zA-Z]+$', each):
                num_words += 1
            else:
                break

        if num_words >= 2 and split_species[0].lower() not in english_words:
            rdp.tokens['s'] = ' '.join(split_species[:2])
            return rdp.factory.create(rdp.tokens)


def make_silvaID_to_seqInfo(fasta_fp, species_only=False):
    taxMaker = TaxIdMakerSpecies() if species_only else TaxIdMaker()
    id_to_info = {}
    factory = RdpFactory()
    mini_factory = RdpFactory(MiniRdp)
    stop_words = ['uncultured', 'environment', 'unclassified', 'symbiont',
                  'unknown', 'unidentified']

    with open(get_script_dir().joinpath('..', 'genera.txt'), 'r') as fh:
        genera_set = set(line.strip().lower() for line in fh)

    with open('/usr/share/dict/words', 'r') as fh:
        english_words = set(line.strip().lower() for line in fh) - genera_set

    with open(fasta_fp, 'r') as fh:
        for record in SeqIO.parse(fh, 'fasta'):
            silvaID, taxa = record.description.split(' ', 1)
            clean_taxa = re.sub(r'[()\[\]\'"]+', '', taxa)
            # if 'Staphylococcus aureus' in clean_taxa:
            #     pdb.set_trace()

            rdp = accept_and_format(stop_words,
                                    english_words,
                                    factory.create_from_unlabeled(clean_taxa,
                                                                  token=';'))
            if rdp is not None:
                # replace rdp with MiniRdp if we pass species on commandline
                if species_only:
                    rdp = mini_factory.create(rdp)

                taxID = taxMaker.make(rdp)
                seqID = SeqID(taxID, silvaID)
                id_to_info[silvaID] = SeqInfo(seqID=seqID, rdp=rdp)

    return id_to_info


def process_raw_read(silvaID_to_seqInfo, rec):
    '''
    silva fasta files are RNA
    '''
    rec.id = str(silvaID_to_seqInfo[rec.id].seqID)
    rec.seq = rec.seq.back_transcribe()
    return rec


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('raw_fasta', type=path)
    parser.add_argument('--region', action='store_true')
    parser.add_argument('--species', action='store_true')
    parser.add_argument('-n', '--amp_len', type=int, default=100)

    args = parser.parse_args()

    fasta_fp = path('ref_16s.fna')
    taxonomy_fp = path('rdp_taxonomy.txt')
    ssu_db_fp = path('ssu_db.pickle')

    silvaID_to_seqInfo = make_silvaID_to_seqInfo(args.raw_fasta, species_only=args.species)

    with open(taxonomy_fp, 'w') as fh:
        fh.writelines(sorted('\t'.join([str(item.seqID), item.rdp]) + '\n'
                             for item in silvaID_to_seqInfo.itervalues()))

    with open(args.raw_fasta, 'r') as fh_in, open(fasta_fp, 'w') as fh_out:
        out_seqs = (process_raw_read(silvaID_to_seqInfo, seq)
                    for seq in SeqIO.parse(fh_in, 'fasta')
                    if seq.id in silvaID_to_seqInfo)

        SeqIO.write(out_seqs, fh_out, 'fasta')

    ssu_db = SsuDataBase(taxonomy_fp)

    with open(ssu_db_fp, 'wb') as fh:
        pickle.dump(ssu_db, fh, -1)

    sh.novoindex(fasta_fp.namebase + '.ndx', fasta_fp)

    if args.region:
        gv9.make_region_db.main(min_amplicon_len=args.amp_len)


if __name__ == '__main__':
    main()
