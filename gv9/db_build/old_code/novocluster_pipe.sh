#!/bin/bash -e

#$ -pe shared 4
#$ -P long
#$ -V
#$ -cwd
#$ -l h_vmem=5g

#This script performs the novoalign clustering necessary for the 16s pipeline
#Run this script from within the directory containing the 16s reference fasta
#  clustering is to be performed on.

refFiles=(ref_16s*.fna)
if [ ${#refFiles[@]} == 1 ]; then
    fna=${refFiles[0]}
    ndx="${fna%.*}.ndx"
else
    echo "more/less than one ref_16s* $(pwd)"
    exit 1
fi

mkdir regions
python /home/comp/gtac/aschriefer/src/ssu/make_region_db.py
mkdir cluster
python /home/comp/gtac/aschriefer/src/ssu/ref_reads.py

if [ ! -f $ndx ]; then
    novoindex -k 12 -s 1 $ndx $fna
else
    if [ $(stat -c %Y $ndx) -lt $(stat -c %Y $fna) ]; then
	novoindex -k 12 -s 1 $ndx $fna
    fi
fi

novoalign -c 4 -k -i PE 50-400 -d $ndx -H -F STDFQ -o SAM -n 150 -t 60 -r E 200 -R 5 -f cluster/ref_R1.fq.gz cluster/ref_R2.fq.gz | samtools view -bS - > cluster/align_all.tmp.bam
samtools sort cluster/align_all.tmp.bam cluster/align_all
samtools index cluster/align_all.bam
rm cluster/align_all.tmp.bam

python /home/comp/gtac/aschriefer/src/ssu/novoalign_cluster_pd.py