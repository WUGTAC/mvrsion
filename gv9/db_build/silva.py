import re
import itertools as it
from Bio.SeqRecord import SeqRecord
import gv9.db_build.base as base


class Parser(base.Parser):
    factory = base.RdpFactory()
    sep = ';'

    def get_seqInfo(self, record):
        nativeID, taxa = record.description.split(' ', 1)
        if not re.match('Bacteria;', taxa):
            raise base.RejectedSequence('Sequence not in Bacteria domain')

        # Take only species + strain info and remove weird characters
        species_info = taxa.split(';')[-1]
        clean_taxa = re.sub(r'[()\[\]\'"]+', '', species_info)

        m = re.match(r'(?P<species>\w+\s+\w+)(?P<strain>.*)', clean_taxa)
        if m is None:
            final_taxa = clean_taxa
        else:
            genus = m.group('species').split()[:1]
            final_taxa = self.sep.join(
                it.chain(genus, m.group('species', 'strain')))

        rdp = self.factory.create_from_unlabeled(final_taxa, token=self.sep)
        return base.SeqInfo(seqID=nativeID, rdp=rdp)

    def format(self, seqInfo, record):
        title = [str(seqInfo.seqID)]
        title.append(record.description.split(None, 1)[1])
        new_seq = record.seq.back_transcribe()
        return SeqRecord(new_seq, id='\t'.join(title), description='')
