#!/usr/bin/env python
import re
import os
import pkg_resources as pkg
from gv9.util.ssu_util import TaxID
from Bio import SeqIO
from path import path
from collections import Counter
import pandas as pd
import shutil
import sh
import itertools as it
import collections as cl
from datetime import timedelta

import ruffus
import drm
import gtac.ruffusUtil as ruffusUtil
'''
This script runs primer prospector using input ref_16s.fna.
The input primers are harcoded in.  Assumes that the cwd is same as dir containing
the ref_16s.fna
'''


def create_amplicons(fasta_fp, min_len=100):
    '''Operates on amplicon files inside the working dir'''
    cwd = path.getcwd()
    make_amplicons = sh.Command('get_amplicons_and_reads.py')
    for pair in get_hits_fp_pairs(cwd):
        make_amplicons('-f', fasta_fp, '-m', str(min_len), '-i',
                       ':'.join(pair))

    # rename amplicon files to format novoCluster.py expects
    rename_amplicon_reads(cwd)
    delete_fasta_reads(cwd)


def create_pcr_results(output_fp):
    cwd = path.getcwd()
    region_rec_dict = {re.search(r'V\d(?:_\d|-V\d)?', fp).group(0):
                       SeqIO.parse(
                           open(fp, 'r'), 'fasta')
                       for fp in cwd.glob('*.fna')}

    series_dict = {}
    for region, rec_list in region_rec_dict.iteritems():
        series_dict[region] = pd.Series(
            Counter(TaxID(rec.id).tax for rec in rec_list),
            dtype=float)

    df = pd.concat(series_dict, axis=1, join='outer')
    df.to_csv(output_fp, sep='\t', na_rep='0', index_label='taxid')
    return df


def delete_fasta_reads(cwd):
    for fp in cwd.glob('*reads.fasta'):
        fp.unlink()


def get_hits_fp_pairs(cwd):
    hits = sorted(cwd.glob('*hits.txt'))
    for _, grp in it.groupby(
            hits,
            key=lambda x: re.match(r'(.*?)[rf]_', x).group(1)):
        yield tuple(grp)


def rename_amplicon_reads(cwd):
    for fp in path(cwd).glob('*amplicons.fasta'):
        amp = re.search(r'(?:V\d+)+', fp).group(0)
        amp1 = re.sub('(\d)V', '\g<1>-V', amp)
        amp2 = re.sub('(\d)(\d)$', '\g<1>_\g<2>', amp1)
        new_fp = 'ref_16s%s.fna' % amp2
        print fp
        print new_fp
        shutil.move(fp, new_fp)


def parse_primer_file(primer_fp, fasta_fp):
    Primer = cl.namedtuple('Primer', 'name,seq')

    fna_name = fasta_fp.basename().stripext()
    primer_info = {}
    with open(primer_fp, 'r') as fh:
        for line in fh:
            primer = Primer(*line.strip().split())
            output_name = path('_'.join([primer.name, fna_name, 'hits.txt']))
            primer_info[output_name] = primer

    return primer_info


def finish():
    create_pcr_results(path.getcwd(), 'region_report.txt')


def main(min_amplicon_len=100):
    # primer_csv_fp = '/home/comp/gtac/aschriefer/src/ssu/primers.csv'
    # parser = ruffus.cmdline.get_argparse()
    # args = parser.parse_args()

    primer_fp = pkg.resource_filename('gv9.db_build',
                                      'data/primers_pprospector.txt')
    # primer_csv_fp = sys.argv[1]

    out_dir = path('pprospector/')
    out_dir.rmtree_p()
    out_dir.mkdir_p()

    fasta_fp = path('ref_16s.fna').abspath()

    # cd into the primer prospector output dir
    out_dir.cd()

    submit_job_lib = drm.get_drm_module()
    job_namer = lambda x: os.path.basename(x)

    auto_drm_job = ruffusUtil.auto_drm_job_factory(
        submit_job_lib.Submitter(script='jobs',
                                 log='log'),
        job_namer)

    ruffusUtil.remove_empty_files(path.getcwd())

    # needs the fasta file path to create the output file name
    hit_file_to_info = parse_primer_file(primer_fp, fasta_fp)

    @ruffus.originate(hit_file_to_info.keys(), fasta_fp, hit_file_to_info)
    def analyze_primers(output_file, fasta_fp, hit_file_to_info):
        # we do this because auto_drm_job names on the second arg
        @auto_drm_job
        def func(fake, output_file):
            primer = hit_file_to_info[output_file]
            cmd = 'analyze_primers.py -f {ref} -p {name} -s {seq}'.format(
                ref=fasta_fp,
                name=primer.name,
                seq=primer.seq, )
            resrc = submit_job_lib.Resource(time=timedelta(hours=2),
                                   memInGB=4,
                                   workers=1, )
            return cmd, resrc

        func('', output_file)

    @ruffus.collate(analyze_primers, ruffus.formatter(r'.*'), '.first',
                    fasta_fp, min_amplicon_len)
    @auto_drm_job
    def get_amplicons(infiles, outfile, fasta_fp, min_len):
        cmd = '''
        python -c "from gv9.db_build.pcr import *; create_amplicons('{0}', min_len={1})"
        '''.format(fasta_fp, min_len)
        resrc = submit_job_lib.Resource(time=timedelta(hours=1), memInGB=2, workers=1, )
        return cmd, resrc

    @ruffus.transform(get_amplicons, ruffus.formatter(r'.*'),
                      'region_report.txt')
    @auto_drm_job
    def get_pcr_report(infiles, outfile):
        cmd = '''
        python -c 'from gv9.db_build.pcr import *; create_pcr_results("{0}")'
        '''.format(outfile)

        resrc = submit_job_lib.Resource(time=timedelta(minutes=20),
                               memInGB=4,
                               workers=1, )
        return cmd, resrc

    ruffus.pipeline_run(checksum_level=0)


if __name__ == '__main__':
    main()
