#-*- coding: utf-8 -*-
import mechanicalsoup as ms
import itertools as it
import time
import re


def find_link_tag(tag):
    return tag.name == 'a' and tag.string == u'¤'


def get_all_species(start_tag):
    '''
    Removes run of same species with diff subsp tags
    '''
    bad_pattern = re.compile(r'[^A-Za-z ]')
    prev = None
    last_name = None
    for tag in start_tag.find_next_siblings('span'):
        if tag['class'] == ['specificepithet']:
            genus = prev.get_text(strip=True)
            species = tag.get_text(strip=True)
            name = ' '.join((genus, species))
            if name != last_name and not bad_pattern.search(name):
                last_name = name
                yield name
        else:
            prev = tag


browser = ms.Browser(soup_config={'features': 'html5lib'})

for grp in ['ac', 'dl', 'mr', 'sz']:
    page = browser.get('http://www.bacterio.net/-allnames%s.html' % grp)
    for sp in it.chain.from_iterable(
            get_all_species(each)
            for each in page.soup.find_all(find_link_tag)):
        print sp

    time.sleep(2)
