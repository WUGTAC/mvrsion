import argparse
import heapq
import collections as cl
import cPickle as pickle
import re
import sys
import shutil

import sh
from path import path
from Bio import SeqIO

import gv9.db_build.pcr
from gv9.util.ssu_util import ReferenceDirectory, SsuDataBase, SeqID


def create_rdp_taxonomy(ssu_db, fasta_iter, output_handle, filter_mapping=None):
    '''
    Assumes that input fasta has already been formated to have valid
    SeqID strings as header ids via format_fasta
    '''
    filter_mapping = (filter_mapping or {})
    seqId_iter = (SeqID(rec.id) for rec in fasta_iter)
    output_handle.writelines(sorted('\t'.join([str(seq), ssu_db[seq] + '\n'])
                                    for seq in seqId_iter
                                    if seq not in filter_mapping))

def format_fasta(in_fh, out_fh):
    def _format(fh):
        for rec in SeqIO.parse(in_fh, 'fasta'):
            d = re.sub(r'^\S+\|', '', rec.description)
            rec.id = d.split(' ', 1)[0]
            rec.description = d
            yield rec
    SeqIO.write(_format(in_fh), out_fh, 'fasta')
    

def merge_sequence_taxa(handle_list, output_handle):
    '''
    This algorithm assumes that the types of SeqID.seq
    differentiation is different between the databases
    so the records can be written exactly without duplication
    Assumes all handles have at least one line in them at the beginning
    Assumes that all the handle files are sorted by first column SeqID
    '''
    Item = type('Item', (cl.namedtuple('Item', 'taxid,rdp'),), {})
    heap = [Item(*fh.readline().split('\t')) for fh in handle_list]
    for i, item in enumerate(heap):
        item.index = i

    heapq.heapify(heap)
    while heap:
        min_item = heap[0]
        output_handle.write('\t'.join(min_item))

        new_record = handle_list[min_item.index].readline()
        if new_record:
            new_item = Item(*new_record.split('\t'))
            new_item.index = min_item.index
            heapq.heappushpop(heap, new_item)
        else:
            heapq.heappop(heap)


def main():
    '''
    This function should be run from within the desired output directory
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('base', type=ReferenceDirectory)
    parser.add_argument('addition', type=ReferenceDirectory)
    parser.add_argument('--region', action='store_true')
    parser.add_argument('-n', '--amp_len', type=int, default=100)
    parser.add_argument('--only_new', action='store_true')

    args = parser.parse_args()

    combined_ref_dir = ReferenceDirectory(path.getcwd())

    # We create a temp subdir to hold all the intermediate files that won't
    # be used in the final database
    tmp_dir = combined_ref_dir.path.joinpath('tmp')
    tmp_dir.mkdir_p()
    tmp_dir.cd()

    tmp_taxonomy = tmp_dir.joinpath('taxonomy.txt')

    # uclust dies if the input fasta files are sorted by length
    sh.uclust('--sort', args.base.fasta(), '--output', 'base_sorted.fasta')
    sh.uclust('--sort', args.addition.fasta(), '--output', 'add_sorted.fasta')

    # first we cluster the addition database
    sh.uclust('--input', 'add_sorted.fasta', '--uc', 'de_novo_results.uc',
              '--id', '0.99')
    sh.uclust('--uc2fasta', 'de_novo_results.uc', '--input',
              'add_sorted.fasta', '--output', 'de_novo_addition.fasta',
              '--types', 'S')

    # now we take the de novo results and cluster them into the base.
    # This presumably will only capture seqs from addition
    sh.uclust('--input', 'de_novo_addition.fasta', '--lib',
              'base_sorted.fasta', '--uc', 'results.uc', '--id', '0.99')
    sh.uclust('--uc2fasta', 'results.uc', '--input', 'de_novo_addition.fasta',
              '--output', 'results.fasta', '--types', 'S')

    # We need to format results.fasta to remove the crap that uclust adds the the header name
    # in order to generate valid SeqIDs
    with open('results.fasta', 'r') as fh_in, open('final.fasta', 'w') as fh_out:
        format_fasta(fh_in, fh_out)
    
    # now we create a rdp_taxonomy.txt file including only the selected seqs from the
    # additional database
    # create the combined fasta file
    # Only add seqs from final.fasta that are in the combined SsuDataBase
    shutil.copyfile(args.base.fasta(), combined_ref_dir.fasta())
    with open('final.fasta', 'r') as fasta, open(tmp_taxonomy, 'w') as taxa, open(combined_ref_dir.fasta(), 'a') as out_fasta:
        add_ssu_db = pickle.load(open(args.addition.db(), 'rb'))
        base_ssu_db = pickle.load(open(args.base.db(), 'rb'))

        if args.only_new:
            create_rdp_taxonomy(add_ssu_db, SeqIO.parse(fasta, 'fasta'), taxa, filter_mapping=base_ssu_db)
            fasta.seek(0)
            seq_iter = (rec for rec in SeqIO.parse(fasta, 'fasta')
                        if rec.id not in base_ssu_db)
        else:
            create_rdp_taxonomy(add_ssu_db, SeqIO.parse(fasta, 'fasta'), taxa)
            fasta.seek(0)
            seq_iter = (rec for rec in SeqIO.parse(fasta, 'fasta'))
        SeqIO.write(seq_iter, out_fasta, 'fasta')

    # check to make sure we have some new sequences to add else end the program
    if tmp_taxonomy.size == 0:
        sys.exit('no new sequences to add, aborting execution')

    # create a combined rdp_taxonomy.txt (which remains sorted on SeqID
    with open(args.base.taxa(), 'r') as in1, open(
            tmp_taxonomy, 'r') as in2, open(combined_ref_dir.taxa(),
                                            'w') as out:
        merge_sequence_taxa([in1, in2], out)

    # create a combined SsuDataBase
    ssu_db = SsuDataBase(combined_ref_dir.taxa())
    with open(combined_ref_dir.db(), 'wb') as fh:
        pickle.dump(ssu_db, fh, -1)

    sh.novoindex(combined_ref_dir.index(), combined_ref_dir.fasta())
    if args.region:
        # We need to cd back into the main database directory for make_region_db
        combined_ref_dir.path.cd()
        gv9.db_build.pcr.main(min_amplicon_len=args.amp_len)


if __name__ == '__main__':
    main()
