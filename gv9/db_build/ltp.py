import re
import gv9.db_build.base as base
from Bio.SeqRecord import SeqRecord


class Parser(base.Parser):
    factory = base.miniFactory

    def get_seqInfo(self, record):
        header = record.description.split('\t')
        nativeID = '.'.join(header[:3])
        m = re.match(r'(?P<species>\w+\s+\w+)(?P<strain>.*)', header[5])
        rdp = self.factory.create_from_unlabeled(';'.join(m.group('species',
                                                                  'strain')))
        return base.SeqInfo(seqID=nativeID, rdp=rdp)

    def format(self, seqInfo, record):
        title = [str(seqInfo.seqID)]
        title.extend(record.description.split('\t')[5:])
        new_seq = record.seq.back_transcribe()
        return SeqRecord(new_seq, id='\t'.join(title), description='')
