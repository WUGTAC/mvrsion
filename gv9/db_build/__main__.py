import pkg_resources as pkg
import cPickle as pickle
from io import StringIO
import functools
import click
import sh
from Bio import SeqIO

import gv9.util.ssu_util as ssu_util
import gv9.db_build
import gv9.db_build.pcr


def build_protocol(ref_dir=None,
                   parser=None,
                   acceptor=None,
                   fasta_fh=None,
                   finalizer=None,
                   mode='w',
                   **kwargs):
    # Im not crazy, SeqIO just likes its iterators
    def loop():
        with open(ref_dir.taxa(), mode) as tax_out:
            for rec in SeqIO.parse(fasta_fh, 'fasta'):
                try:
                    native_seqInfo = parser.get_seqInfo(rec)
                except gv9.db_build.RejectedSequence:
                    continue

                if acceptor.accept(native_seqInfo):
                    gv9_seqInfo = finalizer.finalize(native_seqInfo)
                    write_taxonomy_line(tax_out, gv9_seqInfo)
                    yield parser.format(gv9_seqInfo, rec)

    with open(ref_dir.fasta(), mode) as fa_out:
        SeqIO.write(loop(), fa_out, 'fasta')


def make_SpeciesAcceptor():
    with open(pkg.resource_filename('gv9.db_build', 'data/species.txt'),
              'r') as fh:
        acceptor = gv9.db_build.SpeciesAcceptor(fh)
        return acceptor


def make_GenusAcceptor():
    with open(pkg.resource_filename('gv9.db_build', 'data/genera.txt'),
              'r') as fh:
        acceptor = gv9.db_build.GenusAcceptor(fh)
        return acceptor


def remove_strain(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        tokens = func(*args, **kwargs)
        tokens['t'] = ''
        return tokens

    return wrapper


def set_species_to_genus(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        tokens = func(*args, **kwargs)
        tokens['s'] = tokens['g']
        tokens['t'] = ''
        return tokens

    return wrapper


def set_non_species_empty(func, acceptor):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        tokens = func(*args, **kwargs)
        info = gv9.db_build.SeqInfo(None, tokens)
        if not acceptor.accept(info):
            tokens['s'] = ''
        tokens['t'] = ''
        return tokens

    return wrapper


def multilevel_protocol_tuples():
    full_dir = ssu_util.MultiLevelReferenceDirectory()
    d = [full_dir, full_dir.genus(), full_dir.species()]
    g_accept = make_GenusAcceptor()
    sp_accept = make_SpeciesAcceptor()
    a = [g_accept, g_accept, sp_accept]

    f = [
        gv9.db_build.Finalizer(edit_fn=set_non_species_empty(lambda x: x,
                                                             sp_accept)),
        gv9.db_build.Finalizer(edit_fn=set_species_to_genus(lambda x: x)),
        gv9.db_build.Finalizer(edit_fn=remove_strain(lambda x: x))
    ]
    return zip(d, a, f)


def write_taxonomy_line(fh, seqInfo):
    fh.write('{seqID}\t{rdp}\n'.format(**vars(seqInfo)))


@click.group(chain=True)
@click.option('--genus', is_flag=True)
@click.pass_context
def main(ctx, genus):
    '''
    All output files will be written inside the working directory
    '''
    ctx.obj = {'genus': genus}
    if genus:
        ctx.obj['edit'] = set_species_to_genus
    else:
        ctx.obj['edit'] = remove_strain


@main.command('ltp')
@click.argument('fasta', type=click.File('rb'))
def ltp_cli(fasta):
    from gv9.db_build import ltp
    parser = ltp.Parser()
    acceptor = gv9.db_build.UnconditionalAcceptor()
    build_protocol(fasta_fh=fasta, **locals())


@main.command('rdp')
@click.argument('fasta', type=click.File('rb'))
def rdp_cli(fasta):
    from gv9.db_build import rdp
    parser = rdp.Parser()
    acceptor = make_SpeciesAcceptor()
    build_protocol(fasta_fh=fasta, **locals())


@main.command('silva')
@click.argument('fasta', type=click.File('rb'))
@click.pass_context
def silva_cli(ctx, fasta):
    from gv9.db_build import silva
    parser = silva.Parser()
    for ref_dir, acceptor, finalizer in multilevel_protocol_tuples():
        ref_dir.create()
        fasta.seek(0)
        build_protocol(fasta_fh=fasta, mode='w', **locals())


@main.command('add')
@click.argument('fasta', type=click.File('rb'))
def add(fasta):
    parser = gv9.db_build.BasicParser()
    acceptor = gv9.db_build.UnconditionalAcceptor()

    full_dir = ssu_util.MultiLevelReferenceDirectory()
    d = [full_dir, full_dir.genus(), full_dir.species()]

    finals = [
        gv9.db_build.Finalizer(edit_fn=remove_strain(lambda x: x)),
        gv9.db_build.Finalizer(edit_fn=set_species_to_genus(lambda x: x)),
        gv9.db_build.Finalizer(edit_fn=remove_strain(lambda x: x))
    ]

    for ref_dir, finalizer in zip(d, finals):
        fasta.seek(0)
        build_protocol(fasta_fh=fasta, mode='a', **locals())


@main.command('check')
@click.argument('names', type=click.File('r'))
def check(names):
    ref_dir = ssu_util.ReferenceDirectory()
    cmd = '''
while read p; 
do echo -e "$p\t$(grep -c -m 1 -i "$p" {taxa})";
done'''.format(taxa=ref_dir.taxa())

    buf = StringIO()
    sh.bash(c=cmd, _in=names, _out=buf)
    output = buf.getvalue()

    print output
    if '0' not in output:
        print 'All microbes present. Yay!'
    else:
        print 'Some microbes missing (T_T)'


@main.command('finish')
@click.option('-n', '--amp_len', type=int, default=100)
def finish(amp_len):
    ref_dir = ssu_util.ReferenceDirectory()
    factory = gv9.db_build.RdpFactory()
    ssu_db = ssu_util.SsuDataBase(
        ref_dir.taxa(), rdp_constructor=factory.create)

    with open(ref_dir.db(), 'wb') as fh:
        pickle.dump(ssu_db, fh, -1)

    sh.novoindex(ref_dir.index(), ref_dir.fasta())
    gv9.db_build.pcr.main(min_amplicon_len=amp_len)


if __name__ == '__main__':
    main()
