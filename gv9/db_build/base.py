import collections as cl
import gv9.util.ssu_util as ssu_util


class SpeciesRdp(ssu_util._RdpString):
    rdp_label_hierarchy = ('s', 't')


class GenusRdp(ssu_util._RdpString):
    rdp_label_hierarchy = ('g', 's', 't')


class RdpFactory(ssu_util.RdpFactory):
    '''
    This is the factory that should be used to parse the rdp_taxonomy.txt
    file into the SsuDataBase object
    '''

    def __init__(self):
        super(RdpFactory, self).__init__(constructor=GenusRdp)


SeqInfo = cl.namedtuple('SeqInfo', 'seqID,rdp')


class RejectedSequence(Exception):
    pass


class Parser(object):
    def get_seqInfo(self, record):
        '''
        :param `Bio.SeqRecord.SeqRecord` record:
        Parse the fasta header for database native ID and taxonomy
        To reject the sequence straight away raise RejectedSequence
        '''
        raise NotImplementedError()

    def format(self, seqInfo, record):
        '''
        :param Bio.SeqRecord.SeqRecord record:
        :param SeqInfo seqInfo:
        :return: Bio.SeqRecord.SeqRecord
        '''
        raise NotImplementedError()


class Acceptor(object):
    '''
    This class is used to implement methods to accept or reject
    a sequence into the final database.
    '''

    def accept(self, seqInfo):
        raise NotImplementedError()
