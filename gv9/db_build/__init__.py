import hashlib
import re
from Bio.SeqRecord import SeqRecord
from gv9.db_build import base
from gv9.db_build.base import SeqInfo, RdpFactory, RejectedSequence
import gv9.util.ssu_util as ssu_util


class Finalizer(object):
    '''
    This class takes the native seqInfo and converts it to a form
    that is expected by the gv9 database files.
    edit_fn has signature OrderedDict -> OrderedDict
    '''
    factory = RdpFactory()

    def __init__(self, edit_fn=None):
        '''
        edit_fn has a 
        '''
        self.edit_fn = edit_fn if edit_fn else lambda x: x

    def create(self, rdp):
        rdp_tokens = self.edit_fn(rdp.tokens)
        return self.factory.create(rdp_tokens)

    def finalize(self, seqInfo, edit_fn=None):
        '''
        :param SeqInfo seqInfo:
        :return: SeqInfo
        '''
        return self.rdp_hash(rdp=self.create(seqInfo.rdp), seqID=seqInfo.seqID)

    def rdp_hash(self, rdp, seqID):
        m = hashlib.md5()
        m.update(rdp)
        new_seqID = ssu_util.SeqID(m.hexdigest(), seqID)
        return SeqInfo(new_seqID, rdp)


class SpeciesAcceptor(base.Acceptor):
    def __init__(self, known_species):
        self.species = set(self._clean(e) for e in known_species)

    def accept(self, seqInfo):
        s = self._clean(seqInfo.rdp['s'])
        return (bool(s) and (s in self.species))

    def _clean(self, s):
        return s.strip().lower()


class GenusAcceptor(base.Acceptor):
    def __init__(self, known_genera):
        self.genera = set(self._clean(e) for e in known_genera)

    def accept(self, seqInfo):
        g = self._clean(seqInfo.rdp['g'])
        return (bool(g) and (g in self.genera))

    def _clean(self, s):
        return s.strip().lower()


class UnconditionalAcceptor(base.Acceptor):
    def accept(self, seqInfo):
        return True


class BasicParser(base.Parser):
    factory = RdpFactory()
    sep = ';'

    def get_seqInfo(self, record):
        nativeID, taxa = self._split(record)
        genus = taxa.split()[0]
        species = taxa
        final_taxa = self.sep.join([genus, species])
        rdp = self.factory.create_from_unlabeled(final_taxa, token=self.sep)
        return SeqInfo(seqID=nativeID, rdp=rdp)

    def format(self, seqInfo, record):
        title = [str(seqInfo.seqID)]
        _, raw_taxa = self._split(record)
        title.append(raw_taxa)
        try:
            new_seq = record.seq.back_transcribe()
        except ValueError:
            new_seq = record.seq
        return SeqRecord(new_seq, id='\t'.join(title), description='')

    def _split(self, record):
        nativeID, taxa = re.split(r'\s+', record.description, maxsplit=1)
        return nativeID, taxa
