import re
from Bio.SeqRecord import SeqRecord
import gv9.db_build.base as base


class Parser(base.Parser):
    factory = base.miniFactory

    def get_seqInfo(self, record):
        m = re.match(r'(\w+) (.*?)\t', record.description)
        nativeID, species = m.group(1,2)
        if not re.search(r'Bacteria;domain', record.description):
            raise base.RejectedSequence('sequence not in Bacteria domain')

        rdp = self.factory.create_from_unlabeled(species)
        return base.SeqInfo(seqID=nativeID, rdp=rdp)

    def format(self, seqInfo, record):
        title = [str(seqInfo.seqID)]
        title.append(record.description.split(None, 1)[1])
        return SeqRecord(record.seq, id='\t'.join(title), description='')
