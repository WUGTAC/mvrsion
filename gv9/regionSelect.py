import collections as cl
import functools as ft
import itertools as it
import logging

import gtac.util
import pandas as pd
import pysam

import gv9.novoCluster as novo
import gv9.util.ssu_util as util


class RegionMultimap(object):

    Record = cl.namedtuple('Record', 'read,region,ref')
    """read and ref are rdp strings and region is a region name

    :param iter record_iter: Iterable of RegionMultimap.Record tuples, generally
        created from cls.read_bam. 
    :param list species_list: List of RdpStrings. Only these entities will have models made.
      Entities that are not present in in_silico_pcr are ignored
    :param dict in_silico_pcr: Dict of species_name to list of amplicons that are predicted
        to amplify for that species. This dict will have info for every single bug in the database.
    """

    def __init__(self,
                 record_iter,
                 species_list=None,
                 in_silico_pcr=None,
                 region_set=None):
        self.data = {
            sp:
            {r: set()
             for r in region_set.intersection(in_silico_pcr[sp])}
            for sp in species_list if sp in in_silico_pcr
        }

        for rec in record_iter:
            if rec.read != rec.ref:
                try:
                    self.data[rec.read][rec.region].add(rec.ref)
                except KeyError:
                    pass

    @classmethod
    def from_bam(cls, bam_fp, ssu_db, in_silico_pcr=None, **kwargs):
        with pysam.AlignmentFile(bam_fp, 'rb') as bam:
            ref_set = cls.get_reference_species(ssu_db, bam)
            return cls(cls.read_bam(bam, ssu_db),
                       species_list=ref_set,
                       in_silico_pcr=in_silico_pcr,
                       **kwargs)

    @classmethod
    def from_existing(cls, other):
        obj = cls.__new__(cls)
        obj.data = other.data
        return obj

    @classmethod
    def from_data(cls, data):
        obj = cls.__new__(cls)
        obj.data = data
        return obj

    @staticmethod
    def get_reference_species(ssu_db, bam):
        return set(ssu_db.get_species(r) for r in bam.references)

    @classmethod
    @gtac.util.memoize
    def get_species(cls, ssu_db, _id):
        return ssu_db.get_species(_id)

    @classmethod
    @gtac.util.memoize
    def get_regionID(cls, info):
        return util.RegionID(info)

    @classmethod
    def read_bam(cls, bam, ssu_db):
        for algn in bam.fetch(until_eof=True):
            #does proper pair have meaning in all alignment?
            if algn.is_proper_pair and algn.is_read1:
                #this is where read mapped to
                rname = bam.getrname(algn.tid)
                ref_rdp = cls.get_species(ssu_db, rname)

                info = algn.qname.split(' ')[0]
                read_regionID = cls.get_regionID(info)

                read_rdp = cls.get_species(ssu_db, read_regionID)

                #do not add mapping within species
                if ref_rdp == read_rdp:
                    continue
                else:
                    yield cls.Record(read_rdp, read_regionID.region, ref_rdp)

    def find_minimal_intersect(self, N=4):
        '''
        Must be called after read_bam()
        N is the maximum number of primers you want to include in the model.
        Returns a dict with {type(self.data.keys()) : set(region_names)}
        The keys of self.data here referred to as taxID can be ssu_util.TaxID or rdp strings
        In the resulting model the idea is to require all regions in the set to be present
        '''
        total_chosen_regions = {}
        total_taxid = set(self.data.iterkeys())
        for taxID, region_map in self.data.iteritems():

            chosen_regions = set()
            map_set = total_taxid

            while (len(map_set) > 0 or len(chosen_regions) > N):
                region_map_iter = (e for e in region_map.iteritems()
                                   if e[0] not in chosen_regions)
                best_region = self.select_best_region(map_set, region_map_iter)
                #select_best_region has signaled there is no improvements left
                if best_region is None:
                    break

                chosen_regions.add(best_region)
                map_set = map_set.intersection(region_map[best_region])

            total_chosen_regions[taxID] = chosen_regions

        return total_chosen_regions

    @staticmethod
    def select_best_region(model_set, region_info_iter):
        '''
        model_set is the intersect of all regions currently chosen for the model
        region_info_iter is an iterable containing (region_name, multimap_set) tuples
        '''
        model_intersected = {
            r: model_set.intersection(m)
            for r, m in region_info_iter
        }

        #stop building model if we have run out of regions
        if len(model_intersected) == 0:
            return None

        elif len(model_intersected) == 1:
            best_region = model_intersected.keys()[0]

        else:
            intersect_dict = cl.defaultdict(dict)
            for r1, r2 in it.combinations(model_intersected.iteritems(), 2):
                intersect_length = len(r1[1].intersection(r2[1]))
                intersect_dict[r1[0]][r2[0]] = intersect_dict[r2[0]][r1[
                    0]] = intersect_length

            avg = pd.np.log(pd.DataFrame(intersect_dict).mean() + 2)
            region_scores = pd.Series(
                {r: len(m)
                 for r, m in model_intersected.iteritems()}).mul(avg)
            best_region = region_scores.nsmallest(1).index[0]

        #if there is no improvement in intersection, stop the model building
        return best_region if len(model_intersected[best_region]) < len(
            model_set) else None

    @classmethod
    def parse_pcr_results(cls, results, ssu_db, min_fraction=0.80):
        '''
        results is a pd.DataFrame or a path to tab separated table
        input table has columns named after regions and index of TaxID.
        The values represent the number of sequences for that taxID that where
        successfully amplified by in-silico pcr

        min_fraction is the % of strains for a species that need to
        have at least one sequence amplified in order to declare the species as amplified
        taxids get aggregated into species
        '''

        def get_species(ssu_db, _id):
            return ssu_db[_id].take_level('s')

        def aggregate_species(df):
            return df.apply(lambda x: len(x[x > 0]) / float(len(x)), axis=0)

        if isinstance(results, pd.DataFrame):
            df = results
        else:
            df = pd.read_table(results, sep='\t', index_col=0)

        df['tax'] = df.index.map(ft.partial(get_species, ssu_db))

        species_df = df.groupby('tax').apply(aggregate_species)
        species_df.drop('tax', inplace=True, axis=1)

        stacked_df = species_df.stack()

        sp_to_region = cl.defaultdict(list)
        for sp, region in stacked_df[stacked_df >=
                                     min_fraction].index.tolist():
            sp_to_region[sp].append(region)

        sp_to_region.default_factory = None

        return sp_to_region

    @classmethod
    def parse_pcr_results_seq_level(cls, results, ssu_db, min_fraction=0.80):
        '''
        results is a pd.DataFrame or a path to tab separated table
        input table has columns named after regions and index of TaxID.
        The values represent the number of sequences for that taxID that where
        successfully amplified by in-silico pcr

        min_fraction is the % of strains for a species that need to
        have at least one sequence amplified in order to declare the species as amplified
        taxids get aggregated into species
        '''

        def get_species(ssu_db, _id):
            return ssu_db[_id].take_level('s')

        if isinstance(results, pd.DataFrame):
            df = results
        else:
            df = pd.read_table(results, sep='\t', index_col=0)

        df['tax'] = df.index.map(ft.partial(get_species, ssu_db))
        df = df.set_index('tax')
        stacked_df = df.div(df.max(axis=1), axis=0).stack()
        sp_to_region = cl.defaultdict(list)
        for sp, region in stacked_df[stacked_df >=
                                     min_fraction].index.tolist():
            sp_to_region[sp].append(region)

        sp_to_region.default_factory = None

        return sp_to_region


class GreedyRegionMultimap(RegionMultimap):
    @staticmethod
    def select_best_region(model_set, region_info_iter):
        try:
            best_region, set_ = min(
                ((r, set_) for r, set_ in region_info_iter),
                key=lambda x: (len(model_set.intersection(x[1])), len(x[1])))
        except ValueError:  #there are no regions left so quit building model
            return None

        return best_region if len(model_set.intersection(set_)) < len(
            model_set) else None


class MultiModelRegionMultimap(GreedyRegionMultimap):
    '''
    Return value here is {species_name : list(set(amplicon_names))}
    Each value of the list is a group of amplicons that can be used to call
    that species
    '''

    def find_minimal_intersect(self, **kwargs):
        '''
        Must be called after read_bam()
        N is the maximum number of primers you want to include in the model.
        Returns a dict with {type(self.data.keys()) : set(region_names)}
        The keys of self.data here referred to as taxID can be ssu_util.TaxID or rdp strings
        In the resulting model the idea is to require all regions in the set to be present
        '''
        total_chosen_regions = {}
        total_taxid = set(self.data.iterkeys())
        for taxID, region_map in self.data.iteritems():

            used_regions = set()
            model_list = []

            while (True):
                chosen_regions = set()
                map_set = total_taxid

                while (len(map_set) > 0):
                    region_map_iter = (e for e in region_map.iteritems()
                                       if e[0] not in chosen_regions and e[0]
                                       not in used_regions)

                    best_region = self.select_best_region(map_set,
                                                          region_map_iter)
                    #select_best_region has signaled there is no improvements left
                    if best_region is None:
                        break

                    chosen_regions.add(best_region)
                    map_set = map_set.intersection(region_map[best_region])

                    #We have found a perfect model so no need to continue
                    if len(map_set) == 0:
                        break

                #Note that here it is possible that a species has no model
                if len(map_set) == 0:
                    model_list.append(chosen_regions)
                    #Do not re-use chosen regions in other models
                    used_regions.update(chosen_regions)
                else:
                    break

            total_chosen_regions[taxID] = model_list

        return total_chosen_regions


class BackupRegionMultimap(MultiModelRegionMultimap):
    """This class choo
    """

    def find_minimal_intersect(self, N=4, **kwargs):
        total_chosen_regions = super(BackupRegionMultimap,
                                     self).find_minimal_intersect()
        for taxID, model_list in total_chosen_regions.iteritems():
            if not any(m for m in model_list):
                total_chosen_regions[taxID] = self.make_backup_model(
                    taxID, N=N)

        return total_chosen_regions

    def make_backup_model(self, taxID, N=4):
        return [
            set(r
                for r, _ in sorted(
                    self.data[taxID].iteritems(), key=lambda x: len(x[1]))[:N])
        ]


class SoftBackupMultimap(BackupRegionMultimap):
    """This class makes a backup model of N+1 choose N region sets.
    It also allows you to include species from multimap sets based off
    of a list of rdp strings.
    """

    def find_minimal_intersect(self, N=4, called_rdp=None, **kwargs):
        '''
        Species not in called_rdp are removed from the keys and value sets of
        the returned model dictionary
        '''
        if called_rdp is not None:
            called_set = set(called_rdp)
            new_data = {}
            for rdp in called_set:
                new_data[rdp] = {
                    r: _set.intersection(called_set)
                    for r, _set in self.data[rdp].iteritems()
                }

            obj = self.from_data(new_data)
        else:
            obj = self
        return super(SoftBackupMultimap, obj).find_minimal_intersect(N=N)

    def make_backup_model(self, taxID, N=4):
        sort_data = sorted(
            self.data[taxID].iteritems(), key=lambda x: len(x[1]))
        sort_regions = [each[0] for each in sort_data]
        return [
            set(r_lst) for r_lst in it.combinations(sort_regions[:N + 1], N)
        ]


def select_best_regions(novoalign_opts,
                        ssu_db,
                        all_regions,
                        ref_dir=None,
                        taxid=None,
                        output_fp=None):
    '''
    :param string novoalign_opts: arguments to run novoalign
    :param SsuDatabase ssu_db:
    :param list all_regions: list of region name strings
    :param ReferenceDirectory ref_dir:
    :param set taxid:
    :param path output_fp:
    :return: dictionary of species name to list of sets of species name
    output_fp is the absolute path where the bam file should be optionally saved
    '''
    assert hasattr(all_regions, '__iter__')

    if (taxid is not None) and (
            not all(isinstance(t, util.TaxID) for t in taxid)):
        logging.warning('Not all taxid are ssu_util.TaxID : ' + ' '.join(
            [repr(t) for t in taxid]))

    taxID_set = set(taxid) if taxid is not None else None
    region_set = set(all_regions)

    # only need to change if you change parse_pcr_results behavior in a subclass
    pcr_results = RegionMultimap.parse_pcr_results(
        ref_dir.region().joinpath('region_report.txt'),
        ssu_db,
        min_fraction=0.8)

    with novo.create_tmp_bam(
            novoalign_opts,
            ref_dir.fasta(),
            ref_dir.region(),
            taxID_set,
            all_regions=region_set,
            output_fp=output_fp) as bam_fp:

        region_map = SoftBackupMultimap.from_bam(
            bam_fp, ssu_db, region_set=region_set, in_silico_pcr=pcr_results)

    return region_map
