import pdb
import shutil
import sh
import pandas as pd
import numpy as np
import re
import os
import sys
import pysam
import cPickle as pickle
from tempfile import mkstemp, NamedTemporaryFile
from Bio import SeqIO
from gv9.util.ssu_util import TaxID, SeqID, RegionID
from gtac.util import temp_file, get_unique_file
from collections import defaultdict
from itertools import izip


def _single_region():
    return {'map': defaultdict(int)}


def _region_to_map():
    return defaultdict(_single_region)


#exclude regions not in seq1 from find_compare and make model
#drop trusted seqs not in region from multimap
class NovoMappingPanda(object):
    def __init__(self, region_lst):
        '''
        seq_lst is a iterable of seqIDs
        ssu_db is a SsuDataBase object
        region_lst is a list of the names of all the region amplicons
        region_dict is a dict of seqID to list of names of regions that were
        successfully extracted from full reference.

        To fully create this object you must call add_alignment() using
        every alignment from novoalign. Then you must call normalize()
        '''
        self.regions = region_lst

        self.mapping = defaultdict(_region_to_map)
        #get rid of region_dict, modify this by add_alignment
        # self.mapping = {
        #     seq: {
        #         r: {'map': defaultdict(int)}
        #         for r in self.regions
        #     }
        #     for seq in seq_lst
        # }

    def add_alignment(self, read_region_id, map_seq_id):
        read_seq_id = SeqID(read_region_id)
        self.mapping[read_seq_id][read_region_id.region]['map'][str(
            map_seq_id)] += 1
        return self

    def find_compare(self, seq_list, thres, regions=''):
        if not regions:
            regions = self.regions

        compare_list = []
        lextend = compare_list.extend
        for seq in seq_list:
            candidates = [c for c in seq_list if c != seq]
            lextend([(seq, c)
                     for c in candidates
                     if max([self[(seq, r, c)] for r in regions]) >= thres])

        return compare_list

    #maybe add dynamic thresholding for pipe_ssu.model_eval based on simple
    #multimapping procedure
    def make_model(self, comp_list, seq_df, top=3, peer_threshold=0.1):

        model_dict = {}
        for seq1, seq2 in comp_list:
            reg_scores = [
                (r, self[(seq1, r, seq2)])
                for r in seq_df.columns
                if self.is_amplified(seq1, r) and self.is_amplified(seq2, r)
            ]

            model_regions = [r
                             for r, score in sorted(reg_scores,
                                                    key=lambda x: x[1])
                             if score <= peer_threshold]

            if len(model_regions) < top:
                continue

            model_dict[(seq1, seq2)] = seq_df.loc[(seq1, seq2),
                                                  model_regions[:top]]

        return model_dict

    def multimap(self, df, trusted_seqs_raw, db_seqs, region):
        import scipy.optimize

        trusted_seqs = [seq
                        for seq in trusted_seqs_raw
                        if self.is_amplified(seq, region)]

        N = len(trusted_seqs)
        try:
            B = df.ix[trusted_seqs, region].fillna(value=0).values
        except KeyError:
            return pd.Series(0, index=trusted_seqs)

        A = np.zeros((N, N), dtype=np.float32)
        norm = np.zeros(N, dtype=np.float32)

        for k, seq in enumerate(trusted_seqs):
            x = self.mapping[seq][region]['map'].select(
                lambda x: x in db_seqs).sum()
            norm[k] = x if x > 0 else 1

        for i, seq in enumerate(trusted_seqs):
            A[i, i] = 1 - (self.mapping[seq][region]['map'].select(
                lambda x: x in db_seqs).sum() - self[(seq, region, seq)])

            for j, seq2 in enumerate(trusted_seqs):
                if i == j:
                    continue
                A[i, j] = self[(seq2, region, seq)]

        A = A / norm
        return pd.Series(scipy.optimize.nnls(A, B)[0], index=trusted_seqs)

    def multimap_simple(self, df, fake_seqs, trusted_seqs, region):
        correction = pd.Series(0, index=trusted_seqs, dtype=float)
        for fake in fake_seqs:
            p_vec = pd.Series(
                {seq: self[(seq, region, fake)]
                 for seq in trusted_seqs},
                dtype=float)
            p_vec = p_vec / (1.0 - p_vec)
            m_vec = df.loc[trusted_seqs, region] * p_vec
            correction = correction.add(
                (m_vec * df.loc[fake, region]) / m_vec.sum(),
                fill_value=0)
        return correction.astype(int)

    def normalize(self):
        '''
        Normalize to mismapping probabilities and cast defaultdicts
        to normal dicts
        '''
        new_mapping = dict(self.mapping)
        for seq in self.mapping:
            new_mapping[seq] = dict(self.mapping[seq])
            for region in self.mapping[seq]:
                series = pd.Series(self.mapping[seq][region]['map'],
                                   dtype=np.float32)
                new_mapping[seq][region]['map'] = series / series.sum()

        self.mapping = new_mapping
        return None

    def is_amplified(self, seq, region):
        return region in self.mapping[seq]
        # try:
        #     return self.mapping[seq][region]['amplify']
        # except KeyError:
        #     return False

    def _max_region_prob(self, seq, regions, key_fn):
        return max([sum(
            [v for k, v in self.mapping[seq]['map'][r].iteritems() if key_fn(k)
             ]) for r in regions])

    def __getitem__(self, key):
        try:
            return self.mapping[key[0]][key[1]]['map'].loc[key[2]]
        except KeyError:
            return 0


def create_fq_pairs(seq, region):
    seq.id = str(RegionID(seq.id, region=region))

    l = len(seq)
    i = min(150, l)
    j = max(0, l - 150)

    p1 = seq[:i]
    p2 = seq[j:l]

    p2.seq = p2.seq.reverse_complement()

    p1.description = '{0}:N:0:29'.format('1')
    p2.description = '{0}:N:0:29'.format('2')

    p1.letter_annotations['phred_quality'] = [30] * len(p1)
    p2.letter_annotations['phred_quality'] = [30] * len(p2)

    return p1, p2


@temp_file(scalar=False)
def create_region_fastq_reads(region_dir,
                              taxID_set=None,
                              region_set=None,
                              **kwargs):

    fastq = [NamedTemporaryFile(mode='w+', suffix='.fastq') for _ in xrange(2)]
    fq_handles = [f.file for f in fastq]

    for pcr in region_dir.glob('*.fna'):
        #I am not using izip on purpose here
        region = re.search(r'(V.*?)\.fna', pcr).group(1)
        if region_set is not None and region not in region_set:
            continue
        with open(pcr, 'r') as fh_in:
            fq_pairs = zip(
                *[create_fq_pairs(seq, region)
                  for seq in SeqIO.parse(fh_in, 'fasta')
                  if taxID_set is None or TaxID(seq.id) in taxID_set])

        for fh, seqs in izip(fq_handles, fq_pairs):
            SeqIO.write(seqs, fh, 'fastq')
            fh.flush()

    #reset file position for any clients that want to read this file
    for fq in fq_handles:
        fq.seek(0)

    return fastq


@temp_file()
def create_tmp_novoindex(ref_seq_fp, taxID_set=None, **kwargs):
    ndx = mkstemp(suffix='.ndx')[1]
    if taxID_set is None:
        sh.novoindex(ndx, ref_seq_fp)
        return ndx

    with open(ref_seq_fp, 'r') as fh_in, NamedTemporaryFile('w') as fh_out:
        out_recs = (rec
                    for rec in SeqIO.parse(fh_in, 'fasta')
                    if TaxID(rec.id) in taxID_set)

        SeqIO.write(out_recs, fh_out.file, 'fasta')
        fh_out.file.flush()
        sh.novoindex(ndx, fh_out.name)
    sh.novoutil('index', ndx, _out=sys.stderr)

    return ndx


@temp_file(scalar=False, out=1)
def create_tmp_bam(novoalign_opts,
                   ref_fasta,
                   region_dir,
                   taxID_set,
                   output_fp=None,
                   **kwargs):
    bam = mkstemp(suffix='.bam')[1]
    with create_tmp_novoindex(ref_fasta, taxID_set, **kwargs) as ndx, \
      create_region_fastq_reads(region_dir, taxID_set, **kwargs) as fq_fp_list:

        fq_fp = map(lambda x: x.name, fq_fp_list)
        run_novoalign(novoalign_opts, fq_fp, ndx, bam)

    if output_fp is not None:
        shutil.copy(bam, output_fp)

    return bam, '%s.bai' % bam

    # def run_novoalign(opts, fq_fp, ndx, bam):
    #         os.system(
    #             ('novoalign -F STDFQ -r All -o SAM {0} -d {1} -f {2} | '+\
    #             'samtools view -uS - | novosort -c 1 -i -o {3} -').format(
    #                 opts, ndx, ' '.join(fq_fp), bam
    #                 )
    #             )


def run_novoalign(opts, fq_fp, ndx, bam):
    tmp_base = mkstemp()[1]
    #sorted_tmp_bam = bam + '.sorted'
    os.system(
        ('novoalign -F STDFQ -H -o SAM {0} -d {1} -f {2} | '+\
         'samtools view -uS - | samtools sort -O bam -T {3} -o {4} -').format(
             opts, ndx, ' '.join(fq_fp), tmp_base, bam
            )
        )


def novoCluster(novoalign_opts, ref_dir=None, taxid=None):
    #ref_dir = '/srv/seq/analysis1/reference_sequences/microbes/ncbi_genome'

    #Do some type checking to prevent strings from being passed
    if taxid is not None and not all(isinstance(t, TaxID) for t in taxid):
        raise TypeError()

    ref_fasta = get_unique_file(ref_dir.joinpath('ref_16s*.fna'))
    region_dir = ref_dir.joinpath('regions')

    taxID_set = set(taxid) if taxid is not None else None

    #ssu_db = pickle.load(open(ref_dir.joinpath('ssu_db.pickle'), 'rb'))

    region_name = ['V1-V2', 'V1', 'V2', 'V3_1', 'V3_2', 'V4', 'V5_1', 'V5_2',
                   'V5-V6', 'V6_1', 'V6_2', 'V6_3', 'V7-V8', 'V9']

    mapping_info = NovoMappingPanda(region_name)

    with create_tmp_bam(novoalign_opts, ref_fasta, region_dir,
                        taxID_set) as bam_fp:

        bam = pysam.AlignmentFile(bam_fp, 'rb')

        for algn in bam.fetch(until_eof=True):
            info = algn.qname.split(' ')[0]
            #does proper pair have meaning in all alignment?
            if algn.is_proper_pair and algn.is_read1:
                ref_SeqID = SeqID(bam.getrname(algn.tid))
                read_RegionID = RegionID(info)
                mapping_info.add_alignment(read_RegionID, ref_SeqID)

        bam.close()

        #normalize the mapping_info into probabilities
    mapping_info.normalize()

    return mapping_info
