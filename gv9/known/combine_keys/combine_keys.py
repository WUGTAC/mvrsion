import pandas as pd
import re
import sys
from functools import partial
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('output_fp')
parser.add_argument('--sample_key_fp', help='This is not normally needed')

mouse_table = 'MN1.0_aggregate_norm_percent_profile_v2_GTAC.classic.txt'
mouse_key = 'MN1.0_aggregate_mapping_file_v2_GTAC.txt'
mixture = 'uneven_mock_community.csv'
bei_mixture = 'bei_known.txt'


def make_gtac_sample_name(key_df, original_name):
    #120111, FP, A01
    date_str, letter_num_raw = key_df.loc[original_name,
                                          ['Date', 'individual']]

    #A1
    letter_num = re.sub(r'([A-Z])0(\d+)', r'\1\2', letter_num_raw)
    return '1-0-{0}-100G-{1}-FP'.format(date_str, letter_num)


def rename_mixture_samples(name):
    if 'Unnamed' not in name:
        return name
    else:
        x = (int(re.search(r'(\d+)', name).group(1)) - 5) / 2
        y = x / 4
        z = x - (4 * y)
        return '48G-{0}-{1}-staggered'.format(y + 1, z + 1)


def make_standard_name(lineage):
    return re.split(r';[a-z]_', lineage)[6].strip().lower().replace('_', ' ')


def make_standard_name_mix(index):
    name = index[0].strip() + ' ' + index[1].strip().split()[0]
    return name.replace('-related', '').lower()


def parse_tiobe_calls(fh):
    """As of now we are not using the abundance info"""
    sample_to_calls = {}
    for line in fh:
        if '>' in line:
            sample = line.strip().lstrip('>')
            sample_to_calls[sample] = []
        else:
            sample_to_calls[sample].append(line.strip().lower())

    sample_to_series = {
        s: pd.Series(1. / len(calls), index=calls)
        for s, calls in sample_to_calls.iteritems()
    }

    return pd.DataFrame.from_dict(sample_to_series)


args = parser.parse_args()

#process the mouse samples
table_df = pd.read_table(mouse_table, sep='\t', index_col=0)
key_df = pd.read_table(mouse_key, sep='\t', index_col=0)

#change taxonomy from rdp to just genus species
table_df['taxonomy'] = table_df['taxonomy'].apply(make_standard_name)
final_table_df = table_df.groupby('taxonomy', axis=0).sum()
final_table_df.rename(
    columns=partial(make_gtac_sample_name, key_df), inplace=True)

#process the known mixtures of 48 microbes
usecols = [1, 2] + range(5, 21, 2)  #the formating of the table is excel fresh
mix_df = pd.read_table(mixture, sep=',', usecols=usecols)
mix_df.rename(columns=rename_mixture_samples, inplace=True)
mix_df.set_index(['Genus', 'Species'], inplace=True)
mix_df['48G-even'] = 1.0 / len(mix_df.index)  #add in the even mixture sample

final_mix_df = mix_df.groupby(make_standard_name_mix, axis=0).sum()

#process the BEI mixture sample
with open(bei_mixture, 'r') as fh:
    bei_species = [
        re.search(r'\w+ \w+', line).group(0).lower()
        for line in fh.readlines()
    ]

bei_df = pd.DataFrame(
    1. / len(bei_species), index=bei_species, columns=['BEI'])

#combine the three tables on "genus species" index
final_df = final_mix_df.join([final_table_df, bei_df], how='outer')
final_df.to_csv(args.output_fp, na_rep=0, sep='\t', index_label='taxonomy')

#make a sample key that matches gordon names to gtac sequencing names
if args.sample_key_fp is not None:
    key_df['GTAC_name'] = [
        make_gtac_sample_name(key_df, n) for n in key_df.index
    ]
    key_df.to_csv(args.sample_key_fp, sep='\t', index_col=False)
