import sys
import re
import pdb

known = sys.argv[1]
db_fp = sys.argv[2]

tax_db = open(db_fp, 'r').read()

in_=0
out=0
total=0
for tax in open(known, 'r'):
    rdp = r'g__{0};\s?s__{1}'.format(*tax.split()[:2])
    std = '[ _]'.join(tax.split()[:2])
    re_str = '|'.join([rdp, std])
    if not re.search(re_str, tax_db, flags=re.I):
        print '{0}: {1} not in file!'.format(re_str, tax.strip())
        out+=1
    else:
        in_+=1

print '%s microbes NOT in %s' % (out,db_fp)
print '%s microbes ARE in %s' % (in_,db_fp)

#try to find total rdp strings if none found look for microbe standard names
# pretty sure this is wrong, what about duplicate rdp strings?
total_microbes = len(set(re.findall(r'k__.*$', tax_db)))
if total_microbes == 0:
    total_microbes = len(set(re.findall(r'[a-z]+[ _]+[a-z]+', tax_db, flags=re.I)))

    
print '%s microbes TOTAL in %s' % (total_microbes,db_fp)
