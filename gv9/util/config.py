from ConfigParser import ConfigParser
from path import path


def get_system_config():
    config = ConfigParser()
    config.read(['gv9.ini', path('~/.config/gv9/gv9.ini').expand()])
    return config
