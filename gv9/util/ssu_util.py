import copy, re
import shutil
from itertools import izip, imap
from glob import glob
from path import path
from pkg_resources import resource_filename
import collections
import functools
import yaml

import gtac.launch


class ValueTree():
    def __init__(self):
        self.value = collections.defaultdict(list)
        self.children = collections.defaultdict(ValueTree)


#exceptions for RdpString class
class RdpStringNotValidError(Exception):
    pass


class InvalidTaxonomyLevel(Exception):
    pass


#exceptions for SsuDataBase class
class TaxonomyNotSpecificError(Exception):
    pass


def _fetch(func):
    @functools.wraps
    def wrapper(self, sample=None):
        ret = self.joinpath(func(self))
        if sample is None:
            #used for ruffus recipes
            return ret
        else:
            #used to fetch files in ssu.py
            fps = glob(ret.format(sample))
            return fps

    return wrapper


class AnalysisDirectory(gtac.launch.AnalysisDirectory):
    '''
    Assumes that this code will always be run from
    the top level of the analysis directory
    '''
    dirs = {
        'raw': ('Reads', '{sample}.R{read}.fq.gz'),
        'read': ('Trimmed', '{sample}.R{read}.fq.gz'),
        'region': ('Regions', '{sample}.qname_to_region.pickle'),
        'qiime': ('qiime', 'results.biom'),
    }

    files = {'call': 'calls.txt'}

    data_dir = path('Data')  #Stores the data used/created by ssu.py

    def create(self, N_data=2):
        '''
        N_data is the number of data folders to recursively make
        This number should be 1 + {number of time you want to run ssu.py filter}
        '''
        super(AnalysisDirectory, self).create()

        self._setup_qiime_dir()
        out_d = self.path.joinpath(self.data_dir)
        for _ in xrange(N_data):
            DataDirectory(out_d).create()
            out_d = out_d.joinpath(self.data_dir)

    def get_data_dir(self, level=0):
        args = [self.data_dir] * (level + 1)
        return DataDirectory(self.path.joinpath(*args))

    def _setup_qiime_dir(self):
        plugin = path(resource_filename('gv9', 'data/qiime_plugin'))
        for fp in plugin.glob('*'):
            fp.copy2(self.qiime())


class DataDirectory(gtac.launch.AnalysisDirectory):
    '''
    At a given level of analysis, align and cluster are inputs
    call and a subdir DataDirectory are the outputs
    Fetch a given level's datadirectory from ssu.py
    via AnalysisDirectory.get_data_in(${input_bam}.dirname())
    '''
    dirs = {
        'align': ('Alignments', '{sample}.bam'),
        'call': ('Calls', '{sample}.{method}.call.txt'),
        'cluster': ('Cluster', '{sample}.novocluster.pickle'),
        'results': ('results', '{method}.taxa_summary.xls'),
        'ppv': ('ppv', '{method}.txt')
    }


class ReferenceDirectory(gtac.launch.AnalysisDirectory):
    dirs = {}
    files = {
        'index': 'ref_16s.ndx',
        'fasta': 'ref_16s.fna',
        'db': 'ssu_db.pickle',
        'taxa': 'rdp_taxonomy.txt',
        'region': 'pprospector',
    }


class MultiLevelReferenceDirectory(ReferenceDirectory):
    files = {
        'fasta': 'ref_16s.fna',
        'taxa': 'rdp_taxonomy.txt',
    }

    def genus(self):
        return ReferenceDirectory(self.path.joinpath('genus'))

    def species(self):
        return ReferenceDirectory(self.path.joinpath('species'))


def read_parameter_file(fp):
    if fp is None:
        return {}

    with open(fp, 'r') as fh:
        params = yaml.load(fh.read())

    return params


class RdpFactory(object):

    token_sep = ';'

    def __init__(self, constructor=None):
        '''
        Constructor class must have a static attribute rdp_label_hierarchy
        Also must have @classmethod _is_level_present(lvl, OrderedDict)
        '''
        self.constructor = (constructor or _RdpString)

        labels = '|'.join(self.constructor.rdp_label_hierarchy)

        self.parser_regex = re.compile(r'\s*((?:%s)__.*)\s*' % labels)

        self.default_tokens = \
          collections.OrderedDict(
            [(t, '') for t in self.constructor.rdp_label_hierarchy])

    def create(self, rdp_info):
        '''
        Creates tokens for all labels in the hierarchy regardless of whether they
        are contained in the raw input string or OrderedDict
        '''

        if isinstance(rdp_info, str):
            tokens = self.__parse_tokens(rdp_info)

        elif isinstance(rdp_info, collections.OrderedDict):
            tokens = rdp_info

        else:
            raise TypeError(
                'create only takes strings or OrderedDict as input. '+\
                'Passed type was %s' % rdp_info.__class__)

            #self.__validate_rdp_tokens(tokens) #Make sure all lineage keys are valid
            #this modifies tokens in place
        self.__clean_tokens(
            tokens)  #strip whitespace and capitalize all tax values

        full_tokens = self.default_tokens.copy()
        full_tokens.update(tokens)

        new_string = self.__token_to_string(full_tokens)
        rdp = self.constructor(new_string)
        rdp.tokens = full_tokens
        rdp.factory = self  #allows rdpstring to return new rdpstrings
        return rdp

    def create_from_unlabeled(self, _str, token=token_sep):
        _dict = \
          collections.OrderedDict(
            izip(self.constructor.rdp_label_hierarchy,_str.split(token))
              )

        return self.create(_dict)

    def __clean_tokens(self, order_dict):
        for key, val in order_dict.iteritems():
            if val:
                order_dict[key] = val.strip().capitalize()

        return order_dict

    def __parse_tokens(self, rdp_string):
        raw_tokens = rdp_string.split(self.token_sep)
        tokens = (t.split('__', 1) for t in raw_tokens
                  if self.parser_regex.match(t))

        return collections.OrderedDict(tokens)

    def __tokenize_rdp(self, rdp_string):
        '''
        Returns an OrderedDict of {level_label : value}
        Checks that the levels are properly named and in order
        All levels down to strain must be present even if empty
        Will ignore "Unassigned" and "Other" in qiime strings
        '''

        tokens = self.__parse_tokens(rdp_string)

        self.__validate_rdp_tokens(tokens, rdp_string)  #ensure proper order

        return tokens

    def __token_to_string(self, tokens):
        return self.token_sep.join('%s__%s' % each for each in tokens.items())

    def __validate_rdp_tokens(self, tokens, raw_string=None):
        error_message = str(tokens) if raw_string is None else raw_string
        #Assert that the order of the labels is same as self.constructor.rdp_label_hierarchy
        labelsValid = all(k == y
                          for (k, v), y in izip(tokens.iteritems(
                          ), self.constructor.rdp_label_hierarchy) if v)

        if not labelsValid:
            raise RdpStringNotValidError('labels not in proper order: ' +
                                         error_message)

        return None


class _RdpString(str):
    '''
    NOT MEANT TO BE CONSTRUCTED DIRECTLY. USE RdpFactory.create()
    FOR THE LOVE OF GOD DONT MESS WITH SELF.TOKENS
    Does not require that full hierarchy is present, just that order is preserved
    Strips whitespace and places ; at end of string if not there
    Also contains attributes called tokens and factory (see RdpFactory.create())
    '''
    token_sep = ';'
    label_sep = '__'
    rdp_label_hierarchy = ('k', 'p', 'c', 'o', 'f', 'g', 's', 't')

    def get_level(self, lvl):
        self.__validate_level(lvl)

        if self.is_level_present(lvl):
            return self.tokens[lvl]
        else:
            return ''

    def is_level_present(self, lvl):
        self.__validate_level(lvl)

        try:
            return bool(self.tokens[lvl])
        except KeyError:
            return False

    @property
    def pretty(self):
        return self.token_sep.join(
            self.label_sep.join(each) for each in self.tokens.iteritems()
            if each[1])

    def iter_tokens(self):
        return self.tokens.iteritems()

    def is_matching_subset(self, other):
        '''
        Returns true if one RdpString is a subset of the other
        '''
        return all(
            [x == y for x, y in zip(self.iterLabels(), other.iterLabels())])

    def remove_level(self, lvl):
        '''
        Set lvl to an empty string
        Also removes everything lower than lvl
        Preserves all levels of original string as empty strings
        '''
        self.__validate_level(lvl)
        levelIndex = self.rdp_label_hierarchy.index(lvl)
        new_tokens = copy.deepcopy(self.tokens)
        new_tokens.update(
            {t: ''
             for t in self.rdp_label_hierarchy[levelIndex:]})

        return self.factory.create(new_tokens)

    def take_level(self, lvl):
        '''
        Set all levels lower than lvl to empty
        '''
        self.__validate_level(lvl)
        levelIndex = self.rdp_label_hierarchy.index(lvl)
        new_tokens = copy.deepcopy(self.tokens)
        new_tokens.update(
            {t: ''
             for t in self.rdp_label_hierarchy[levelIndex + 1:]})

        return self.factory.create(new_tokens)

    def __iter__(self):
        return self.tokens.itervalues()

    def __getitem__(self, key):
        return self.tokens[key]

    @classmethod
    def __validate_level(cls, lvl):
        if lvl not in cls.rdp_label_hierarchy:
            raise InvalidTaxonomyLevel('%s not in %s' %
                                       (lvl, str(cls.rdp_label_hierarchy)))


class _SsuID(object):
    '''
    All entries get cast into str
    '''
    field_sep = ':'

    @classmethod
    def clean_input(cls, val):
        if isinstance(val, str):
            values = val.split(cls.field_sep)
        elif hasattr(val, '__iter__'):
            values = map(str, val)
        else:
            values = [str(val)]

        return values[:len(cls._fields)]

    @classmethod
    def from_tuple(cls, tuple_):
        return cls(cls.field_sep.join(map(str, tuple_)))

    def __str__(self):
        return self.field_sep.join(self)


def SsuIDFactory(name, fields):
    '''
    name is a string naming the returned class
    fields is input to namedtuple field_names
    '''

    def __new__(cls, val, *args, **kwargs):
        cleaned_val = cls.clean_input(val)
        cleaned_val.extend(imap(str, args))
        for k, v in kwargs.iteritems():
            kwargs[k] = str(v)

        self = super(eval(name), cls).__new__(cls, *cleaned_val, **kwargs)
        return self


    return \
      type(
          name,
          (collections.namedtuple(name, fields), _SsuID),
          {'__new__' : __new__}
          )


TaxID = SsuIDFactory('TaxID', ['tax'])
SeqID = SsuIDFactory('SeqID', ['tax', 'seq'])
RegionID = SsuIDFactory('RegionID', ['tax', 'seq', 'region'])

# class TaxID(collections.namedtuple('data', 'tax'), _SsuID):
#     '''
#     The ID classes only accept one argument in their constructor.
#     It must be a string or a tuple
#     '''
#     def __new__(cls, val):
#         cleaned_val = cls.clean_input(val)
#         self = super(TaxID, cls).__new__(cls, *cleaned_val)
#         return self

# class SeqID(collections.namedtuple('data', 'tax,seq'),_SsuID):
#     def __new__(cls, val, *args, **kwargs):
#         cleaned_val = cls.clean_input(val)
#         cleaned_val.extend(args)
#         self = super(SeqID, cls).__new__(cls, *cleaned_val, **kwargs)
#         return self

# class RegionID(collections.namedtuple('data', 'tax,seq,region'), _SsuID):
#     def __new__(cls, val, *args, **kwargs):
#         cleaned_val = cls.clean_input(val)
#         cleaned_val.extend(args)        
#         self = super(RegionID, cls).__new__(cls, *cleaned_val, **kwargs)
#         return self


class SsuDataBase():
    '''
    Backend is currently python dictionaries.
    Upgrade to sqlite or hd5py to feel fancy
    A taxID is a ncbi taxonomy id ie 511112
    A seqID is a taxID plus a number denoting the copy ie 511112:3
    Generally an argument name taxID to a method will cleaned so that a seqID
     is also an acceptable argument (ie seqID gets downcast to taxID)

    rdp_constructor defaults to RdpFactory().create
    '''

    def __init__(self, rdp_tax_fp, rdp_constructor=None):

        self.taxid_to_rdp = \
          self._build_taxid_to_rdp(
              rdp_tax_fp,
              rdp_constructor=rdp_constructor
              )

        self.tax_tree = self._build_taxonomy_tree()

        #this is here for performance since all we usually care about is species anyway
        self.taxid_to_species = {
            tax: rdp.take_level('s')
            for tax, rdp in self.taxid_to_rdp.iteritems()
        }

    def get_species(self, taxid):
        return self.taxid_to_species[TaxID(taxid)]

    def get_all_tax(self, level):
        '''
        If level=s then this returns all species in the database
        Only returns entries that have at least one sequence
        '''
        return set(self.taxid_to_rdp[tax].take_level(level)
                   for tax in self.database)

    def rdp_to_taxid(self, rdp_string):
        '''
        Returns set of taxIDs which match the given rdp_string
        '''

        if not isinstance(rdp_string, _RdpString):
            raise TypeError('rdp_string must be of class _RdpString')

        current_node = self.tax_tree

        for token in rdp_string:
            if not token:
                continue
            current_node = current_node.children[token]

        possible_taxID = set()
        self.__get_leaf_taxID_list(current_node, possible_taxID)
        return possible_taxID

    def _build_taxid_to_rdp(self, rdp_tax_fp, rdp_constructor=None):
        import csv

        if rdp_constructor is None:
            rdpFactory = RdpFactory()
            rdp_constructor = rdpFactory.create

        taxid_to_rdp = {}
        with open(rdp_tax_fp, 'r') as fh:
            for seqid, raw_rdp_string in csv.reader(fh, delimiter='\t'):

                taxID = TaxID(seqid)
                rdp = rdp_constructor(raw_rdp_string)

                #Ensure that all entries with same taxID have same rdp_string
                if taxID in taxid_to_rdp and taxid_to_rdp[taxID] != rdp:
                    raise RuntimeError('Mismatching taxa on taxID %s' % taxID)
                else:
                    taxid_to_rdp[taxID] = rdp

        return taxid_to_rdp

    def _build_taxonomy_tree(self):
        import csv
        tax_tree = ValueTree()
        for taxID, rdp_string in self.taxid_to_rdp.iteritems():

            #walk dat tree girl
            current_node = tax_tree

            for tax in rdp_string:
                if tax:
                    current_node = current_node.children[tax]

            current_node.value['taxID'].append(taxID)
        return tax_tree

    def __get_leaf_taxID_list(self, node, taxID_set):
        '''
        mutates passed set in place
        '''
        if node.value['taxID']:
            taxID_set.update(node.value['taxID'])
        if node.children:
            for lower_node in node.children.itervalues():
                self.__get_leaf_taxID_list(lower_node, taxID_set)

    def __hash__(self):
        return id(self)

    def __getitem__(self, taxid):
        taxID = TaxID(taxid)
        return self.taxid_to_rdp[taxID]

    def __contains__(self, taxid):
        taxID = TaxID(taxid)
        return taxID in self.taxid_to_rdp
