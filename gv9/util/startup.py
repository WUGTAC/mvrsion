import re
import gtac.startup


class MiseqDirectoryData(gtac.startup.DirectoryData):
    fastq_regexp = re.compile(
        r'(.*?)(?:_S\d+_L001)?[_.]R([12])(?:_001)?\.f(?:ast)?q\.gz')


class HiseqDirectoryData(gtac.startup.DirectoryData):
    fastq_regexp = re.compile(r'(.*?)\.R([12])\.fq\.gz')
