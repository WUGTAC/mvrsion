#!/usr/bin/env python
import argparse
import logging
import os
from datetime import timedelta

import ruffus
import sh
from path import path

import pkg_resources as pkg

from gv9.util.ssu_util import AnalysisDirectory, ReferenceDirectory
import gv9.util.config
import gv9.util.startup
import gtac.startup
import gtac.cmdline
from gtac.ruffusUtil import auto_drm_job_factory, remove_empty_files, SUBMIT_JOB_LOGGING

from drm import get_drm_module
'''
This is the main controller script of gv9
'''


def get_argparse():
    config = gtac.startup.config.system_config('mvrsion-config.yaml')
    factory, _ = gtac.startup.bootstrap()
    factory.register(gv9.util.startup.MiseqDirectoryData)
    factory.register(gv9.util.startup.HiseqDirectoryData)

    def ref_dir(x):
        return ReferenceDirectory(
            gtac.cmdline.abs_exst_path(path(config.get('reference', x))))

    def existing_analysis_dir(x):
        return AnalysisDirectory(gtac.cmdline.abs_exst_path(x))

    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument(
        '-o', '--output_dir', required=True, type=AnalysisDirectory)
    parser.add_argument(
        '-p', '--parameters', type=gtac.cmdline.abs_exst_path, required=True)
    parser.add_argument('--data', nargs='+', type=factory.build)
    parser.add_argument(
        '-s',
        '--sample',
        type=gtac.cmdline.abs_exst_path,
        help='a table with rows matching sample name to paired read paths')
    parser.add_argument('--ref_dir', default='silva', type=ref_dir)
    parser.add_argument('--base', type=existing_analysis_dir)
    parser.add_argument('-k', '--known', type=gtac.cmdline.abs_exst_path)
    parser.add_argument(
        '-w', '--workers', type=gtac.cmdline.positive_int, default=8)
    parser.add_argument('--tval', type=int, default=60)

    parser.add_argument(
        '--primers',
        default=pkg.resource_filename(__name__, 'data/ssu_primers.txt'),
        type=gtac.cmdline.abs_exst_path, )

    parser.add_argument(
        '--adapters',
        default=pkg.resource_filename(__name__, 'data/adapters.fa'),
        type=gtac.cmdline.abs_exst_path, )

    return argparse.ArgumentParser(
        parents=[parser, ruffus.cmdline.get_argparse()], add_help=False)


def setup_from_base(args):
    os.system('ln -s {0}/* {1}'.format(args.base.raw(), args.output_dir.raw()))
    os.system(
        'ln -s {0}/* {1}'.format(args.base.read(), args.output_dir.read()))
    os.system(
        'ln -s {0}/* {1}'.format(args.base.region(), args.output_dir.region()))
    os.system('ln -s {0}/* {1}'.format(
        args.base.get_data_dir(0).align(),
        args.output_dir.get_data_dir(0).align()))


def job_namer(fp):
    return path(fp).relpath().replace('/', '-').lstrip('-')


def main():
    args = get_argparse().parse_args()

    def name_link(_, sample, iread):
        return args.output_dir.raw(sample=sample, read=iread)

    # should refactor this into click style commands for release
    # If the output dir exists but there are no raw fastq files within the pipeline will fail
    args.output_dir.create(N_data=2)
    if args.sample:
        gtac.startup.fetch_fastq(args.sample)
    elif args.data:
        sample_table = args.output_dir.path.joinpath('samples.txt')
        gtac.startup.write_sample_table(
            args.data, sample_table, make_link_name=name_link)
        gtac.startup.fetch_fastq(sample_table)
    elif args.base:
        setup_from_base(args)

    args.output_dir.path.cd()
    logger = logging.getLogger(__name__)
    hdlr = logging.FileHandler('log.txt')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)

    logger.info('Reference Sequences: %s',
                sh.md5sum(args.ref_dir.fasta()).stdout)
    logger.info('Config file is: %s', args.parameters)
    logger.info('Config file contents: %s', open(args.parameters).read())

    submit_job_lib = get_drm_module()

    auto_drm_job = auto_drm_job_factory(
        submit_job_lib.Submitter(script='jobs', log='log'), job_namer)

    remove_empty_files(args.output_dir.path)
    raw_reads = args.output_dir.raw().glob('*')

    @ruffus.collate(raw_reads,
                    ruffus.formatter(r".+/(?P<sample>.*?)\.R[12]"), [
                        args.output_dir.read("{sample[0]}", read=1),
                        args.output_dir.read("{sample[0]}", read=2)
                    ], "{sample[0]}", args)
    @auto_drm_job
    def primer_trim(input_fps, output_fps, sample_name, args):
        cmd = '''primerTrimmer -i {in_fq} -o {out_fq} --name \
        -p {primers} -a {adapters} --pickle {qname}'''.format(
            in_fq=' '.join(input_fps),
            out_fq=' '.join(output_fps),
            primers=args.primers,
            adapters=args.adapters,
            qname=args.output_dir.region(sample_name), )

        resrc = submit_job_lib.Resource(
            time=timedelta(hours=2),
            memInGB=1, )

        return cmd, resrc

    @ruffus.transform(
        primer_trim,
        ruffus.formatter(r".+/(?P<sample>.*?)\.R1", r".+/(?P<sample>.*?)\.R2"),
        args.output_dir.get_data_dir(0).align("{sample[0]}"), args)
    @auto_drm_job
    def align(input_fps, output_fp, args):
        cmd = ('novoalign {opts} -c {workers} -d {ndx} -f {reads}'
               ' | samtools view -bS - > {bam}').format(
                   ndx=args.ref_dir.index(),
                   workers=args.workers,
                   reads=' '.join(input_fps),
                   bam=output_fp,
                   opts='-t %s -H -k -i PE 50-400 -F STDFQ -o SAM -r Random' %
                   args.tval, )  #think about best way to handle novoalign

        resrc = submit_job_lib.Resource(
            time=timedelta(hours=4),
            memInGB=4,
            workers=args.workers, )

        return cmd, resrc

    #output of init is considered the second stage alignments
    @ruffus.transform(align,
                      ruffus.formatter(r".+/(?P<sample>.*?)\.bam"),
                      args.output_dir.get_data_dir(1).align('{sample[0]}'),
                      '{sample[0]}', args)
    @auto_drm_job
    def gv9_init(input_fp, output_fp, sample_name, args):

        cmd = '''ssu init --input {in_dir} --output {out_dir} \
        --ref_dir {ref} --threads {workers} --sample {sample} \
        --analysis {analysis}'''.format(
            in_dir=args.output_dir.get_data_dir(0),
            out_dir=args.output_dir.get_data_dir(1),
            ref=args.ref_dir,
            workers=args.workers,
            sample=sample_name,
            analysis=args.output_dir, )

        cmd += ' --known %s' % args.known if args.known is not None else ''
        cmd += ' --tval %s' % args.tval if args.tval is not None else ''
        cmd += ' --parameters %s' % args.parameters if args.parameters is not None else ''

        resrc = submit_job_lib.Resource(
            time=timedelta(hours=4),
            memInGB=8,
            workers=args.workers, )

        return cmd, resrc

    #These functions bless method og_species as output of ssu.py filter
    #do not remove this method from ssu.py filter
    @ruffus.transform(gv9_init,
                      ruffus.formatter(r'.+/(?P<sample>.*?)\.bam'),
                      args.output_dir.get_data_dir(1).call(
                          '{sample[0]}', method='og_species'), '{sample[0]}',
                      args)
    @auto_drm_job
    def gv9_final_filter(input_fp, output_fp, sample_name, args):
        cmd = '''ssu filter --input {in_dir} \
        --ref_dir {ref} --sample {sample} --analysis {analysis} \
        '''.format(
            in_dir=args.output_dir.get_data_dir(1),
            ref=args.ref_dir,
            sample=sample_name,
            analysis=args.output_dir, )

        cmd += ' --known %s' % args.known if args.known is not None else ''
        cmd += ' --tval %s' % args.tval if args.tval is not None else ''
        cmd += ' --parameters %s' % args.parameters if args.parameters is not None else ''

        resrc = submit_job_lib.Resource(
            time=timedelta(hours=1),
            memInGB=2, )

        return cmd, resrc

    @ruffus.active_if(args.known)
    @ruffus.collate(
        gv9_init,
        ruffus.formatter(r'.*'),
        '.first',
        args, )
    @auto_drm_job
    def calc_first_ppv(input_fps, output_fp, args):

        cmd = '''combine_calls {dir_}; \
        report_results {dir_} {known}'''.format(
            dir_=args.output_dir.get_data_dir(0),
            known=args.known, )

        resrc = submit_job_lib.Resource(
            time=timedelta(minutes=30),
            memInGB=2, )

        return cmd, resrc

    @ruffus.collate(
        gv9_final_filter,
        ruffus.formatter(r'.*'),
        '.done',
        args, )
    @auto_drm_job
    def write_final(input_fps, output_fp, args):

        cmd = 'combine_calls {dir_}'.format(
            dir_=args.output_dir.get_data_dir(1))

        if args.known:
            cmd += ' && report_results {dir_} {known}'.format(
                dir_=args.output_dir.get_data_dir(1),
                known=args.known, )

        cmd += ' && echo 1 > {output_fp}'.format(output_fp=output_fp)
        resrc = submit_job_lib.Resource(
            time=timedelta(minutes=30),
            memInGB=2, )

        return cmd, resrc

    @ruffus.transform(write_final,
                      ruffus.formatter(r'.*'), args.output_dir.qiime(''), args)
    @auto_drm_job
    def qiime_downstream(_, biom, args):
        results_table = args.output_dir.get_data_dir(level=1).results(
            '', method='og_species')
        qiime = args.output_dir.qiime()
        sample_key = qiime.joinpath('sample_key.txt')
        biom_results = args.output_dir.qiime('')
        xls_results = biom.replace('.biom', '.xls')
        cmd = r'''
module load qiime_gtac
tmpBiom=$(mktemp -u -p $PWD)
tmpMd=$(mktemp -p $PWD)
trap "rm -f $tmpBiom $tmpMd" EXIT
awk -v OFS="\t" 'NR == 1 {{b=gensub(/[^a-zA-Z0-9\t]+/, ".", "g"); print b}} NR > 1 {{a=gensub(/\.[0-9]+/, "", "g"); print a}}' {results_table} > {xls_results}
biom convert -i {xls_results} -o $tmpBiom --table-type "OTU table" --to-json
cat <(echo -e "#OTUID\ttaxonomy") <(awk -v OFS="\t" -F "\t" 'NR > 1 {{print $1, $1}}' {results_table}) > $tmpMd
rm {biom_results}
biom add-metadata -i $tmpBiom -o {biom_results} --observation-metadata-fp $tmpMd
cd {qiime} && ./launch_post_qiime.sh {biom_results} {sample_key}
'''.format(**locals())

        resrc = submit_job_lib.Resource(
            time=timedelta(hours=4),
            memInGB=10, )

        return cmd, resrc

    ruffus.cmdline.run(args, checksum_level=0)


if __name__ == '__main__':
    main()
