import cPickle as pickle
import sys
import collections as cl
import pysam

bam_fp,region_fp = sys.argv[1:]

read_to_region = pickle.load(open(region_fp, 'rb'))

with pysam.AlignmentFile(bam_fp, 'rb') as bam:
    mapped_counts = cl.Counter(read_to_region.get(align.qname, 'unknown') for align in bam.fetch(until_eof=True)
                        if not align.is_unmapped)

with pysam.AlignmentFile(bam_fp, 'rb') as bam:
    unmapped_counts = cl.Counter(read_to_region.get(align.qname, 'unknown') for align in bam.fetch(until_eof=True)
                        if align.is_unmapped)


print mapped_counts
print ''
print unmapped_counts
