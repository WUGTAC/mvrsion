#!/usr/bin/env python
import pandas as pd
import sys
import re
from path import path
from gv9.util.ssu_util import DataDirectory
from glob import glob

'''
This script creates a taxa abundance table for all samples for each method in data_dir.calls()
using the per sample abundance files
'''

def make_method_df(method_name, data_dir):
    '''
    This function takes all the per sample calls and creates a dataframe
    with each column representing the microbe read counts for each sample
    '''
    series_lst = [pd.read_table(fp, sep='\t', index_col=0)
                  for fp in glob(data_dir.call(sample='*', method=method_name))]

    method_df = pd.concat(series_lst, axis=1)

    return method_df

def find_method_names(data_dir):
    #api failure, how to extrace values from AnalysisDirectory?
    # Warning, method names must not include a .
    methods = [re.search(r'\.([^.]*?)\.call\.txt', fp).group(1)
               for fp in glob(data_dir.call(sample='*', method='*'))]

    return set([m for m in methods if m])

def main():
    data_dir = DataDirectory(sys.argv[1])
    for method in find_method_names(data_dir):
        df = make_method_df(method, data_dir)
        fp = data_dir.results(sample='', method=method)
        df.to_csv(fp, sep='\t', na_rep=0, index_label='tax')

if __name__ == '__main__':
    main()
