import cPickle as pickle
import argparse
import sys
import pysam
import HTSeq
from path import path

import gtac.cmdline
import gv9.util.config
from gv9.util.ssu_util import ReferenceDirectory


def ref_dir(x):
    return ReferenceDirectory(gtac.cmdline.abs_exst_path(path(
        gv9.util.config.get_system_config().get('reference', x))))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--ref', type=ref_dir, required=True)
    parser.add_argument('-i',
                        '--input',
                        type=gtac.cmdline.abs_exst_path,
                        required=True)
    parser.add_argument('--format', choices=['fasta', 'fastq'])
    parser.add_argument('--rdp', required=True)

    args = parser.parse_args()

    with open(args.ref.db(), 'rb') as fh:
        ssu_db = pickle.load(fh)

    with pysam.AlignmentFile(args.input, 'rb') as bamfile:
        for align in bamfile.fetch(until_eof=True):
            if not align.is_unmapped:
                rname = bamfile.getrname(align.tid)
                ref_rdp = ssu_db.get_species(rname)
                if ref_rdp == args.rdp:
                    seq = HTSeq.Sequence(align.seq, align.query_name)
                    seq.write_to_fasta_file(sys.stdout)


if __name__ == '__main__':
    main()
