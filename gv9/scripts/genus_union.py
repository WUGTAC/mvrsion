import argparse
import warnings
import functools as ft

from path import path
import pandas as pd

import gv9.util.ssu_util as ssu_util


def aggregate(df):
    factory = ssu_util.RdpFactory()

    def _agg(x):
        rdp = factory.create(x)
        rdp.tokens['s'] = rdp['g']
        return factory.create(rdp.tokens)

    return df.groupby(_agg).sum()


def union(df_list):
    d = [aggregate(df) for df in df_list]
    warnings.warn('This function has been hacked to munge sample names!!!!')
    d = [df.rename(columns=lambda x: x.replace('_', '-')) for df in d]
    columns = ft.reduce(lambda x, y: x.union(y.columns), d, pd.Index([]))
    index = ft.reduce(lambda x, y: x.union(y.index), d, pd.Index([]))
    final_df = pd.DataFrame(columns=columns, index=index)
    for df in d:
        final_df.update(df)
    return final_df


def read_df(fp):
    return pd.read_csv(fp, sep='\t', index_col=0, na_values=['0', '0.0'])


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', nargs='+', type=read_df)
    parser.add_argument('-o', '--output', type=path)

    args = parser.parse_args()
    df = union(args.input)
    df.to_csv(args.output, sep='\t', na_rep=0, index_label='tax')


if __name__ == '__main__':
    main()
