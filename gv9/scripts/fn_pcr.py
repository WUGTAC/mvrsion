import pandas as pd
import cPickle as pickle
import argparse
from path import path
from Bio import SeqIO
from gv9.util.ssu_util import ReferenceDirectory, TaxID
from gv9.regionSelect import RegionMultimap
from gv9.db_build import Finalizer


def read_no_calls(fp_list):
    df_list = [pd.read_csv(fp, sep='\t') for fp in fp_list]
    final = Finalizer()
    for fp, df in zip(fp_list, df_list):
        df['Sample'] = fp.basename().replace(
            '.og_species.read_table_no_call.xls', '')
        df['Species'] = df['Species'].apply(lambda x: x.capitalize())
        df['Genus'] = df['Species'].apply(lambda x: x.split()[0])
        raw = [';'.join(e) for e in zip(df['Genus'], df['Species'])]
        df['Species'] = pd.Series(
            [final.factory.create_from_unlabeled(e) for e in raw])
        df.pop('Genus')

    df = pd.concat(df_list, ignore_index=True)
    return df


def species_summary_df(df, fasta):
    final = Finalizer()
    summary_df = df.groupby('Species').count()[['TP']]
    summary_df = summary_df.rename(columns={'TP': 'sample_count'})
    raw = [final.rdp_hash(x, '').seqID.tax for x in summary_df.index]
    summary_df['taxid'] = pd.Series(raw, index=summary_df.index)
    # set taxid as index for seq_count data
    summary_df = summary_df.reset_index().set_index('taxid')

    seq_count = {x: 0 for x in summary_df.index}
    for rec in fasta:
        taxID = TaxID(rec.id)
        if taxID.tax in seq_count:
            seq_count[taxID.tax] += 1
    summary_df['seq_count'] = pd.Series(seq_count)
    # set Species as index for pcr_results
    summary_df = summary_df.reset_index().set_index('Species')
    return summary_df


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'input',
        type=path,
        help='path to a directory containing *read_table_no_calls.xls file')

    parser.add_argument(
        'ref_dir', type=ReferenceDirectory, help='path to database directory')

    parser.add_argument('-o', '--output', type=path, help='table output path.')

    args = parser.parse_args()
    df = read_no_calls(args.input.glob('*.og_species.read_table_no_call.xls'))
    with open(args.ref_dir.fasta()) as fh:
        summary_df = species_summary_df(df, SeqIO.parse(fh, 'fasta'))

    with open(args.ref_dir.db()) as fh:
        ssu_db = pickle.load(fh)

    pcr_results_seq = RegionMultimap.parse_pcr_results_seq_level(
        args.ref_dir.region().joinpath('region_report.txt'),
        ssu_db,
        min_fraction=0.8)

    pcr_results = RegionMultimap.parse_pcr_results(
        args.ref_dir.region().joinpath('region_report.txt'),
        ssu_db,
        min_fraction=0.8)

    summary_df['predicted_amplify'] = pd.Series({
        s: ','.join(r)
        for s, r in pcr_results.iteritems() if s in summary_df.index
    })

    summary_df['predicted_amplify_seq'] = pd.Series({
        s: ','.join(r)
        for s, r in pcr_results_seq.iteritems() if s in summary_df.index
    })

    df.to_csv(args.output, sep='\t', index=False)
    summary_df.sort_values('sample_count', ascending=False, inplace=True)
    summary_df.to_csv(args.output + '.summary', sep='\t')


if __name__ == '__main__':
    main()
