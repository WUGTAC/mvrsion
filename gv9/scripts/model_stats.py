import cPickle as pickle
import sys
import collections as cl
import itertools as it

model_fp = sys.argv[1]

with open(model_fp, 'rb') as fh:
    model = pickle.load(fh)

total = cl.Counter(it.chain.from_iterable(it.chain.from_iterable(model.itervalues())))

num_models = cl.Counter(len(lst) for lst in model.itervalues())
model_len = cl.Counter(len(s) for s in it.chain.from_iterable(model.itervalues()))

print total
print 'number of models:'
print num_models
print 'length of models:'
print model_len
