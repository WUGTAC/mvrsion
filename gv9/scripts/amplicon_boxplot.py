import cPickle as pickle
import argparse
import collections as cl
import functools as ft

from path import path
import seaborn as sns
import pandas as pd


def get_sample_name(fp):
    return fp.basename().replace('.qname_to_region.pickle', '')


def read_pickle(fp_list):
    data = {}
    for fp in fp_list:
        with open(fp) as fh:
            qname_to_region = pickle.load(fh)
            counts = cl.Counter(qname_to_region.itervalues())
        data[get_sample_name(fp)] = counts
    amplicons = ft.reduce(lambda x, y: x | y , (set(d.keys()) for d in data.values()), set())
    default = {k: 0 for k in amplicons}
    for k in data.keys():
        d = default.copy()
        d.update(data[k])
        data[k] = d
    return pd.DataFrame.from_dict(data, orient='index')
    

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'input',
        type=path,
        help='path to a directory containing *qname_to_region.pickle files')

    parser.add_argument('output', type=path, help='path to write boxplot graph')
    parser.add_argument('--dpi', type=int)

    args = parser.parse_args()

    df = read_pickle(args.input.glob('*.pickle'))
    norm_df = df.div(df.sum(axis=1), axis=0)
    
    ax = sns.boxplot(data=norm_df, order=sorted(norm_df.columns))
    ax.set(xlabel='Amplicon', ylabel='Fraction of Total Reads')
    sns.plt.savefig(args.output, dpi=args.dpi)


if __name__ == '__main__':
    main()
