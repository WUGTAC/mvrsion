#!/usr/bin/env python
import pandas as pd
import pdb
import argparse
from path import path
from gv9.util.ssu_util import RdpFactory, DataDirectory
from gv9.microbeCall import KnownSample
'''
First argument can be a directory or a single file
Second argument is path to table containing relative abundance data for all samples
This script takes as input a directory containing relative abundance table with all samples and outputs a ppv table in the directory given by DataDirectory.ppv for each method represented in input
'''


def process_abundance_table(in_table, out_table, known, min_abundance=0):
    '''
    Assumes that the first column of in_table is the taxonomy string
    '''
    factory = RdpFactory()
    result_df = pd.read_table(in_table, sep='\t', index_col=0)
    result_df.index = [factory.create(x) for x in result_df.index]

    ppv_df = pd.DataFrame(columns=['TP', 'FP', 'FN'])

    def count(known, sample, series):
        called_rdp = series[series > min_abundance].index
        species_rdp = [r for r in called_rdp if r.get_level('s')]

        tp = len([c for c in species_rdp if known.abundance(sample, c) > 0])
        fp = len(species_rdp) - tp
        fn = known.count(sample) - tp
        return tp, fp, fn

    for raw_sample in result_df.columns:
        #skip any lines named taxonomy
        if 'taxonomy' in raw_sample.lower():
            continue

        #convert qiime-style samples to style in the known csv
        sample = raw_sample.replace('.', '-')
        ppv_df.loc[sample, ] = count(known, sample, result_df[raw_sample])

    ppv_df.sort(inplace=True)
    ppv_df.to_csv(out_table, sep='\t', index_label='Sample')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'input',
        type=path,
        help='table of relative abundances or directory of such')

    parser.add_argument(
        'known', type=path, help='table of known relative abundances')

    parser.add_argument(
        '-o',
        '--output',
        type=path,
        help='output path. Only used if input is a file')

    parser.add_argument(
        '-m',
        '--min_abundance',
        type=float,
        default=0,
        help='min relative abundance to consider a call valid. Mainly used for qiime'
    )

    args = parser.parse_args()

    known = KnownSample(args.known)

    #remember that there are two calls to process_abundance_table here
    if args.input.isdir():
        data_dir = DataDirectory(args.input)

        for fp in data_dir.results().glob('*'):
            method = fp.basename().replace('.taxa_summary.xls', '')
            out_fp = data_dir.ppv(sample='', method=method)
            process_abundance_table(
                fp, out_fp, known, min_abundance=args.min_abundance)

    elif args.input.isfile():
        out_fp = (args.output or args.input.stripext() + '.ppv.xls')
        process_abundance_table(
            args.input, out_fp, known, min_abundance=args.min_abundance)


if __name__ == '__main__':
    main()
