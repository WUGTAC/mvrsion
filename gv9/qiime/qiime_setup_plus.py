import os, re, argparse
import pkg_resources as pkg
from HTSeq import FastqReader
from path import path
from datetime import timedelta
import cPickle as pickle
import string
import json
import logging
import ruffus
import itertools as it
import pandas as pd

import gtac.startup
from gtac.util import smart_open
from gtac.ruffusUtil import auto_drm_job_factory, remove_empty_files, SUBMIT_JOB_LOGGING

from drm import get_drm_module

import gv9.util.ssu_util as ssu_util
import gv9.util.startup

# DATA_DIR = path('/home/comp/gtac/aschriefer/gtac_repo/gssu/qiime/')

# These are regions that are too long to join on with 150 bp reads
# The expected number of overlapping bases is max(0, 2x - L) x=read length, L=amplicon length
LONG_REGIONS = ('V1-V2', 'V5-V6', 'V7-V8')


class QiimeAnalysisDirectory(ssu_util.AnalysisDirectory):
    dirs = ssu_util.AnalysisDirectory.dirs.copy()
    dirs.update({'joined': ('joined', '{sample}.join.fq.gz')})


def add_qiime_label(seq, sample_name, i, post=''):
    seq.id = '{0}_{1}'.format(convert_to_qiime_sample(sample_name, post), i)
    seq.description = ''
    return seq


def convert_to_qiime_sample(*args):
    return re.sub(r'[^a-zA-Z0-9.]', '.', '.'.join(args))


def _create_qiime_seqs(analysis_dir, input_files, output_file, regions):
    #### THIS IS NOT INTEGRATED INTO QIIMEANALYSISDIRECTORY
    analysis_dir = QiimeAnalysisDirectory(analysis_dir)
    output_file = path(output_file)
    regions = set(regions)

    def _make_tuples(s, r):
        r_sample = convert_to_qiime_sample(s, r)
        return (r_sample, s, r, s)

    def _get_region_fp(fq):
        s = re.search(r'joined/(.*?)\.join', fq).group(1)
        return path('Regions').joinpath(s + '.qname_to_region.pickle')

    def _get_iter(fp, sample, regions, qname_to_region_fp):
        with open(qname_to_region_fp, 'rb') as qname_fh:
            qname_to_region = pickle.load(qname_fh)

        iread = 0

        #If there is only one region then dont add the region to sample name
        if len(regions) > 1:
            names = {r: convert_to_qiime_sample(sample, r) for r in regions}
        else:
            names = {regions.copy().pop(): convert_to_qiime_sample(sample)}

        num_reads = {r: 1 for r in regions}

        with smart_open(fp, 'r') as fh:
            for seq in FastqReader(fh):
                seq_region = find_region(qname_to_region, seq)
                if seq_region in regions:
                    seq.name = "{0}_{1}".format(
                        names[seq_region],
                        num_reads[seq_region], )
                    seq.descr = ''
                    num_reads[seq_region] += 1

                    yield seq

    samples = [
        convert_to_qiime_sample(re.sub(r'\.join.*', '', os.path.basename(p)))
        for p in input_files
    ]

    #Write minimal region sample key and normal sample key
    #region sample key is a hack to make qiime plus work
    region_df = pd.DataFrame.from_records(
        [_make_tuples(*e) for e in it.product(samples, regions)],
        columns=['#SampleID', 'SampleName', 'Region', 'Description'])

    region_df.to_csv('region_key_qiime.txt', sep='\t', index=False)

    sample_df = region_df
    sample_df.pop('#SampleID')
    sample_df.rename(columns={'SampleName': '#SampleID'}, inplace=True)
    sample_df.drop_duplicates(inplace=True)
    sample_df.to_csv('sample_key_qiime.txt', sep='\t', index=False)

    #write all reads matching input regions into combined_seqs.fna
    input_reads = \
      it.chain.from_iterable(
          _get_iter(
              fq,
              samp,
              regions,
              _get_region_fp(fq),
              )
          for fq, samp in it.izip(input_files, samples)
          )

    with smart_open(output_file, 'w') as fh_out:
        for seq in input_reads:
            seq.write_to_fasta_file(fh_out)


def find_region(qname_to_region, seq):
    return qname_to_region.get(seq.name)


def format_template_file(ref_name, template_fp, output_fp, **kwargs):
    values = {
        'hmp': {
            'id_to_taxonomy_fp':
            '/scratch/gtac/gtac_data/reference_sequences/microbes/hmp/rdp_taxonomy.txt',
            'reference_seqs_fp':
            '/scratch/gtac/gtac_data/reference_sequences/microbes/hmp/ref_16s_hmp.fna',
        },
        'gg': {
            'id_to_taxonomy_fp':
            '/scratch/gtac/gtac_data/reference_sequences/microbes/greengenes/gg_13_5_otus/taxonomy/97_otu_taxonomy.txt',
            'reference_seqs_fp':
            '/scratch/gtac/gtac_data/reference_sequences/microbes/greengenes/gg_13_5_otus/rep_set/97_otus.fasta',
        },
        'ncbi': {
            'id_to_taxonomy_fp':
            '/scratch/gtac/gtac_data/reference_sequences/microbes/ncbi_genome/rdp_taxonomy.txt',
            'reference_seqs_fp':
            '/scratch/gtac/gtac_data/reference_sequences/microbes/ncbi_genome/ref_16s_ncbi.fna',
        },
        'ncbi_shotgun': {
            'id_to_taxonomy_fp':
            '/scratch/gtac/gtac_data/reference_sequences/microbes/ncbi_shotgun/rdp_taxonomy.txt',
            'reference_seqs_fp':
            '/scratch/gtac/gtac_data/reference_sequences/microbes/ncbi_shotgun/ref_16s.fna',
        },
        'silva': {
            'id_to_taxonomy_fp':
            '/scratch/gtac/gtac_data/reference_sequences/microbes/silva_db/silva_123/multilevel_aligned/rdp_taxonomy.txt',
            'reference_seqs_fp':
            '/scratch/gtac/gtac_data/reference_sequences/microbes/silva_db/silva_123/multilevel_aligned/ref_16s.fna',
        },
    }  #yapf: disable

    with open(template_fp, 'r') as fh_in, open(output_fp, 'w') as fh_out:
        fh_out.write(
            string.Template(fh_in.read()).substitute(values[ref_name], **
                                                     kwargs))


def make_S_pattern(low=None, high=None):
    '''
    Used to capture only samples within the specified _S\d+_ range
    '''
    low = 1 if low is None else low
    high = 1000 if high is None else high

    if low > high:
        raise ValueError('low must be <= high')

    return '(?:{0})'.format('|'.join(map(str, range(low, high + 1))))


def job_namer(fp):
    return path(fp).relpath().replace('/', '-').lstrip('-')


def main():
    factory, _ = gtac.startup.bootstrap()
    factory.register(gv9.util.startup.MiseqDirectoryData)
    factory.register(gv9.util.startup.HiseqDirectoryData)

    parser = argparse.ArgumentParser(
        parents=[ruffus.cmdline.get_argparse()], add_help=False)
    parser.add_argument(
        '-o',
        '--output_dir',
        type=QiimeAnalysisDirectory,
        required=True,
        help='name of qiime analysis directory')
    parser.add_argument('--data', nargs='+', type=factory.build, required=True)
    parser.add_argument(
        '--ref',
        choices=['hmp', 'gg', 'ncbi', 'ncbi_shotgun', 'silva'],
        default='gg')
    parser.add_argument(
        '--qiime_fasta', type=path, default='combined_seqs.fna')
    parser.add_argument(
        '--primers',
        default=pkg.resource_filename('gv9', 'data/ssu_primers.txt'),
        type=gtac.cmdline.abs_exst_path, )
    parser.add_argument(
        '--adapters',
        default=pkg.resource_filename('gv9', 'data/adapters.fa'),
        type=gtac.cmdline.abs_exst_path, )
    parser.add_argument(
        '-r',
        '--regions',
        type=str,
        nargs='+',
        default=['V1', 'V2', 'V3_2', 'V4', 'V5_2', 'V6_1', 'V9'])

    parser.add_argument('--raw', action='store_true',
                        help='use if you want to join fastq reads before '+\
                        'they have been trimmed by scythe and sickle')

    parser.add_argument('-S_highest', type=int,
                        help='for miseq runs the fastq names are custID_S\d+ .'+\
                        'Fastqs with S higher than this value will be ignored')

    parser.add_argument('-S_lowest', type=int,
                        help='for miseq runs the fastq names are custID_S\d+ .'+\
                        'Fastqs with S lower than this value will be ignored')

    args = parser.parse_args()

    def make_link_name(_, sample, read):
        return args.output_dir.raw(sample=sample, read=read)

    if not args.output_dir.path.exists():
        args.output_dir.create(N_data=1)
        sample_table = args.output_dir.path.joinpath('samples.txt')
        gtac.startup.write_sample_table(args.data, sample_table,
                                        make_link_name)
        gtac.startup.fetch_fastq(sample_table)

        data_dir = pkg.resource_filename('gv9', 'data')
        format_template_file(
            args.ref,
            pkg.resource_filename('gv9', 'data/launch_qiime_template.txt'),
            args.output_dir.path.joinpath('launch_qiime.sh'),
            data_dir=data_dir)
        format_template_file(
            args.ref,
            pkg.resource_filename('gv9', 'data/qiime_parameters_template.txt'),
            args.output_dir.path.joinpath('qiime_parameters.txt'))

    args.output_dir.path.cd()

    logging.basicConfig(
        filename='log.txt',
        level=SUBMIT_JOB_LOGGING, )

    submit_job_lib = get_drm_module()
    RESOURCE = submit_job_lib.Resource()
    auto_drm_job = auto_drm_job_factory(
        submit_job_lib.Submitter(
            script='jobs', log='log'), job_namer)

    remove_empty_files(args.output_dir.path)
    raw_reads = args.output_dir.raw().glob('*')

    @ruffus.collate(raw_reads,
                    ruffus.formatter(r".+/(?P<sample>.*?)\.R[12]"),
                    args.output_dir.region("{sample[0]}"), "{sample[0]}", args)
    @auto_drm_job
    def region_match(input_fps, output_fp, sample_name, args):
        cmd = '''primerTrimmer -i {in_fq} -o {out_fq} --name \
        -p {primers} -a {adapters} --pickle {qname} \
        --untrimmed /dev/null /dev/null'''.format(
            in_fq=' '.join(input_fps),
            out_fq='/dev/null /dev/null',  #' '.join(output_fps),
            primers=args.primers,
            adapters=args.adapters,
            qname=output_fp, )
        resrc = RESOURCE.build(
            time=timedelta(minutes=45),
            memInGB=1, )
        return cmd, resrc

    @ruffus.collate(
        raw_reads,
        ruffus.formatter(r".+/(?P<sample>.*?)\.R[12]"),
        args.output_dir.joined("{sample[0]}"), )
    @auto_drm_job
    def join_reads(input_files, output_file):
        #Note that if you change adapter_trim to collate you might have to use input_files[0] instead
        output_template = re.sub(r'\.join\.', '.%.', output_file)
        cmd = 'fastq-join -v " " -m 6 {0} -o {1}'.format(' '.join(input_files),
                                                         output_template)
        resrc = RESOURCE.build(
            time=timedelta(minutes=45),
            memInGB=1, )
        return cmd, resrc

    @ruffus.follows(region_match)
    @ruffus.collate(
        [region_match, join_reads],
        ruffus.formatter(r".*"),
        args.qiime_fasta,
        set(args.regions), )
    @auto_drm_job
    def create_qiime_seqs(input_files, output_file, regions):
        '''
        We take region pickle and joined reads files as input just to hold correctly on them in queue.
        However right now the create_qiime_seqs fn takes only fastqs as input and gets pickles through a callback
        '''
        fastq_files = [
            fp for fp in input_files
            if re.search(r'(?:\.fq|\.fastq)(?:\.gz)?$', fp)
        ]
        with open('tmp', 'w') as fh_out:
            json.dump(
                [path.getcwd(), fastq_files, output_file,
                 list(regions)], fh_out)
        cmd = 'python -c \'import json; from gv9.qiime.qiime_setup_plus import _create_qiime_seqs; _create_qiime_seqs(*json.load(open("tmp", "r")))\''
        resrc = RESOURCE.build(
            time=timedelta(hours=1),
            memInGB=1, )
        return cmd, resrc

    ruffus.cmdline.run(args, checksum_level=0)


if __name__ == '__main__':
    main()
