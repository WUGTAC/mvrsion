#!/bin/bash -xe

#This script compares the beta diversity distance matrix of categories to detect significant differences
# using the qiime script compare_categories.py
#Assumes the weighted and unweighted unifrac distance matricies have been calculated.
key=$1

key_columns=(`head -n 1 $key`)
n_cats=$((${#key_columns[*]}-2))

if [ $n_cats -lt 1 ]; then
    echo "there are no categories to compute"
    exit 1
fi

categories=("${key_columns[@]:1:n_cats")

for category in "${categories[@]}"; do
    compare_categories.py --method permanova -m $key -c $category \
	-i bdiv/weighted_unifrac_dm.txt -o bdiv/weighted_unifrac_comparison

    compare_categories.py --method permanova -m $key -c $category \
	-i bdiv/unweighted_unifrac_dm.txt -o bdiv/unweighted_unifrac_comparison
    
done