import pandas as pd
import sh
import re
import argparse
import tempfile
import shutil
import pdb
from path import path
from functools import partial
from gtac.util import temp_file

@temp_file()
def convert_to_table(in_fp):
    '''
    Assumes that seqs are named sample_\d+
    '''
    df = pd.read_table(in_fp, sep='\t', header=None, usecols=([0,1] + range(2,7,2) + [7]), names=['seq', 'similarity', 'taxa_group', 'genus', 'species', 'bootstrap'])

    #only select entries with default sping_summary cutoffs for similarity and species bootstrap score
    df = df[(df['similarity'] >= 0.5) & (df['bootstrap'] >= 0.8)]

    #convert seq names to sample names
    regex = re.compile(r'_\d+\s*$')
    formatter = partial(regex.sub, '')
    df['sample'] = df['seq'].apply(formatter)

    #add in a count variable and pivot the table using sum
    df['count'] = 1
    df['#OTU ID'] = df[['genus', 'species']].apply(lambda x: 'g__{0};s__{1}'.format(*x), axis=1)

    new_df = pd.pivot_table(df, values='count', index='#OTU ID', columns='sample', aggfunc=pd.np.sum)
    new_df['taxonomy'] = new_df.index

    out_fp = tempfile.mkstemp()[1]
    
    new_df.to_csv(out_fp, sep='\t', na_rep='0')
    
    return out_fp


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=path)
    parser.add_argument('-o', '--output', type=path)
    parser.add_argument('--tsv', action='store_true', help='save output as tsv else save as biom')
    
    args = parser.parse_args()

    with convert_to_table(args.input) as tsv_fp:
        if args.tsv:
            shutil.copyfile(tsv_fp, args.output)
        else:
            sh.biom.convert('-i', tsv_fp, '-o', args.output, '--to-tsv', '--header-key=taxonomy')

            
if __name__ == '__main__':
    main()            
