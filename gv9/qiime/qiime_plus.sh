#!/bin/bash -x
#Note that compute_core_microbiome does not round down or up
# for fraction * number samples. So require 2 regions out of 6 you need
# to have fraction=30 (6*.3=1.8 < 2) not 40 (6*.4=2.4 > 2)
percent=$1
key=$2

fraction=$(echo "scale=1; x=$percent / 100; print 0; x" | bc)
sentinel_fraction=$(echo "scale=2; x=$percent / 100 + .01; print 0; x" | bc)

collapse_key=collapsed_sample_key_qiime.txt

otu=otu_table_mc2_w_tax.biom

combine_otu=combined_otu.biom
collapse_otu=collapsed_otu_table.biom
filter_otu=filter_otu_table.biom

final_otu="all_region_otu_table.biom"

trap "rm -f $collapse_otu; rm -fr core_tmp*; rm -f $filter_otu" EXIT

#should change this to only include samples with reads in them
samples_array=( $(awk 'NR > 1 {print $2}' $key | uniq) )

core_biom_files=''
for sample in "${samples_array[@]}"; do
    
    tmp_dir="core_tmp_$sample"

    compute_core_microbiome.py -i $otu -o $tmp_dir \
	--min_fraction_for_core $fraction \
	--max_fraction_for_core $sentinel_fraction \
	--num_fraction_for_core_steps 2 \
	--mapping_fp $key --valid_states "SampleName:$sample"

    filter_samples_from_otu_table.py \
	-i "$tmp_dir/core_table_$percent.biom" \
	-o "$tmp_dir/$filter_otu" \
	--mapping_fp $key \
	--valid_states "SampleName:$sample"
    
    collapse_samples.py -b "$tmp_dir/$filter_otu" -m $key \
	--output_biom_fp "$tmp_dir/$collapse_otu" --output_mapping_fp "$tmp_dir/$collapse_key" \
	--collapse_fields SampleName --collapse_mode median

    if [[ "$?" -eq 0 ]]; then
	core_biom_files+="$tmp_dir/$collapse_otu,"
    fi
    
done

merge_otu_tables.py -i ${core_biom_files%,} -o $final_otu

#filter_otus_from_otu_table.py -i $collapse_otu -o $filter_otu --min_count_fraction 0.005
#mv $collapse_otu $final_otu
#normalize_table.py -i $combine_otu -o $final_otu -a CSS
# if [ "${#samples_array[@]}" -gt 1 ]
# then
#     normalize_table.py -i $collapse_otu -o $final_otu -a CSS
# else    
#     mv $filter_otu $final_otu
# fi

echo "$final_otu"