#!/bin/bash -xe

#$ -V
#$ -cwd
#$ -l h_vmem=3g

input=( $raw_reads )
trimmed=( "${input[@]/%.fq.gz/_trim.fq}" )

trap "rm -f ${trimmed[@]}" EXIT

$$primer_trimmer -i $${input[@]} -o $${trimmed[@]} -a $adapter 
$$fastq_join -v " " -m 8 $${trimmed[@]} -o $output
