#!/usr/bin/env python

#This script takes as input a R1 fq.gz files (R2 directory determined in script)
#and searches each read pair in each file in directory for the region primer
#pairs to determine which variable region the read is from.  The output is a pickle file
#of a dictionary {read_name : region} into input_dir/../Regions/
import pdb, gzip, argparse, itertools
import cPickle as pickle
from Bio import SeqIO
from gv9.util.ssu_util import regionMatcherList

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', nargs=2, required=True)
parser.add_argument('-o', '--output', required=True)
parser.add_argument('--primers', default='/home/comp/gtac/aschriefer/src/qiime_plus/primers.csv')

args = parser.parse_args()

def region_search(rmatchList, read1, read2):
    qname_to_region = {}

    fh1 = gzip.open(read1, 'rb')
    fh2 = gzip.open(read2, 'rb')

    read1_seqiter = SeqIO.parse(fh1, 'fastq')
    read2_seqiter = SeqIO.parse(fh2, 'fastq')


    for record1,record2 in itertools.izip(read1_seqiter, read2_seqiter):
        seq1 = str(record1.seq[:30])
        seq2 = str(record2.seq[:30])

        qname_to_region[record1.id] = rmatchList.find_match(seq1, seq2)
            
    return qname_to_region


ssu_regions = regionMatcherList(args.primers)
qname_to_region = region_search(ssu_regions, *args.input)

pickle_fh = open(args.output, 'wb')
pickle.dump(qname_to_region, pickle_fh, -1)
pickle_fh.close()
