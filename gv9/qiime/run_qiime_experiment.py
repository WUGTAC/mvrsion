import click
import sh
import itertools as it
import os
import pkg_resources as pkg

from path import path

TEST = [['V3_2', 'V4'], ['silva']]
#TEST = [['V3_2', 'V4'], ['gg']]


@click.group()
def main():
    pass


@main.command()
@click.argument('demux', nargs=-1)
def setup(demux):
    for region, database in it.product(*TEST):
        name = '-'.join([database, region])
        data = ' '.join(demux)
        cmd = '''setup_qiime \
        --data {data} -r {region} --ref {database} -o {name}'''.format(
            **locals())
        os.system(cmd)


@main.command()
def launch():
    for root, dirs, files in os.walk(os.getcwd()):
        # we dont want to relaunch qiime in directories that already have been run
        if 'launch_qiime.sh' in files and (not any('tax_otu' in d
                                                   for d in dirs)):
            os.system(
                'cd %s && sbatch launch_qiime.sh combined_seqs.fna tax_otu' %
                root)


@main.command()
@click.argument(
    'out',
    type=click.Path(resolve_path=True),
    required=False,
    default='results')
def results(out):
    result_dir = path(out)

    for root, dirs, files in os.walk(os.getcwd()):
        if 'otu_table_mc2_w_tax.biom' in files:

            cmd = '''
cd {} && summarize_taxa.py -i otu_table_mc2_w_tax.biom -L 1,2 -o tax --suppress_biom_table_output
'''.format(root)
            os.system(cmd)
            for lvl in ['', '_genus']:
                known = pkg.resource_filename(
                    'gv9', 'data/validation_knowns{}.txt'.format(lvl))
                # next we need to call report_results on this taxa table
                exp = path(root).parent.name
                ppv_table = result_dir.joinpath(exp + lvl + '.xls')
                taxa = 'otu_table_mc2_w_tax_L2.txt' if not lvl else 'otu_table_mc2_w_tax_L1.txt'
                cmd2 = r'''
cd {root}/tax
sed -r -e '{{1d; s/#OTU/OTU/; s!^g__([A-Za-z]+)\t!g__\1;s__\1\t!g}}' {taxa} > tmp && mv tmp {taxa}
report_results {taxa} {known} -o {ppv_table}
'''.format(**locals())
                print(cmd2)
                os.system(cmd2)


if __name__ == '__main__':
    main()
