#!/bin/bash -xe
compare_file=$1
KEY=$2
depth=$3
min_sample_for_otu=$4 #Generally choose 25% of total number of samples
RAW_OTU_TABLE=$5

#RAW_OTU_TABLE='otu_table_mc2_w_tax_no_pynast_failures.biom'
OTU_TABLE='otu_table_rarefied.biom'

#CATEGORY='Group'

SAMPLE_FILTER='sample_filter.biom.tmp'
OTU_FILTER='otu_filter.biom.tmp'

trap 'rm -f $SAMPLE_FILTER; rm -f $OTU_FILTER' EXIT

#normalize_table.py -i $RAW_OTU_TABLE -o $OTU_TABLE -a CSS
single_rarefaction.py -i $RAW_OTU_TABLE -o $OTU_TABLE -d $depth

filter_otus_from_otu_table.py -i $OTU_TABLE -o $OTU_FILTER \
    --min_samples $min_sample_for_otu

while read LINE; do
    grps=($LINE)
    category=${grps[0]}
      OUTPUT="${category}_anova.xls"

      # filter_samples_from_otu_table.py -i $OTU_TABLE -o $SAMPLE_FILTER \
      # 	-m $KEY -s "$category:${grps[1]},${grps[2]}"

      # filter_otus_from_otu_table.py -i $SAMPLE_FILTER -o $OTU_FILTER \
      # 	--min_count 10 --min_samples 3

      
      group_significance.py -i $OTU_FILTER -m $KEY -c $category -o $OUTPUT \
      	--test kruskal_wallis

      eval trap -p EXIT #qiime commands usually freak out if output exists

    
done < $compare_file
	