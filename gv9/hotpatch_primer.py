from path import path
import os

for read1 in path('Reads1').glob('*filtered.fq.gz'):
    read2 = read1.replace('Reads1', 'Reads2').replace('R1', 'R2')
    os.system('/home/comp/gtac/aschriefer/gtac_repo/primerTrimmer_htseq.py -i {0} {1} -o tmp.1.fq.gz tmp.2.fq.gz --name -p /home/comp/gtac/aschriefer/gtac_repo/gssu/ssu_primers.txt'.format(read1, read2))
    os.system('mv tmp.1.fq.gz %s' % read1)
    os.system('mv tmp.2.fq.gz %s' % read2)
