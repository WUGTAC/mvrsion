import pandas as pd
import re

raw = pd.read_table('/home/home/aeschriefer/burnham_2nd.csv', sep=',', header=[0,1])

def process_into_df(fp, sample):
    call_df = pd.read_table(fp, sep='\t')
    call_df['tax'] = call_df['tax'].apply(lambda x: re.sub(r';?[a-z]__', '', x))
    call_df.set_index('tax', inplace=True)
    call_df[sample] = call_df[sample] / float(call_df[sample].sum() + 1)
    return call_df
    

df_dict = {}
for sample in raw.columns.levels[0]:
    raw_sample = raw[sample].drop('db-silva', axis=1).drop('db-combined', axis=1).dropna(how='any')
    raw_sample['organism'] = raw_sample['organism'].apply(lambda x: re.sub(r'_', ' ', x))
    raw_sample.set_index('organism', inplace=True)
    raw_sample.index.name = 'tax'

    silva_calls = process_into_df('/scratch/gtac/analysis/16s_seq/burnham/silva-aligned3_second/Data/Data/Calls/{0}.og_species.call.txt'.format(sample), sample)
    silva_calls.rename(columns={sample: 'db-silva'}, inplace=True)

    # need to verify which calls i gave them
    combined_calls = process_into_df('/scratch/gtac/analysis/16s_seq/burnham/combined_second/Data/Data/Calls/{0}.og_species.call.txt'.format(sample), sample)

    combined_calls.rename(columns={sample: 'db-combined'}, inplace=True)

    data = pd.concat([raw_sample, silva_calls, combined_calls], join='outer', axis=1)
    data.reset_index(level=0, inplace=True)
    
    df_dict[sample] = data

final = pd.concat(df_dict, axis=1)
final.to_csv('/home/aeschriefer/burnham_2nd_final.csv', sep=',', na_rep='0', index=False)
