import pdb
from functools import partial, wraps
import pandas as pd
import logging
import re
import fuzzywuzzy.process
import collections as cl
import itertools as it


class CallMethod(object):
    '''
    The dataframe with only microbes that were called is self.called.
    The total species level dataframe is self.total.
    CallMethod.call() takes as the first argument a dataframe with SeqID for rows,
      variable regions in columns and a tax column containing RdpString.
    The workflow here is that MicrobeCaller initializes a CallMethod object and calls CallMethod.call()
    '''

    def __init__(self):
        self.total = None
        self.called = None

    def call(self, df, **kwargs):
        return self

    @property
    def not_called(self):
        if self.total is None or self.called is None:
            return None
        else:
            no_call_index = self.total.index.difference(self.called.index)
            return self.total.loc[no_call_index]

    @staticmethod
    def make_species_df(sequence_mapping_df):
        '''
        Aggregate on the raw value of the tax column strings
        '''
        return sequence_mapping_df.groupby('tax').sum()

    @staticmethod
    def region_consensus(analysis_df, min_regions=2, threshold=100, **kwargs):
        '''
        Takes any dataframe and only keeps rows that have >= threshold reads
        in at least min_regions columns
        '''
        pass_th = analysis_df.apply(lambda x: x >= threshold)
        return analysis_df[pass_th.sum(axis=1) >= min_regions]

    def evaluate(self, metric_fn, knownSample, sample, **kwargs):
        y_pred = []
        y_true = []

        for rdp in self.called.index:
            y_pred.append(1)
            y_true.append(1 if knownSample.abundance(sample, rdp) else 0)

        #we need to add fake entries for FN
        FN = knownSample.count(sample) - sum(y_true)

        y_pred.extend([0] * FN)
        y_true.extend([1] * FN)

        return metric_fn(y_true, y_pred, **kwargs)


class SimpleSpecies(CallMethod):
    def call(self, df, min_regions=2, threshold=100, **kwargs):
        '''
        Returns subset of analysis_df of rows that contain min_regions
        number of regions with >= thresh reads
        '''
        self.total = self.make_species_df(df)
        self.called = self.region_consensus(self.total,
                                            min_regions=min_regions,
                                            threshold=threshold)
        return self


class ExpSimpleSpecies(SimpleSpecies):
    def call(self, df, min_regions=2, threshold=100, base=10, **kwargs):
        self.total = self.make_species_df(df)
        total_called = pd.DataFrame()
        for iexp in xrange(0, min_regions):
            r = min_regions - iexp
            th = threshold * base**iexp
            called = self.region_consensus(self.total,
                                           min_regions=r,
                                           threshold=th)
            not_yet_seen = called.loc[called.index.difference(
                total_called.index)]
            total_called = pd.concat(
                (total_called, not_yet_seen),
                axis=0,
                join='outer',
                copy=False, )
        self.called = total_called
        return self


class Multimap(SimpleSpecies):
    def call(self, df, novoMapping, db_seqID=None, all_regions=None, **kwargs):
        '''
        df is a sequence_mapping_df
        called_seqs is a list of SeqIDs that we believe to be true calls
        db_seqs is a list of SeqIDs for all sequences in the alignment database
        all_regions is a list of all region names
        '''
        called_seqs = self.region_consensus(df, **kwargs).index

        output_seq_df = pd.DataFrame({r: novoMapping.multimap(df, called_seqs,
                                                              db_seqID, r)
                                      for r in all_regions})
        output_seq_df['tax'] = df['tax']

        super(Multimap, self).call(output_seq_df, **kwargs)

        return self


class DynamicRegions(CallMethod):
    def call(self, df, model=None, threshold=100, **kwargs):
        self.total = self.make_species_df(df)
        self.called = self.dynamic_call(self.total, model, threshold=threshold)
        return self

    @classmethod
    def dynamic_call(cls, analysis_df, species_to_regions, threshold=100):
        selector = analysis_df.apply(cls.apply_model,
                                     axis=1,
                                     model=species_to_regions,
                                     th=threshold)
        return analysis_df[selector]

    @staticmethod
    def apply_model(series, model=None, th=0):
        try:
            regions = model[series.name]
        except KeyError:
            logging.warn('no region model for taxa %s' % series.name)
            return False
        #the model can be a set of regions or a list of sets of regions
        else:
            if isinstance(regions, set):
                return (series[regions] >= th).all()
            else:
                return any((series[r] >= th).all() for r in regions)


class UnionCall(DynamicRegions):
    '''
    Assumes that the model is created from a NON backup style RegionMultimap
    ie MultiModelRegionMultimap
    '''

    def call(self, df, model=None, threshold=100, **kwargs):
        self.total = self.make_species_df(df)
        model_called = self.dynamic_call(self.total,
                                         model,
                                         threshold=threshold)
        simple_called = ExpSimpleSpecies().call(df,
                                                threshold=threshold,
                                                **kwargs).called
        self.called = self.total.loc[model_called.index.union(
            simple_called.index)]
        return self


class TwoRoundCall(DynamicRegions):
    def __init__(self):
        self.first_call = SimpleSpecies()
        super(TwoRoundCall, self).__init__()

    def call(self,
             df,
             multimap=None,
             threshold=100,
             min_regions=None,
             **kwargs):
        """
        :param pandas.DataFrame df: Indexed by ssu_util.SeqID and containing 'tax' column
        :param gv9.regionSelect.SoftBackupMultimap multimap:
        :param 2-tuple min_regions:
        :return: self
        """
        min_regions = (min_regions or (4, 4))
        self.first_call.call(df,
                             threshold=threshold,
                             min_regions=min_regions[0])

        model = multimap.find_minimal_intersect(
            N=min_regions[1],
            called_rdp=self.first_call.called.index, )

        return super(TwoRoundCall, self).call(df, model, threshold=threshold)


class ThreeRoundCall(DynamicRegions):
    def __init__(self):
        self.first_caller = SimpleSpecies()
        super(ThreeRoundCall, self).__init__()

    def call(self,
             df,
             multimap=None,
             threshold=100,
             min_regions=None,
             **kwargs):
        """
        :param pandas.DataFrame df: Indexed by ssu_util.SeqID and containing 'tax' column
        :param gv9.regionSelect.SoftBackupMultimap multimap:
        :param 2-tuple min_regions:
        :return: self
        """
        min_regions = (min_regions or (4, 4))
        self.first_caller.call(df,
                               threshold=threshold,
                               min_regions=min_regions[0])

        first_model = multimap.find_minimal_intersect(
            N=min_regions[1],
            called_rdp=self.first_caller.called.index, )

        second_caller = DynamicRegions()
        second_caller.call(df, first_model, threshold=threshold)

        second_model = multimap.find_minimal_intersect(
            N=min_regions[1],
            called_rdp=second_caller.called.index, )

        return super(ThreeRoundCall, self).call(df,
                                                second_model,
                                                threshold=threshold)


class KnownSample(object):
    '''
    This class takes as input a table containing known relative abundances of samples.
    Each row is a microbe and each column is a sample.
    Given a call in rdp string form and a sample name this object returns the true relative abundance of the microbe.
    If the bug is not actually present then the abundance is 0
    '''

    def __init__(self, fp):
        self.df = pd.read_table(fp, sep='\t', index_col=0)

    def abundance(self, sample, rdp):
        standard = self.make_rdp_to_standard([rdp])[rdp]
        if standard is not None:
            return self.df.loc[standard, self.get_sample(sample)]
        else:
            return 0

    def count(self, sample):
        '''
        Return the number of microbes present in the known sample
        '''
        return len(self.df[self.df[self.get_sample(sample)] > 0].index)

    def get_sample(self, sample):
        fmt = re.sub(r'[^0-9a-zA-Z]', '-', sample)
        matches = fuzzywuzzy.process.extract(fmt, self.df.columns, limit=2)
        if matches[0][1] < 0.95 or matches[0][1] == matches[1][1]:
            raise KeyError('sample %s does not match a known sample' % sample)

        return matches[0][0]

    def make_rdp_to_standard(self, rdp_list):
        '''
        Try different methods to match a rdp call to the simple names in
        the known file.  Sometimes in the rdp strings the common name is genus + species
        other times it is just species.
        The spingo rdp database has common name as _ joined species name
        '''
        rdp_to_standard = {}
        for rdp in rdp_list:
            methods = (rdp.get_level('s').lower(),
                       rdp.get_level('s').lower().replace('_', ' '), )
            standard = next((m for m in methods if m in self.df.index), None)
            rdp_to_standard[rdp] = standard

        return rdp_to_standard

    def rename_to_rdp(self, rdp_index):
        '''
        Returns a new dataframe that has matching microbe index renamed as rdp
        '''
        mapping = {std: rdp
                   for rdp, std in self.make_rdp_to_standard(
                       rdp_index).iteritems() if std is not None}
        return self.df.rename(index=mapping)


class MicrobeCaller(object):
    def __init__(self):
        self._name_to_method = {}
        self._name_to_kwargs = {}
        self._name_to_args = {}
        self._results = {}

    def add_method(self, name, method, *args, **kwargs):
        '''
        method is a class of type CallMethod
        The __init__ of method must take no arguments.
        CallMethod object must have a method call(seq_df, *args, **kwargs)
        The positional args must be added here, only seq_df and **kwargs
        is passed by run().
        Any kwargs passed here overwrite kwargs passed to run for this method
        Method must return a df indexed by species 
        '''
        keywords = {}
        keywords.update(kwargs)
        self._name_to_kwargs[name] = keywords
        self._name_to_args[name] = args
        self._name_to_method[name] = method

    def run(self, sequence_df, **kwargs):

        for name, method in self._name_to_method.iteritems():

            keywords = self._name_to_kwargs[name].copy()
            kwargs.update(keywords)
            self._results[name] = method()
            self._results[name].call(sequence_df, *self._name_to_args[name],
                                     **kwargs)

        return self._results

    def make_sequence_mapping_df(self,
                                 bamfile,
                                 ssu_db,
                                 read_to_region,
                                 all_regions=None):
        '''
        all_regions is a list of all variable region names selected to be part
        of the analysis.  It is used to clean the df to ensure all seqs have
        an entry for all the model regions.

        if all_regions is None then there is no cleaning of the columns
        all regions present in the bamfile are returned as columns.
        any regions with no alignments are not present
        '''

        region_to_seqid_counts = cl.defaultdict(cl.Counter)

        for align in bamfile.fetch(until_eof=True):
            if align.is_proper_pair and align.is_read1 and align.qname:
                seq = bamfile.getrname(align.tid)
                region = read_to_region[align.qname]
                region_to_seqid_counts[region][seq] += 1

        analysis_df = pd.DataFrame.from_dict(region_to_seqid_counts,
                                             orient='columns')

        analysis_df = self.standardize_regions(analysis_df,
                                               all_regions=all_regions)
        #Reduce taxa classification level to species only
        analysis_df['tax'] = pd.Series(
            [ssu_db[seqID].remove_level('t') for seqID in analysis_df.axes[0]],
            index=analysis_df.axes[0])

        return analysis_df

    def standardize_regions(self, analysis_df, all_regions=None):
        if all_regions is not None:
            #Select only regions in all_regions
            regions_to_drop = set(analysis_df.columns).difference(all_regions)
            analysis_df.drop(regions_to_drop, axis=1, inplace=True)

            #Add columns containing all zeros for regions that didnt show up at all
            add_regions = set(all_regions).difference(analysis_df.columns)
            analysis_df = analysis_df.join(pd.DataFrame(
                {r: 0
                 for r in add_regions},
                index=analysis_df.index))
            return analysis_df
        else:
            return analysis_df


class SoftPairedMicrobeCaller(MicrobeCaller):
    def make_sequence_mapping_df(self,
                                 bamfile,
                                 ssu_db,
                                 read_to_region,
                                 all_regions=None):
        '''
        Assumes that the input bam is read name sorted
        '''
        region_to_seqid_counts = cl.defaultdict(cl.Counter)

        for qname, pair in it.groupby(
                bamfile.fetch(until_eof=True),
                key=lambda x: x.qname):
            pair_list = list(pair)
            #only take reads if both pairs are mapped to the same species
            if all(not align.is_unmapped for align in pair_list) and \
               len(set(ssu_db.get_species(bamfile.getrname(align.tid)) for align in pair_list)):

                region = read_to_region[pair_list[0].qname]
                seq = bamfile.getrname(pair_list[0].tid)
                region_to_seqid_counts[region][seq] += 1

        analysis_df = pd.DataFrame.from_dict(region_to_seqid_counts,
                                             orient='columns')
        analysis_df = self.standardize_regions(analysis_df,
                                               all_regions=all_regions)
        #Reduce taxa classification level to species only
        analysis_df['tax'] = pd.Series(
            [ssu_db.get_species(seqID) for seqID in analysis_df.axes[0]],
            index=analysis_df.axes[0])

        return analysis_df


class NotPairedMicrobeCaller(MicrobeCaller):
    def make_sequence_mapping_df(self,
                                 bamfile,
                                 ssu_db,
                                 read_to_region,
                                 all_regions=None):
        region_to_seqid_counts = cl.defaultdict(cl.Counter)
        for align in bamfile.fetch(until_eof=True):
            if not align.is_unmapped:
                region = read_to_region[align.qname]
                seq = bamfile.getrname(align.tid)
                region_to_seqid_counts[region][seq] += 1

        analysis_df = pd.DataFrame.from_dict(region_to_seqid_counts,
                                             orient='columns')
        analysis_df = self.standardize_regions(analysis_df,
                                               all_regions=all_regions)
        #Reduce taxa classification level to species only
        analysis_df['tax'] = pd.Series(
            [ssu_db.get_species(seqID) for seqID in analysis_df.axes[0]],
            index=analysis_df.axes[0])

        return analysis_df
