#!/usr/bin/env python
import pandas as pd
import sys
import os
import pdb

'''
Takes a list of filepaths to gv9 ppv files (tab sep with headers TP FP FN)
 and prints out the basic stat summary of each one
'''

def print_five_stats(fp):
    print 'File name : %s' % fp
    df = pd.read_table(fp, sep='\t', header=0, index_col=0)
    print str(df.describe())
    print ''


if __name__ == '__main__':
    for fp in sys.argv[1:]:
        print_five_stats(os.path.abspath(fp))
