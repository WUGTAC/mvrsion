#!/usr/bin/env python
import argparse
import cPickle as pickle
import itertools
import logging
import re
import sys
from collections import defaultdict, Counter

import pandas as pd
import pysam
from path import path

import gv9.microbeCall as mc
import gv9.regionSelect
from gv9.novoCluster import run_novoalign, create_tmp_novoindex
from gv9.util.ssu_util import AnalysisDirectory, ReferenceDirectory, DataDirectory, read_parameter_file
'''
This is the main analysis script of gv9
'''

REGION_NAMES = ('V1-V2', 'V2', 'V3_2', 'V4', 'V5-V6', 'V6_1', 'V7-V8')


def get_ssu_argparser():
    base_parser = argparse.ArgumentParser(add_help=False)
    base_parser.add_argument(
        '-i',
        '--input',
        required=True,
        type=DataDirectory,
        help='path to DataDirectory with input bam and novocluster')

    base_parser.add_argument('--sample', required=True)

    base_parser.add_argument(
        '-r',
        '--ref_dir',
        required=True,
        type=ReferenceDirectory,
        help='path to directory containing reference files for analysis')

    base_parser.add_argument(
        '--analysis',
        required=True,
        type=AnalysisDirectory,
        help='path to main analysis directory')

    base_parser.add_argument(
        '-p',
        '--parameters',
        type=read_parameter_file,
        default={},
        help='yaml file containing commandline arguments for this program'
        'this file must have propert PyYaml type flags as it is loaded directly into'
        ' argparse.Namespace')

    base_parser.add_argument(
        '--known',
        type=path,
        help='path to list of microbes known to be in sample')

    base_parser.add_argument(
        '-m',
        '--min_regions',
        type=int,
        default=3,
        help='number of regions needed to be present to call a species')

    base_parser.add_argument(
        '-t',
        '--read_threshold',
        type=float,
        default=5e-4,
        help='fraction of total reads needed to call a region as present')

    base_parser.add_argument(
        '--tval',
        type=int,
        default=60,
        help='t-value to be passed as argument to novoalign for clustering and sub-db alignment'
    )

    base_parser.add_argument(
        '--no_align', action='store_true', help='pass to not do realignment')

    base_parser.add_argument(
        '--threads',
        type=int,
        default=1,
        help='number of threads to use for novoalign')

    base_parser.add_argument(
        '--novo_opts',
        default='-k -i PE 100-400 -r Random',
        help='string to pass to novoalign during novoCluster protocol')

    base_parser.add_argument(
        '--save',
        action='store_true',
        help='flag to save tmp files for testing/debugging')

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subparser_name')

    parser_init = subparsers.add_parser('init', parents=[base_parser])
    parser_init.add_argument(
        '-o', '--output', type=DataDirectory, help='path to output directory')

    parser_init.set_defaults(func=run_initial, min_regions=2)

    #####SECONDARY PROCESS ARGUMENTS
    parser_filter = subparsers.add_parser('filter', parents=[base_parser])
    parser_filter.add_argument(
        '--novo',
        action='store_true',
        help='Flag to perform novoalign procedure on sub-db based off of final calls'
    )
    parser_filter.add_argument(
        '--peer_threshold',
        type=float,
        default=0.05,
        help='min multimap probability between two seqs to determine peers for the trial'
    )

    parser_filter.set_defaults(func=run_filter)

    return parser


def read_known_set(known_fp):
    if known_fp is None:
        return None

    with open(known_fp, 'r') as fh:
        k = set(' '.join(x.lower().strip().split()[:2]) for x in fh)

    return k


def make_sequence_mapping_df(bamfile, ssu_db, read_to_region,
                             all_regions=None):
    '''
    all_regions is a list of all variable region names selected to be part
    of the analysis.  It is used to clean the df to ensure all seqs have
    an entry for all the model regions.

    if all_regions is None then there is no cleaning of the columns
    all regions present in the bamfile are returned as columns.
    any regions with no alignments are not present
    '''

    region_to_seqid_counts = defaultdict(Counter)

    for align in bamfile.fetch(until_eof=True):
        if align.is_proper_pair and align.is_read1 and align.qname:
            seq = bamfile.getrname(align.tid)
            region = read_to_region[align.qname]
            region_to_seqid_counts[region][seq] += 1

    analysis_df = pd.DataFrame.from_dict(
        region_to_seqid_counts, orient='columns')

    if all_regions is not None:
        #Select only regions in all_regions        
        regions_to_drop = set(analysis_df.columns).difference(all_regions)
        analysis_df.drop(regions_to_drop, axis=1, inplace=True)

        #Add columns containing all zeros for regions that didnt show up at all
        add_regions = set(all_regions).difference(analysis_df.columns)
        analysis_df = analysis_df.join(
            pd.DataFrame(
                {r: 0
                 for r in add_regions}, index=analysis_df.index))

    #Reduce taxa classification level to species only
    analysis_df['tax'] = pd.Series(
        [ssu_db[seqID].remove_level('t') for seqID in analysis_df.axes[0]],
        index=analysis_df.axes[0])

    return analysis_df


def realign_on_sub_db(novoalign_opts,
                      ref=None,
                      fastq=None,
                      taxid=None,
                      bam_fp=None):
    with create_tmp_novoindex(ref, taxid) as ndx:
        run_novoalign(novoalign_opts, fastq, ndx, bam_fp)


def write_raw_reads_table(method_name_to_result,
                          output_dir,
                          sample='',
                          knownSample=None):
    '''
    Writes a table that shows the read counts that are used in a method
    to make the final consensus calls (this can be after modification from raw)
    for all microbes that were called.  Does not show uncalled bugs.

    Only runs if known file is passed because we annotate by TP
    '''
    if knownSample is None:
        return None

    def _make_table(known, rdp_list):
        d = {
            rdp: {
                'TP': knownSample.abundance(sample, rdp),
                'Genus': rdp.get_level('g'),
                'Species': rdp.get_level('s')
            }
            for rdp in rdp_list
        }  #yapf: disable

        return pd.DataFrame.from_dict(d, orient='index')

    for method_name, result in method_name_to_result.iteritems():
        output_fp = output_dir.joinpath('%s.%s.read_table.xls' %
                                        (sample, method_name))

        tmp_df = _make_table(knownSample, result.called.index)
        final_df = tmp_df.join(result.called)
        final_df.sort(['Genus', 'TP'], ascending=[1, 0], inplace=True)
        final_df.to_csv(
            output_fp, sep='\t', na_rep='0', index=False, index_label=False)


def write_no_call_raw_reads_table(method_name_to_result,
                                  output_dir,
                                  sample='',
                                  knownSample=None):
    '''
    Writes a table that shows the read counts that are used in a method
    to make the final consensus calls (this can be after modification from raw)
    for all microbes that were called.  Does not show uncalled bugs.

    Only runs if known file is passed because we annotate by TP
    '''
    if knownSample is None:
        return None

    def _make_table(known, total_rdp_list, not_called_rdp_list):
        d1 = {
            rdp: {
                'TP': knownSample.abundance(sample, rdp),
                'Genus': rdp.get_level('g'),
                'Species': rdp.get_level('s')
            }
            for rdp in not_called_rdp_list
        }  #yapf: disable

        #now we include things in known that were not found at all
        # ie no alignments whatsoever in the analysis
        rdp_known = known.rename_to_rdp(total_rdp_list)[known.get_sample(
            sample)]
        unaligned = rdp_known.loc[rdp_known.index.difference(total_rdp_list)]

        d2 = {
            ind: {
                'TP': unaligned.loc[ind],
                'Genus': '',
                'Species': ind,
            }
            for ind in unaligned.index if unaligned.loc[ind] > 0
        }  #yapf: disable

        d1.update(d2)
        return pd.DataFrame.from_dict(d1, orient='index')

    for method_name, result in method_name_to_result.iteritems():
        output_fp = output_dir.joinpath('%s.%s.read_table_no_call.xls' %
                                        (sample, method_name))

        tmp_df = _make_table(knownSample, result.total.index,
                             result.not_called.index)

        final_df = tmp_df.join(result.not_called)
        final_df = final_df.loc[final_df['TP'] >
                                0]  #only look at false negatives
        final_df.sort(['Genus', 'TP'], ascending=[1, 0], inplace=True)
        final_df.to_csv(
            output_fp, sep='\t', na_rep='0', index=False, index_label=False)


def write_total_reads_table(method_name_to_result, output_dir, sample=''):
    def _make_table(rdp_list):
        d = {
            rdp: {
                'Genus': rdp.get_level('g'),
                'Species': rdp.get_level('s')
            }
            for rdp in rdp_list
        }  #yapf: disable

        return pd.DataFrame.from_dict(d, orient='index')

    for method_name, result in method_name_to_result.iteritems():
        output_fp = output_dir.joinpath('%s.%s.total.xls' %
                                        (sample, method_name))

        tmp_df = _make_table(result.total.index)
        final_df = tmp_df.join(result.total)
        final_df.sort(['Genus'], ascending=[1], inplace=True)
        final_df.to_csv(
            output_fp, sep='\t', na_rep='0', index=False, index_label=False)


def write_relative_abundance(method_name_to_result, data_dir, sample=''):
    '''
    Right now this just takes the mean of reads assigned to all regions for a species
    '''
    for method_name, result in method_name_to_result.iteritems():
        output_fp = data_dir.call(sample=sample, method=method_name)

        out_series = result.called.mean(axis=1)
        out_series.index.name = 'tax'
        out_series.name = sample
        out_series.to_csv(output_fp, sep='\t', index=True, header=True)


def write_result_files(args, method_name_to_result):
    '''
    Remember that call files are written at same level directory
    as input bam files came from.
    '''
    knownSample = mc.KnownSample(
        args.known) if args.known is not None else None

    #These files are used downstream so pass a DataDirectory
    write_relative_abundance(
        method_name_to_result, data_dir=args.input, sample=args.sample)

    #These files arent tracked by DataDirectory so pass the raw dir path
    write_raw_reads_table(
        method_name_to_result,
        output_dir=args.input.call(),
        sample=args.sample,
        knownSample=knownSample, )

    write_no_call_raw_reads_table(
        method_name_to_result,
        output_dir=args.input.call(),
        sample=args.sample,
        knownSample=knownSample, )

    write_total_reads_table(
        method_name_to_result,
        output_dir=args.input.call(),
        sample=args.sample, )


def run_initial(args, write_out=True, caller=None, realign=True, **kwargs):
    if caller is None:
        caller = mc.SoftPairedMicrobeCaller()
        caller.add_method('og_species', mc.ExpSimpleSpecies, base=5)

    ssu_db = pickle.load(open(args.ref_dir.db(), 'rb'))

    with pysam.AlignmentFile(args.input.align(args.sample), 'rb') as bamfile:
        read_to_region = pickle.load(
            open(args.analysis.region(args.sample), 'rb'))
        sequence_df = caller.make_sequence_mapping_df(
            bamfile, ssu_db, read_to_region, all_regions=REGION_NAMES)

    min_reads_per_region = max(
        10, int(sequence_df.ix[:, :-1].sum().sum() * args.read_threshold))

    logging.info('min reads per region is : %s' % min_reads_per_region)
    results = caller.run(sequence_df,
                         threshold=min_reads_per_region,
                         **vars(args))

    #some function kwarg control of behavior
    if write_out:
        write_result_files(args, results)

    if not realign:
        return results

    #all taxIDs that match all species called
    #TODO: think about only writing seqs that get called
    taxID_set = set(
        itertools.chain.from_iterable(
            ssu_db.rdp_to_taxid(rdp)
            for rdp in results['og_species'].called.index))

    if len(taxID_set) == 0:
        logging.info('No taxa called, stopping model building early')
        return None

    if args.no_align:
        logging.info('Skipping realignment and region model building')
        return None

    novo_opts = args.novo_opts + ' -c {} -t {}'.format(args.threads, args.tval)
    cluster_pickle = args.output.cluster(args.sample)
    with open(cluster_pickle, 'wb') as fh:
        #optionally save the results of novoalign -r All
        bam_fp = args.output.path.joinpath(args.sample +
                                           '.all.bam') if args.save else None
        novo_opts_all = re.sub(r'-r \w+', '-r All 400', novo_opts)
        # Performs the novoalign -r All procedure
        region_multimap = gv9.regionSelect.select_best_regions(
            novo_opts_all,
            ssu_db,
            REGION_NAMES,
            ref_dir=args.ref_dir,
            taxid=taxID_set,
            output_fp=bam_fp, )

        pickle.dump(region_multimap, fh, -1)

    fastq_fp_list = [
        args.analysis.read(
            args.sample, read=r) for r in range(1, 3)
    ]

    realign_on_sub_db(
        novo_opts,
        ref=args.ref_dir.fasta(),
        fastq=fastq_fp_list,
        taxid=taxID_set,
        bam_fp=args.output.align(args.sample), )


def run_filter(args, write_out=True, caller=None, **kwargs):
    #change this to a try block for the with. Also should refactor novoalign all to this section
    if not args.input.cluster(args.sample).exists():
        logging.info(
            'No dynamic model was made in previous step. Ending early')

        #have to write an empty dataframe here
        df = pd.DataFrame(columns=['tax', args.sample])
        df.to_csv(
            args.input.call(
                sample=args.sample, method='og_species'), sep='\t')
        return None

    if caller is None:
        multimap = pickle.load(open(args.input.cluster(args.sample), 'rb'))
        caller = mc.SoftPairedMicrobeCaller()
        caller.add_method(
            'og_species',
            mc.ThreeRoundCall,
            multimap=multimap,
            min_regions=(4, 4))

    ssu_db = pickle.load(open(args.ref_dir.db(), 'rb'))

    #this is not safe if input bam does not exist
    with pysam.AlignmentFile(args.input.align(args.sample), 'rb') as bamfile:
        read_to_region = pickle.load(
            open(args.analysis.region(args.sample), 'rb'))
        sequence_df = caller.make_sequence_mapping_df(
            bamfile, ssu_db, read_to_region, all_regions=REGION_NAMES)

    min_reads_per_region = max(
        10, int(sequence_df.ix[:, :-1].sum().sum() * args.read_threshold))

    logging.info('min reads per region is : %s' % min_reads_per_region)
    results = caller.run(sequence_df,
                         threshold=min_reads_per_region,
                         **vars(args))

    #Just write into Calls/ at same level as input
    if write_out is True:
        write_result_files(args, results)

    return results


def main(**kwargs):
    argv = sys.argv[1:]
    FORMAT = '%(relativeCreated)d %(pathname)s %(lineno)d :: %(message)s'
    logging.basicConfig(stream=sys.stderr, level=logging.INFO, format=FORMAT)

    args = get_ssu_argparser().parse_args(argv)

    #parameters yaml file is parsed into a dict
    config_values = args.parameters.get(args.subparser_name)

    #overwrite values passed on commandline with those in parameter file
    if config_values is not None:
        values = vars(args).copy()
        values.update(config_values)

        new_args = argparse.Namespace(**values)
    else:
        new_args = args

    return args.func(new_args, **kwargs)


def cli():
    '''
    This is for use in distutils entry_points for setup.py
    main() returns an object and this makes sys.exit (used by entry_point)
    to return a non-zero exit code. Changing this to return None will
    cause test_filter.py to stop working, so I just wrap main() here
    '''
    main()
    return None


if __name__ == '__main__':
    cli()
