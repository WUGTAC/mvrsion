import seaborn as sns
import pandas as pd
import re
import math
import argparse
import warnings
import sklearn.metrics
from path import path
from gv9.util.ssu_util import RdpFactory

import microbeCall as mc
'''
This script takes as input a relative abundance table with all samples and
outputs various graphics relating to the know abundances of the samples
'''


#The header of the called_df should have all the sample names
def false_negative_abundance_histogram(called_df,
                                       knownSample=None,
                                       output_fp=None):
    samples = called_df.columns

    #only look at knowns within samples in the called_df
    known_stack = knownSample.rename_to_rdp(
        called_df.index).loc[:, samples].stack()

    #filter out zero values from knowns
    known_stack = known_stack[known_stack > 0]

    #first make everything a FN then later on we update the called to TP
    df = pd.DataFrame(
        {
            'log10': known_stack.apply(lambda x: int(math.log10(x))),
            'call': 'FN'
        },
        index=known_stack.index)

    #take intersection of called and known to get false negatives
    #all microbe/sample rows here are truly present so anything with called=False is a FN
    called_df_norm = called_df.apply(lambda x: x / x.sum())
    called_df_stack = called_df_norm.stack()
    called_df_stack = called_df_stack[called_df_stack > 0]

    index = df.index.intersection(called_df_stack.index)
    df.loc[index, 'call'] = 'TP'

    #now add in the false positives
    fp_index = called_df_stack.index.difference(df.index)
    #pdb.set_trace()
    called_fp_df = pd.DataFrame(
        {
            'log10':
            called_df_stack.loc[fp_index].apply(lambda x: int(math.log10(x))),
            'call': 'FP'
        },
        index=fp_index)

    final_df = pd.concat([df, called_fp_df], axis=0).sort('log10')

    #final_df['call'] = final_df['call'].astype('category', categories=['FN', 'TP', 'FP'], ordered=False)

    if output_fp is not None:
        sns.set_style('whitegrid')
        sns.factorplot(
            x='log10',
            hue='call',
            kind='count',
            data=final_df,
            palette={'TP': 'blue',
                     'FN': 'red',
                     'FP': 'green'})
        sns.plt.savefig(output_fp)

    return final_df


## def false_positive_abundance_histogram(called_df, knownSample=None, output_fp=None):
##     samples = called_df.columns

##     #only look at knowns within samples in the called_df
##     known_stack = knownSample.rename_to_rdp(called_df.index).loc[:,samples].stack()

##     #Filter out zero values from knowns
##     known_stack = known_stack[known_stack > 0]


def abundance_scatterplot(called_df, knownSample=None, output_fp=None):
    '''
    called df has columns of sample names and rows species names.
    
    If output_fp is None this function does not write a graph and
    just returns the R2 score of the scatterplot.
    
    The scatterplot is log transformed before plotted / scored
    '''
    warnings.warn('This function has been hacked to munge sample names!!!!')
    called_df = called_df.rename(columns=lambda x: re.sub(r'[_.]', '-', x))

    samples = called_df.columns

    #only look at knowns within samples in the called_df
    known_stack = knownSample.rename_to_rdp(
        called_df.index).loc[:, samples].stack()

    #filter out zero values from knowns
    known_stack = known_stack[known_stack > 0]

    #normalize the called_df to make sure we are looking at relative abundances
    called_df_norm = called_df.apply(lambda x: x / x.sum())
    called_df_stack = called_df_norm.stack()

    df = pd.concat(
        {
            'called': called_df_stack[called_df_stack > 0],
            'known': known_stack
        },
        axis=1,
        join='outer', )

    #only look at TP calls
    df.dropna(axis=0, how='any', inplace=True)

    log_transform_df = df.apply(pd.np.log10)

    if output_fp is not None:
        g = sns.jointplot(
            x='known',
            y='called',
            data=log_transform_df,
            kind='reg',
            xlim=(-6, 0),
            ylim=(-5.5, 0))
        g.set_axis_labels('log10 known abundance', 'log10 called abundance')
        sns.plt.savefig(output_fp, dpi=1200)

    return sklearn.metrics.r2_score(log_transform_df['known'],
                                    log_transform_df['called'])


def relative_abundance(species_df, sample):
    '''
    Takes the mean of reads assigned to all regions for a species
    The sample is assigned as the name of the output pd.Series
    '''
    out_series = species_df.mean(axis=1)
    out_series.index.name = 'tax'
    out_series.name = sample

    return out_series


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'input', type=path, help='table of relative abundances')

    parser.add_argument(
        'known', type=path, help='table of known relative abundances')

    parser.add_argument('output', type=path, help='output filepath template')

    args = parser.parse_args()

    called_df = pd.read_table(args.input, sep='\t', index_col=0)

    factory = RdpFactory()
    called_df.index = [factory.create(ind) for ind in called_df.index]

    known = mc.KnownSample(args.known)

    # false_negative_abundance_histogram(
    #     called_df,
    #     knownSample=known,
    #     output_fp=args.output + '.fn_histogram.png')
    abundance_scatterplot(
        called_df, knownSample=known, output_fp=args.output + '.abundance.png')
