import time

import pytest
import sh
from path import path

from gtac.startup import MiseqData
from gtac.util import run_command

from gv9.util.ssu_util import AnalysisDirectory
import gv9.util.ssu_util
'''
To make an existing analysis directory you need to add the raw fastq files,
the trimmed fastqs, the read_to_amplicon.pickle, and Data/Alignments/*.bam
'''


@pytest.fixture(params=[True, False])
def oldest_miseq_run(request):
    config = gv9.util.config.get_system_config()
    miseq_path = path(config.get('data', 'miseq'))
    run_dir = min(
        (d
         for d in miseq_path.glob('*')
         if d.isdir() and MiseqData.valid_token(d.name)),
        key=lambda x: x.getctime())
    data = MiseqData(run_dir.name, miseq_path)
    num_reads = sum(len(e) for e in data.get_sample_to_fastq().values())
    return run_dir.name if request.param else run_dir, num_reads


@pytest.fixture(params=[True, False])
def out_dir(request, tmpdir_factory):
    p = tmpdir_factory.mktemp('sub')
    if request.param:
        p.remove(rec=1)
    return p


@pytest.fixture()
def known_fp(pkg_dir):
    return pkg_dir.joinpath('data', 'validation_knowns.txt')


# Problem with this is that this will break if references change
# I could freeze a version of silva and have a silva_test config option
# could even add it to git
@pytest.mark.parametrize("analysis_dir,config", [
    ('test_analysis_dir', None),
    ('test_analysis_dir', 'config14.yml'),
])
def test_existing_analysis_dir(analysis_dir, config, test_dir, work_dir,
                               known_fp):
    '''
    Tests a complete run with analysis dir setup. Trimmed and first alignment already done
     and --known being passed
    '''

    # find the absolute path of the test data
    full_analysis_dir = test_dir.joinpath(analysis_dir)

    # move the reads over into a directory we can submit jobs safely from
    sh.rsync('-r', '--delete', full_analysis_dir.rstrip('/'), work_dir)

    adir = AnalysisDirectory(work_dir.joinpath(analysis_dir))

    cmd_str = 'gv9 -o %s --ref_dir test --known %s' % (adir.path, known_fp)

    full_config = test_dir.joinpath('config', str(config))
    cmd_str += ' -p %s' % full_config if config is not None else ''

    # make sure all the files have proper timemarks for ruffus to process
    touch_str = cmd_str + ' --touch_files_only'
    run_command(touch_str)

    p = run_command(cmd_str)
    p.wait()
    assert int(p.returncode) == 0
    time.sleep(300)  # This will fail if jobs don't get queued right away
    assert len(adir.get_data_dir(1).ppv().glob('*')) > 0
    # In the future I should assert what calls we expect to make


def test_setup_analysis_dir(out_dir, oldest_miseq_run):
    '''
    Tests the initialization and linking in of reads 
    '''
    d = AnalysisDirectory(out_dir)
    data, num_reads = oldest_miseq_run
    cmd_str = 'gv9 -o {0} --touch_files_only -T primer_trim -v 1 --data {1}'.format(
        out_dir, data)
    p = run_command(cmd_str)
    p.wait()
    assert int(p.returncode) == 0
    assert len(d.raw().glob('*')) == num_reads
    assert len(d.read().glob('*')) == num_reads  # Ensure that pipeline can run
