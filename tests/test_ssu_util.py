import sys

import pytest
import functools
from collections import OrderedDict
from gv9.util.ssu_util import *
from gv9.util.ssu_util import _RdpString


sys.path.insert(0, '../')
sys.path.insert(0, './')



class MiniRdpString(_RdpString):
    rdp_label_hierarchy = ('g', 's')


@pytest.fixture()
def factory():
    return RdpFactory()


@pytest.fixture()
def mini_factory():
    return RdpFactory(MiniRdpString)
    

def test_ssu_ids():
    tax = '480'
    seq = '480:0'
    region = '480:0:V4'
    assert str(TaxID(tax)) == tax
    assert str(TaxID(seq)) == tax
    assert str(SeqID(seq)) == seq
    assert str(SeqID(region)) == seq
    
    rID = RegionID(region)
    assert str(rID) == region
    assert rID.tax == tax
    assert rID.seq == '0'
    assert rID.region == 'V4'

    
    
    with pytest.raises(TypeError) as e:
        SeqID(tax)

    with pytest.raises(TypeError) as e:
        RegionID(seq)


    #test str and repr methods
    assert str(TaxID(rID)) == tax
    assert repr(rID) == "RegionID(tax='480', seq='0', region='V4')"
        
    assert str(SeqID(tax, 0)) == seq #check that non-string input can be printed
    assert str(SeqID(tax, seq=0)) == seq    
    assert str(SeqID(tax, seq='0')) == seq

    assert hash(SeqID(tax, seq='0')) == hash(SeqID(tax, seq=0)) #str cast of **kwargs
    assert hash(SeqID(tax, seq='0')) == hash(SeqID(tax, 0)) #str cast of *args
    assert hash(SeqID(int(tax), seq='0')) == hash(SeqID(tax, 0)) #str cast of first positional arg

    assert hash(RegionID(region)) == hash(RegionID((480, 0, 'V4'))) #str cast of iterable input

    
def test_SsuDataBase_rdp_to_taxid(test_dir, factory):
    ssu_db = SsuDataBase(test_dir.joinpath('data', 'rdp_tax.txt'))
    
    rdp_sp = factory.create('k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Faecalibacterium;s__fake1;t__')
    
    rdp_genus = factory.create('k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Faecalibacterium')

    rdp_family = factory.create('k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae')

    # This string is not in the taxonomy file
    rdp_not_present = factory.create('k__NotReal;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Faecalibacterium;s__fake1')

    rdp_no_lineage = factory.create('k__;p__;c__;o__;f__;g__;s__Bacteroides vulgatus;t__')

    assert ssu_db.rdp_to_taxid(rdp_sp) \
      == set([TaxID('4'), TaxID('8'), TaxID('9')])
      
    assert ssu_db.rdp_to_taxid(rdp_genus) \
      == set([TaxID('4'), TaxID('8'), TaxID('9'), TaxID('5')])

    assert ssu_db.rdp_to_taxid(rdp_family) \
      == set([TaxID('4'), TaxID('8'), TaxID('9'), TaxID('5'), TaxID('3')])

    assert ssu_db.rdp_to_taxid(rdp_not_present) == set()

    assert ssu_db.rdp_to_taxid(rdp_no_lineage) == set([TaxID('10')])


def test_rdp(factory):
    rdp = factory.create('k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Faecalibacterium;s__fake1;t__A')
    
    #make sure trailing ; doesnt matter
    assert rdp == \
      factory.create('k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Faecalibacterium;s__fake1;t__A;')

    #make sure capitalization doesn't matter
    assert rdp == \
      factory.create('k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Faecalibacterium;s__ FaKe1 ;t__a;')

    assert rdp == \
      factory.create(
          OrderedDict(
              [('k','Bacteria'),('p','Firmicutes'),('c','Clostridia'),('o','Clostridiales'),('f','Ruminococcaceae'),('g','Faecalibacterium'),('s','fake1'),('t','A')]
              )
          )

    #make sure capitalization doesn't matter 
    assert rdp == \
      factory.create(
          OrderedDict(
              [('k','Bacteria'),('p','Firmicutes'),('c','Clostridia'),('o','Clostridiales'),('f','Ruminococcaceae'),('g','Faecalibacterium'),('s',' FaKe1 '),('t','a')]
              )
          )

    #test that rdp handles this qiime situation properly
    rdp = factory.create('k__Bacteria;p__Actinobacteria;c__Actinobacteridae;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;Other')
    assert rdp == 'k__Bacteria;p__Actinobacteria;c__Actinobacteridae;o__Bifidobacteriales;f__Bifidobacteriaceae;g__Bifidobacterium;s__;t__'


def test_silva_parsing(factory):
    rdp = factory.create_from_unlabeled('Bacteria;Actinobacteria;Actinobacteria;Corynebacteriales;Corynebacteriaceae;Corynebacterium 1;Corynebacterium tuberculostearicum')

    assert rdp == 'k__Bacteria;p__Actinobacteria;c__Actinobacteria;o__Corynebacteriales;f__Corynebacteriaceae;g__Corynebacterium 1;s__Corynebacterium tuberculostearicum;t__'


def test_dunder_in_species(factory):
    raw = "k__Bacteria;p__Cyanobacteria;c__Cyanobacteria;o__Subsectioniii;f__Familyi;g__Leptolyngbya;s__Leptolyngbya 'albertano-kovacik green' ecfyyy__00;t__"

    assert factory.create(raw) == raw
    

def test_mini_rdp_parse(factory, mini_factory):
    
    rdp = factory.create('g__A;s__B')
    
    assert rdp.pretty == 'g__A;s__B'
    assert rdp.tokens == {'k' : '', 'p' : '', 'c' : '', 'o' : '', 'f' : '', 'g' : 'A', 's' : 'B', 't' : ''}

    mini_rdp = mini_factory.create('g__A;s__B')
    assert mini_rdp == 'g__A;s__B'
    assert mini_rdp.tokens == {'g' : 'A', 's' : 'B'}

    #now check that a mini rdp can be promoted to standard rdp
    assert factory.create(mini_rdp).tokens \
      == {'k' : '', 'p' : '', 'c' : '', 'o' : '', 'f' : '', 'g' : 'A', 's' : 'B', 't' : ''}
    

def test_mini_demote_rdp(factory, mini_factory):
    # test demotion of a raw string
    full_str = 'k__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;f__Ruminococcaceae;g__Faecalibacterium;s__ FaKe1 ;t__a;'
    mini_rdp = mini_factory.create(full_str)
    assert mini_rdp == 'g__Faecalibacterium;s__Fake1'
    assert mini_rdp.tokens == {'g': 'Faecalibacterium', 's': 'Fake1'}

    # test demotion of a full rdp object
    full_rdp = factory.create(full_str)
    mini_rdp = mini_factory.create(full_rdp)
    assert mini_rdp == 'g__Faecalibacterium;s__Fake1'
    assert mini_rdp.tokens == {'g': 'Faecalibacterium', 's': 'Fake1'}


def test_rdp_slicing(factory):
    raw = 'k__;p__;c__;o__;f__;g__;s__Bacteroides vulgatus;t__'
    rdp = factory.create(raw)
    assert rdp.take_level('s') == raw
    assert rdp.take_level('g') == 'k__;p__;c__;o__;f__;g__;s__;t__'

    assert rdp.get_level('s') == 'Bacteroides vulgatus'
    

#this test is broken
@pytest.mark.skipif(True, reason='broken test')
def test_analysis_directory():
    a_dir = AnalysisDirectory('analysis')
    a_dir.path.rmtree_p()
    a_dir.create(N_data=2)
    a_dir.path.cd()
    for d in a_dir.data_dir.itervalues():
        assert d.exists()

    assert a_dir.joinpath(*a_dir.dirs['data'] * 2).exists()
    assert not a_dir.joinpath(*a_dir.dirs['data'] * 3).exists()

    with pytest.raises(OSError):
        a_dir.mkdir()

    a_dir.rmtree()
    
    
