import logging
import cPickle as pickle
import itertools as it

import pytest
from path import path
from sklearn import metrics

import gv9.util.ssu_util as util
import gv9.microbeCall as mc
import gv9.regionSelect as rs
from gv9.ssu import main


logging.basicConfig(filename='results.txt', mode='w')


def data():
    return (
        mc.KnownSample('/home/aeschriefer/gv9/known/validation_knowns.txt'),
        util.AnalysisDirectory(
            '/scratch/gtac/analysis/16s_seq/gv9_validation/silva-aligned2_tworound_A49W5_good_regions_config16//'),
    )


def tp_and_fp(y_true, y_pred):
    tp = sum(1 for t, p in it.izip(y_true, y_pred) if t == 1 and p == 1)
    fp = sum(1 for t, p in it.izip(y_true, y_pred) if t == 0 and p == 1)
    return 'TP : %s FP : %s' % (tp, fp)


@pytest.fixture(scope='session', params=[0.8])
def reference(request):
    #assume that this doesnt change for subclasses
    #Adding more cases to this will cause bams to be re-read
    ref_dir = util.ReferenceDirectory(
        '/scratch/gtac/gtac_data/reference_sequences/microbes/silva_db/silva_123/aligned2')
    ssu_db = pickle.load(open(ref_dir.db(), 'r'))

    report = ref_dir.path.joinpath('pprospector', 'region_report.txt')
    pcr_results = rs.RegionMultimap.parse_pcr_results(
        report,
        ssu_db,
        min_fraction=request.param)
    return ref_dir, ssu_db, pcr_results, request.param


@pytest.fixture(scope='session')
def simple_reference():
    ref_dir = util.ReferenceDirectory(
        '/scratch/gtac/gtac_data/reference_sequences/microbes/silva_db/silva_123/aligned2')
    return ref_dir


@pytest.fixture(params=[
    (rs.SoftBackupMultimap, {'N': 4}),
    #(rs.MultiModelRegionMultimap, {}),
])
def multimap_method(request):
    return request.param
    #return rs.RegionMultimap


@pytest.fixture(params=[
    (mc.TwoRoundCall, {'min_regions': (3, 4)}),
    (mc.TwoRoundCall, {'min_regions': (3, 3)}),
])
def call_method(request):
    return request.param


@pytest.fixture(params=[
    (mc.ExpSimpleSpecies, {'base': 5}),
    (mc.ExpSimpleSpecies, {'base': 10}),
])
def init_call_method(request):
    return request.param


@pytest.fixture(params=[(mc.SimpleSpecies, ), (mc.ExpSimpleSpecies, ), ])
def init_method(method):
    return method


@pytest.fixture(params=[mc.SoftPairedMicrobeCaller])
def caller(request):
    return request.param()


@pytest.fixture(
    scope='session',
    params=[path('/scratch/gtac/analysis/16s_seq/gv9_validation/config16.yml')]
)
def data_fixture(request):
    #(ref_dir, knownSample, analysis_dir, list of sample names, config_path)
    return data() + tuple([request.param, ])


#'BEI', '48G-even', '1-0-120111-100G-A2-FP',
# add K-0-Axilla sometime else, i think first pass didnt call anything
@pytest.fixture(scope='session',
                params=['BEI', '48G-even', 'A-0-Axilla', 'A-0-Groin',
                        'K-0-Nose', 'K-0-Groin'])
def sample(request, reference):
    #assume that construction does not change for RegionMultimap subclasses
    _, ssu_db, pcr_results, _ = reference
    _, adir = data()
    sample = request.param
    rs_map = rs.RegionMultimap.from_bam(
        adir.get_data_dir(1).path.joinpath(sample + '.all.bam'),
        ssu_db,
        in_silico_pcr=pcr_results, )
    return sample, rs_map


@pytest.fixture(scope='session',
                params=['BEI', '48G-even', 'A-0-Axilla', 'A-0-Groin',
                        'K-0-Nose', 'K-0-Groin'])
def simple_sample(request):
    _, adir = data()
    sample_name = request.param
    with open(adir.get_data_dir(1).cluster(sample_name), 'rb') as fh:
        regionMultimap = pickle.load(fh)

    return sample_name, regionMultimap


def test_filter_method(simple_reference, data_fixture, multimap_method,
                       call_method, simple_sample, caller):
    ref_dir = simple_reference
    known, adir, config = data_fixture
    sample, regionMultimap = simple_sample
    multimap_class, intersect_kwargs = multimap_method
    call_class, call_kwargs = call_method

    print ' '.join(map(str,
                       [config, multimap_class, intersect_kwargs, call_class,
                        call_kwargs, caller.__class__, sample]))

    multimap = multimap_class.from_existing(regionMultimap)
    regionModel = multimap.find_minimal_intersect(**intersect_kwargs)
    #pdb.set_trace()
    cli = 'filter -i {in_} -r {ref} --analysis {adir} -p {config} --sample {s}'.format(
        in_=adir.get_data_dir(1),
        ref=ref_dir,
        config=config,
        adir=adir,
        s=sample, )

    caller.add_method('test',
                      call_class,
                      model=regionModel,
                      multimap=multimap,
                      **call_kwargs)

    results = main(cli.split(), write_out=False, caller=caller)

    print 'f1_score : %s  %s' %\
        (results['test'].evaluate(metrics.f1_score, known, sample),
         results['test'].evaluate(tp_and_fp, known, sample),
        )


@pytest.mark.parametrize('sample_name,threshold,region', list(it.product(
    ['BEI', '48G-even', '1-0-120111-100G-A2-FP', 'A-0-Axilla', 'A-0-Groin',
     'K-0-Nose', 'K-0-Groin'], ['5e-4', '1e-4', '5e-5'], ['3', '4'])))
def test_init_method(init_call_method, caller, simple_reference, sample_name,
                     threshold, region):
    call_class, call_kwargs = init_call_method
    ref_dir = simple_reference
    known, adir = data()
    sample = sample_name

    print ' '.join(map(str, [threshold, region, caller.__class__,
                             call_class, call_kwargs, sample]))

    cli = 'init -i {in_} -r {ref} --analysis {adir} -t {threshold} -m {region} --sample {s}'.format(
        in_=adir.get_data_dir(0),
        ref=ref_dir,
        threshold=threshold,
        region=region,
        adir=adir,
        s=sample, )

    caller.add_method('test', call_class, **call_kwargs)

    results = main(cli.split(), write_out=False, caller=caller, realign=False)

    print 'recall : %s  precision : %s  %s' % \
        (results['test'].evaluate(metrics.recall_score, known, sample),
         results['test'].evaluate(metrics.precision_score, known, sample),
         results['test'].evaluate(tp_and_fp, known, sample),)
