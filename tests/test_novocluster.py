import sys

import pytest
from path import path
from gv9.novoCluster import *
from gv9.util.ssu_util import TaxID


sys.path.insert(0, '../')
sys.path.insert(0, './')



#N is the number of sequences matching taxids
#Errors can still occur for large tax_list if the fastqs arent flushed
@pytest.mark.parametrize("ref,tax_list,N", [
    ('ncbi_shotgun', ['1121278', '1070319', '690566'], 5),
    ('silva/silva_119', ['1000565', '1000566', '1000570'], 5)
])
def test_create_tmp_bam(ref, tax_list, N):
    ref_dir = path('/GTAC/analysis1/reference_sequences/microbes').joinpath(ref)
    ref_fasta = ref_dir.joinpath('ref_16s.fna')
    taxid = map(TaxID, tax_list)
    opts = '-c 1'

    temp_fp = []
    #check that novoindex has the right number of sequences
    with create_tmp_novoindex(ref_fasta, set(taxid)) as ndx:
        headers = sh.novoutil.headers(ndx)
        #novoutils headers includes one header line
        assert len(headers.strip().split('\n')) == N + 1
        temp_fp.append(ndx)

    with create_region_fastq_reads(ref_dir.joinpath('regions'), set(taxid)) as fq_tuple:
        reads = map(lambda x: list(SeqIO.parse(x.file, 'fastq')), fq_tuple)
        assert len(reads[0]) > 0 and len(reads[0]) == len(reads[1])
        temp_fp.extend(map(lambda x: path(x.name), fq_tuple))

        
    with create_tmp_bam(opts, ref_fasta, ref_dir.joinpath('regions'), set(taxid)) as bam:
        fh = pysam.AlignmentFile(bam, 'rb') #check that file is bam formatted
        fh.fetch(until_eof=True) #check that we can retrieve alignments, should be a better way
        fh.close()
        temp_fp.extend([bam, path(bam+'.bai')])

    assert all(not fp.exists() for fp in temp_fp)

    
def test_protocol():    
    ref_dir = path('/GTAC/analysis1/reference_sequences/microbes/ncbi_shotgun')
    taxid = set([TaxID('1121278'), TaxID('1070319'), TaxID('690566')])
    opts = '-c 1'
    region_name = ['V1-V2', 'V1', 'V2', 'V3_1', 'V3_2', 'V4', 'V5_1', 'V5_2',
                   'V5-V6', 'V6_1', 'V6_2', 'V6_3', 'V7-V8', 'V9']

    
    NovoMappingPanda(region_name) #make sure the object can be created
    x = novoCluster(opts, ref_dir, taxid)
    assert isinstance(x.mapping[('1070319','0')]['V1']['map'], pd.Series)
    
    #make sure it can be pickled
    tmp = 'tmp.pickle'
    pickle.dump(x, open(tmp, 'wb'), -1)
