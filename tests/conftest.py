import pytest
import gv9.regionSelect as rs
import gv9.util.config
import gv9.util.ssu_util
from path import path


@pytest.fixture
def pkg_dir():
    return path(__file__).realpath().dirname().parent.joinpath('gv9')


@pytest.fixture
def test_dir():
    return path(__file__).realpath().dirname()


@pytest.fixture
def work_dir():
    return path(gv9.util.config.get_system_config().get('test', 'work_dir'))


@pytest.fixture
def ref_dir():
    return gv9.util.ssu_util.ReferenceDirectory(
        gv9.util.config.get_system_config().get('reference', 'silva'))


@pytest.fixture
def combined_ref_dir():
    return gv9.util.ssu_util.ReferenceDirectory(
        gv9.util.config.get_system_config().get('reference', 'combined'))


@pytest.fixture
def rdp_factory():
    return gv9.util.ssu_util.RdpFactory()


@pytest.fixture
def region_multimap_args():
    """
    Follows the naming pattern of test_microbeCall.py seq_df fixture
    """
    C = rs.RegionMultimap.Record
    recs = [C('A', 'V1', 'C'), C('A', 'V2', 'C'), C('A', 'V3', 'C'),
            C('B', 'V1', 'C'), C('B', 'V2', 'A'), C('C', 'V1', 'A'),
            C('C', 'V3', 'A')]
    species_list = ['A', 'B', 'C']
    all_regions = ['V1', 'V2', 'V3']
    pcr_report = {sp: ['V1', 'V2', 'V3'] for sp in species_list}
    return recs, species_list, pcr_report, set(all_regions)
