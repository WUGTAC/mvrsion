from sklearn.grid_search import GridSearchCV
from sklearn.base import BaseEstimator
from tempfile import mkdtemp
from path import path
from functools import partial
import logging
import random
import numpy as np
import pandas as pd
import pysam
import cPickle as pickle
import os
import re
import sh
import pdb

import gv9.microbeCall as mc
import gv9.util.ssu_util as util
import gtac.util
from gv9.ssu import make_sequence_mapping_df
from gv9.novoCluster import run_novoalign
import gv9.reporting


def data():
    #sklearn is a harsh mistress
    ref_dir = util.ReferenceDirectory('/scratch/gtac/reference_sequences/microbes/silva_db/silva_119')
    return {
            'known' : mc.KnownSample('/home/aeschriefer/gv9/known/validation_knowns.txt'),
            'N_reads' : 1e5,
            'ref_dir' : ref_dir,
            'adir' : util.AnalysisDirectory(
                '/scratch/gtac/16s_seq/gv9_validation/full_combined_config2'),
        }


@gtac.util.temp_file(scalar=False)
def subset_fastq(fq_list, N=1e6):
    '''
    Args are filepaths to gzipped fastq files
    '''
    tmp = [sh.mktemp(u=True, p='/tmp').stdout.strip()
           for _ in fq_list]

    for t,fq in zip(tmp, fq_list):
        os.mkfifo(t, 0600)
        os.system('gunzip -c {fq} | head -n {n} > {t}&'.format(fq=fq, n=4*int(N), t=t))
        #sh.head(sh.gunzip(fq, c=True), n=4*int(N), _out=t, _bg=True)
        
    return tmp
    

def get_sample_names(adir):
    '''
    Places mixture sample names at the front
    '''
    names = [x.basename().replace('.R1.fq.gz', '') for x in adir.read().glob('*.R1.fq.gz')]
    return sorted(names, key=lambda x: 0 if '48G' in x else 1)

    
class NovoalignEstimator(BaseEstimator):
    def __init__(self, meta=None, t=None, C=None, R=None, k=None):
        self.t = t
        self.C = C
        self.R = R
        self.k = k
        self.meta = meta


    def fit(self, samples, y=None):
        raw_opts = '-e 100 -c 8 -t {t} -C {C} -R {R} -k {k}'.format(**self.__dict__)
        opts = re.sub(r'-\w None', '', raw_opts)
        self.opts = opts

        self.data_dir = path(mkdtemp())
        
        bam_list = []
        
        for sample in samples:
            
            bam_fp = self.data_dir.joinpath(sample + '.bam')
            bam_list.append(bam_fp)
            logging.info('running novoalign %s for sample %s' % (self.opts,sample))
            
            with subset_fastq(
                    [
                        self.meta['adir'].read(sample=sample, read=1),
                        self.meta['adir'].read(sample=sample, read=2)
                    ],
                    N = self.meta['N_reads'],
                ) as fq_list:

                run_novoalign(
                    self.opts,
                    fq_list,
                    self.meta['ref_dir'].index(),
                    bam_fp,
                    )

                assert bam_fp.exists()
            
        return bam_list
    
        
    def predict(self, samples, y=None):
        pass


    def evaluate(self, ssu_db, bam_fp_list):
        series_list = []
        for bam_fp in bam_fp_list:
            sample = bam_fp.basename().stripext()
            with pysam.AlignmentFile(bam_fp, 'rb') as bamfile, \
                 open(self.meta['adir'].region(sample=sample), 'rb') as fh:
                seq_df = make_sequence_mapping_df(bamfile,
                                                  ssu_db,
                                                  pickle.load(fh),
                                                  )
            caller = mc.SimpleSpecies()
            species_df = caller.call(seq_df, min_regions=1, threshold=1).called
            series_list.append(gv9.reporting.relative_abundance(species_df, sample))

        abundance_df = pd.DataFrame({x.name : x for x in series_list})
        return gv9.reporting.abundance_scatterplot(abundance_df, self.meta['known'], output_fp=None)


        
def scorer(ssu_db, estimator, X):
    val = estimator.evaluate(ssu_db, estimator.data_dir.glob('*.bam'))
    estimator.data_dir.rmtree_p()
    return val
    


def sample_shuffle(N, folds=3):
    '''
    Assumes that the 4 mixture samples are first and the rest are mouse
    Selects one mixture and 2 mouse samples
    '''
    for m in random.sample(xrange(4), folds):
        yield [m] + random.sample(xrange(4,N), 2),[0]

    

if __name__ == '__main__':
    logging.basicConfig()

    meta=data()
    
    ssu_db = pickle.load(open(meta['ref_dir'].db(), 'r'))

    #set a parameter as [None] to use novoalign default
    param_grid = [
        {'t' : [60,90,120], 'R' : [2,5,7], 'C' : [None], 'meta':[meta], 'k' : [None]}
        ]

    param_grid = [
        {'t' : [60], 'R' : [15, 30, 45], 'C' : [None], 'meta':[meta], 'k' : ['']}
        ]

    ## param_grid = [
    ##     {'t' : [60,90], 'R' : [None], 'C' : [None], 'meta':[meta], 'k' : [None]}
    ##     ]



    sample_name_array = get_sample_names(meta['adir'])

    clf = GridSearchCV(NovoalignEstimator(),
                       param_grid,
                       scoring=partial(scorer,ssu_db),
                       cv=[x for x in sample_shuffle(len(sample_name_array))],
                       n_jobs=1)


    
    clf.fit(sample_name_array)
    
    print 'best set of parameters:'
    print clf.best_params_

    for params, mean_score, scores in clf.grid_scores_:
        print "%0.3f (+/-%0.03f) for %r" % (mean_score, scores.std() / 2, params)
              

    
    logging.shutdown()
