import gv9.reporting
import novoalign_tuning as nt
import pytest
import cPickle as pickle
import pdb

@pytest.fixture
def align_data():
    meta = nt.data()
    meta['N_reads'] = 1e4
    params = {'t' : 60, 'meta' : meta}
    
    ssu_db = pickle.load(open(meta['ref_dir'].db(), 'r'))
    sample_name_array = nt.get_sample_names(meta['adir'])
    
    return sample_name_array, params, ssu_db
    

def test_abundance_scatterplot(align_data):
    sample_names, params, ssu_db = align_data

    estimator = nt.NovoalignEstimator(**params)
    bam_list = estimator.fit(sample_names[5:6])
    assert len(bam_list) > 0
    r2 = estimator.evaluate(ssu_db, bam_list)
    assert r2 > -2
    
