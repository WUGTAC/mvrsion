from pandas.util.testing import assert_frame_equal
import pandas as pd
from gv9.microbeCall import *
from gv9.util.ssu_util import SeqID, RdpFactory, DataDirectory
import gv9.regionSelect as rs
import gv9.ssu
import gv9.combine_calls
import pytest

class FakeBam(object):
    def fetch(self, *args, **kwargs):
        return []

@pytest.fixture
def seq_df():
    index = map(SeqID, [('X', 1), ('X', 2), ('Y', 4), ('Z', 2)]) #make taxID diff from tax
    data = [(50, 50, 20, 'A'), (60, 10, 90, 'A'), (100, 100, 20, 'B'), (20, 100, 10, 'C')]
    df = pd.DataFrame.from_records(data, index=index, columns=['V1', 'V2', 'V3', 'tax'])
    return df

@pytest.fixture
def exp_seq_df():
    index = map(SeqID, [('X', 1), ('X', 2), ('Y', 4), ('Z', 2)]) #make taxID diff from tax
    data = [(50, 50, 20, 'A'), (60, 10, 90, 'A'), (100, 100, 5, 'B'), (20, 100, 5, 'C')]
    df = pd.DataFrame.from_records(data, index=index, columns=['V1', 'V2', 'V3', 'tax'])
    return df


def test_SimpleSpecies(seq_df):
    result = SimpleSpecies().call(seq_df, min_regions=2, threshold=100)

    assert result.called.index.equals(pd.Index(['A', 'B']))
                                      
    assert result.not_called.index.equals(pd.Index(['C']))    
    
    assert result.total.index.equals(pd.Index(['A', 'B', 'C']))

def test_ExpSimpleSpecies(seq_df, exp_seq_df):
    result = ExpSimpleSpecies().call(seq_df, min_regions=3, threshold=10, base=10)
    assert result.called.index.equals(pd.Index(['A', 'B', 'C']))
    assert result.not_called.index.equals(pd.Index([]))
    assert result.called.columns.equals(pd.Index(['V1', 'V2', 'V3']))

    result = ExpSimpleSpecies().call(exp_seq_df, min_regions=3, threshold=10, base=10)
    assert result.called.index.equals(pd.Index(['A', 'B']))
    assert result.not_called.index.equals(pd.Index(['C']))
    assert result.called.columns.equals(pd.Index(['V1', 'V2', 'V3']))
    

def test_DynamicRegions(seq_df):
    model = {'A' : set(['V1', 'V3']),
             'B' : set(['V1', 'V3']),
             'C' : set(['V2']),
             }

    result = DynamicRegions().call(seq_df, model, threshold=100)
    
    assert result.called.index.equals(pd.Index(['A', 'C']))
    
    #now ensure that if there is no model for C it is not called
    model = {k:v for k,v in model.items() if k != 'C'}
    result = DynamicRegions().call(seq_df, model, threshold=100)
    assert result.called.index.equals(pd.Index(['A']))

    #Add in a region not present in seq_df
    model['A'].add('V4')
    result = DynamicRegions().call(seq_df, model, threshold=100)
    assert result.called.index.equals(pd.Index([]))

def test_DynamicRegions_softbackup(seq_df, region_multimap_args):
    multimapper = rs.SoftBackupMultimap(*region_multimap_args)
    model = multimapper.find_minimal_intersect(N=2)
    result = DynamicRegions().call(seq_df, model=model)
    assert result.called.index.equals(pd.Index(['A', 'B', 'C']))

def test_TwoRoundCall_softbackup(seq_df, region_multimap_args):
    multimapper = rs.SoftBackupMultimap(*region_multimap_args)
    caller = TwoRoundCall()
    caller.call(seq_df, multimap=multimapper, min_regions=(2,2), threshold=100)
    assert caller.called.index.equals(pd.Index(['A', 'B']))

    #none of the species will pass the 3 region simple filter so no calls
    caller.call(seq_df, multimap=multimapper, min_regions=(3,3), threshold=100)
    assert caller.called.index.equals(pd.Index([]))


def test_KnownSample_rename_to_rdp(pkg_dir):
    factory = RdpFactory()
    rdp_list = [factory.create('k__;p__;c__;o__;f__;g__akkermansia;s__muciniphila;t__')]
    known = KnownSample(pkg_dir.joinpath('data', 'validation_knowns.txt'))
    akker_index = known.df.index.tolist().index('akkermansia muciniphila')

    new_df = known.rename_to_rdp(rdp_list)

    assert len(new_df.index) == len(known.df.index)
    assert len(new_df.columns) == len(known.df.columns)
    assert new_df.index[akker_index] == rdp_list[0]
    assert known.df.index[akker_index] == 'akkermansia muciniphila'

    
def test_KnownSample_abundance(pkg_dir):
    factory = RdpFactory()
    rdp = factory.create('k__;p__;c__;o__;f__;g__akkermansia;s__muciniphila;t__')
    known = KnownSample(pkg_dir.joinpath('data', 'validation_knowns.txt'))

    assert known.abundance('48G-1-1-staggered', rdp) == 0.0837

    rdp = factory.create('k__;p__;c__;o__;f__;g__akkermansia;s__;t__')

    assert known.abundance('48G-1-1-staggered', rdp) == 0


def test_KnownSample_fuzzy_samplematch(pkg_dir):
    known = KnownSample(pkg_dir.joinpath('data', 'validation_knowns.txt'))
    assert known.get_sample('1-0-10512-100G-B9-FP') == '1-0-10512-100G-B9-FP'
    assert known.get_sample('1.0.10512.100G.B9.FP') == '1-0-10512-100G-B9-FP'
    assert known.get_sample('1.0.010512.100G.B9.FP') == '1-0-10512-100G-B9-FP'
    assert known.get_sample('1-0-010512-100G-C11-FP') == '1-0-10512-100G-C11-FP'
    with pytest.raises(KeyError):
        known.get_sample('1-0-10512-100G-C19-FP')


def test_make_sequence_df_empty_bam():
    empty_seq_df = gv9.ssu.make_sequence_mapping_df(FakeBam(), None, None, all_regions=None)
    assert_frame_equal(empty_seq_df, pd.DataFrame(columns=['tax']), check_names=True)

    empty_seq_df = gv9.ssu.make_sequence_mapping_df(FakeBam(), None, None, all_regions=gv9.ssu.REGION_NAMES)
    assert_frame_equal(empty_seq_df, pd.DataFrame(columns=gv9.ssu.REGION_NAMES + ('tax',)), check_names=True)


def test_write_abundance_no_call(tmpdir, seq_df):
    #Should refactor this function to be a method of a new class CallResults
    # that is returned by MicrobeCaller.run()
    _dir = tmpdir.mkdir('Data')
    data_dir = DataDirectory(str(_dir))
    data_dir.create()
    caller = MicrobeCaller()
    caller.add_method('og', method=SimpleSpecies, threshold=200)

    #has a non-empty seq_df but the threshold is too high to call any microbes
    results = caller.run(seq_df)
    gv9.ssu.write_relative_abundance(results, data_dir, sample='test')
    df = gv9.combine_calls.make_method_df('og', data_dir)
    assert_frame_equal(df, pd.DataFrame(columns=['test']), check_names=True)

    #Test case of input bam file with no alignments
    #If there are no alignments, methods of other passed objects should not be called
    empty_seq_df = gv9.ssu.make_sequence_mapping_df(FakeBam(), None, None, all_regions=None)
    results = caller.run(empty_seq_df)
    gv9.ssu.write_relative_abundance(results, data_dir, sample='test')
    df = gv9.combine_calls.make_method_df('og', data_dir)
    assert_frame_equal(df, pd.DataFrame(columns=['test']), check_names=True)

    #Test case of input bam file with no alignments and using all_regions=region_names
    empty_seq_df_all_regions \
        = gv9.ssu.make_sequence_mapping_df(FakeBam(), None, None, all_regions=gv9.ssu.REGION_NAMES)
    results = caller.run(empty_seq_df_all_regions)
    gv9.ssu.write_relative_abundance(results, data_dir, sample='test')
    df = gv9.combine_calls.make_method_df('og', data_dir)
    assert_frame_equal(df, pd.DataFrame(columns=['test']), check_names=True)
