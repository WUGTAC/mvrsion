import pytest
import gv9.util.ssu_util as util
import gv9.regionSelect as rs

class SmallRdp(util._RdpString):
    rdp_label_hierarchy = ('s')

    
@pytest.fixture
def in_silico_pcr():
    factory = util.RdpFactory(SmallRdp)
    db = {}
    db['1'] = factory.create('s__a')
    db['2'] = factory.create('s__b')
    db['3'] = factory.create('s__c')
    db['4'] = factory.create('s__c')
    db['5'] = factory.create('s__d')

    #s__a only has V1 and V2 but V2 maps to 
    pcr_report = rs.pd.DataFrame({'V1' : [1,1,1,1,1], 'V2' : [0,3,0,14,0],
                                  'V3' : [0,3,4,5,0], 'V4' : [0,0,0,0,1]},
                                  index=map(str, range(1,6)))

    #format of these tuples is (read_taxa, region, ref_taxa) meaning the region of read_taxa maps to ref_taxa
    align_results = [(db['1'], 'V1', db[str(x)]) for x in xrange(2,5)] \
      + [(db['1'], 'V4', db['4'])] \
      + [(db['2'], 'V1', db[str(x)]) for x in ['1', '3']] \
      + [(db['2'], 'V2', db['1']), (db['2'], 'V2', db['2']), (db['2'], 'V3', db['3'])]
    region_set = set(['V1', 'V2', 'V3', 'V4'])
    return db,pcr_report,[rs.RegionMultimap.Record(*r) for r in align_results], region_set


def test_parse_pcr_results(in_silico_pcr):
    db,report,_,_ = in_silico_pcr
    result = rs.RegionMultimap.parse_pcr_results(report, db)
    assert result == \
      {'s__A' : ['V1'], 's__B' : ['V1', 'V2', 'V3'], 's__C' : ['V1', 'V3'], 's__D' : ['V1', 'V4']}
      ## set([(db['1'], 'V1'), (db['2'], 'V2'), (db['2'], 'V1'),
      ##      (db['2'], 'V3'), (db['3'], 'V1'), (db['3'], 'V3'),
      ##      (db['5'], 'V1'), (db['5'], 'V4'),
      ##      ])
    
def test_RegionMultimap(in_silico_pcr):
    db,pcr_results,align_results,region_set = in_silico_pcr
    
    #test object construction
    species_set = set(db.values())
    species_set.remove(db['5']) #s__D is not in our sub-db
    
    multimap = rs.RegionMultimap(align_results, species_set, rs.RegionMultimap.parse_pcr_results(pcr_results, db), region_set=region_set)
    assert multimap.data == \
      {'s__C': {'V1' : set([]), 'V3': set([])},
       's__B': {'V2': set(['s__A']), 'V3' : set(['s__C']), 'V1' : set(['s__A', 's__C'])},
       's__A': {'V1' : set(['s__B', 's__C'])}
      }
    
    chosen_regions = multimap.find_minimal_intersect(3)
    assert chosen_regions[db['1']] == set(['V1'])
    assert chosen_regions[db['2']] == set(['V2', 'V3'])

def test_SoftBackupMultimap(region_multimap_args):
    mapper = rs.SoftBackupMultimap(*region_multimap_args)
    model = mapper.find_minimal_intersect(N=2)
    assert model == {'A': [set(['V1', 'V2']), set(['V1', 'V3']), set(['V2', 'V3'])],
                     'B': [set(['V3']), set(['V1', 'V2'])],
                     'C': [set(['V2'])]}

    model = mapper.find_minimal_intersect(N=3)
    assert model == {'A': [set(['V1', 'V2', 'V3'])],
                     'B': [set(['V3']), set(['V1', 'V2'])],
                     'C': [set(['V2'])]}

    model = mapper.find_minimal_intersect(N=2, called_rdp=['A', 'B'])
    assert model == {'A': [set(['V1']), set(['V2']), set(['V3'])],
                     'B': [set(['V1']), set(['V3'])],}
