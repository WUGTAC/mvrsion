from rope.base.project import Project
from rope.refactor.move import MoveModule
from rope.contrib import generate

from path import path

# command to run before this python script from project root is:
# find gv9 -type d ! -wholename "*test*" -exec touch '{}'/__init__.py \; ; git reset --hard; rm -r gv9/gv9;
root = path('/home/aeschriefer/gv9')
myproject = Project(root)
raw_modules = [p for p in path('gv9').glob('*')
               if p.isdir() or p.ext == '.py']

raw_modules2 = [m for m in raw_modules if not ('tests' in m or 'scripts' in m or '__init__' in m)]

srcdir = [s for s in myproject.get_source_folders() if s.path == 'gv9'][0]

dest = generate.create_package(myproject, 'gv9', srcdir)

for p in raw_modules2:
    mover = MoveModule(myproject, myproject.get_resource(p))
    myproject.do(mover.get_changes(dest))
    
